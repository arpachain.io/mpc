## Useful Tools
### Eth Account Generator
```
tools/accounts_generator
```
Generate a batch of accounts (private key and its corresponding address) that is compatible to ethereum network
