package request_test

import (
	"encoding/json"
	"log"
	"os"
	"testing"

	"github.com/dgraph-io/badger"
	"github.com/stretchr/testify/assert"
	"gitlab.com/arpachain/mpc/eth/pkg/request"
)

type test struct {
	mpcID string
	reqID int64
}

var (
	dbPath         string
	badgerDB       *badger.DB
	testRepository request.Repository
	testCases      = []test{
		{"12345", 4},
		{"Test", 888},
	}
	nonExistMpcIds = []string{
		"Not inserted case 1",
		"Not existed case 2",
	}
)

func TestMain(m *testing.M) {
	setDBPath()
	retCode := m.Run()
	os.Exit(retCode)
}

func setDBPath() {
	goPath := os.Getenv("GOPATH")
	dbPath = goPath + "/src/gitlab.com/arpachain/mpc/eth/test/tmp_req_repo"
}

type setupTestCase func(t *testing.T, db *badger.DB, testCases []test)

func setup(t *testing.T, fns ...setupTestCase) func(t *testing.T) {
	log.Println("setting up test...")
	opts := badger.DefaultOptions(dbPath)
	var err error
	badgerDB, err = badger.Open(opts)
	testRepository = request.NewBadgerRepository(badgerDB)
	if err != nil {
		t.Fatalf("setting up test failed: %v", err)
	}
	// calls setupTestCase functions if passed in
	for _, fn := range fns {
		fn(t, badgerDB, testCases)
	}
	return func(t *testing.T) {
		log.Println("tearing down test...")
		testRepository = nil
		badgerDB.Close()
		badgerDB = nil
		err := os.RemoveAll(dbPath)
		if err != nil {
			t.Fatalf("tearing down test failed: %v", err)
		}
	}
}

func TestInsert(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t)
	defer tearDown(t)

	for _, testCase := range testCases {
		err := testRepository.Insert(testCase.mpcID, testCase.reqID)
		assert.NoError(err)
	}

	for _, testCase := range testCases {
		reqID, exists, err := searchInDatabase(
			t,
			badgerDB,
			request.KeyPrefix,
			testCase.mpcID,
		)
		assert.NoError(err)
		assert.True(exists)
		assert.Equal(testCase.reqID, reqID)
	}

	t.Log("Ensure inserting the same hashs again will not return an error...")
	for _, testCase := range testCases {
		err := testRepository.Insert(testCase.mpcID, testCase.reqID)
		assert.NoError(err)
	}

}
func TestLookUp(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, insertToDatabase)
	defer tearDown(t)

	for _, testCase := range testCases {
		reqID, exists, err := testRepository.LookUp(testCase.mpcID)
		assert.NoError(err)
		assert.True(exists)
		assert.Equal(testCase.reqID, reqID)
	}
}

func TestLookUp_NotFound(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, insertToDatabase)
	defer tearDown(t)

	for _, noExistMpcId := range nonExistMpcIds {
		_, exists, err := testRepository.LookUp(noExistMpcId)
		assert.NoError(err)
		assert.False(exists)
	}
}

func insertToDatabase(t *testing.T, db *badger.DB, testCases []test) {
	for _, testCase := range testCases {
		txn := db.NewTransaction(true)
		defer txn.Discard()
		reqIDBytes, err := json.Marshal(testCase.reqID)
		if err != nil {
			t.Fatalf("setting up test case failed: %v", err)
		}
		err = txn.Set([]byte(request.KeyPrefix+testCase.mpcID), reqIDBytes)
		if err != nil {
			t.Fatalf("setting up test case failed: %v", err)
		}
		if err := txn.Commit(); err != nil {
			t.Fatalf("setting up test case failed: %v", err)
		}
	}
}

// Look up a mpcID and return the corresponding reqID(int64).
// If the mpcID is not found in the db, the second argument returned will be false, without an error.
// Therefore, first check the error, then check if the value is found, finally use the value.
func searchInDatabase(t *testing.T, db *badger.DB, keyPrefix, mpcID string) (int64, bool, error) {
	var reqID int64
	err := db.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(keyPrefix + mpcID))
		if err != nil {
			return err
		}
		err = item.Value(func(v []byte) error {
			err := json.Unmarshal(v, &reqID)
			return err
		})
		return err
	})
	if err != nil {
		if err == badger.ErrKeyNotFound {
			return reqID, false, nil
		}
		return reqID, false, err
	}
	return reqID, true, nil
}
