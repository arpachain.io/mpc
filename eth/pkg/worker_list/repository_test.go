package worker_list_test

import (
	"encoding/json"
	"log"
	"os"
	"testing"

	"gitlab.com/arpachain/mpc/eth/pkg/worker_list"
	"github.com/dgraph-io/badger"
	"github.com/ethereum/go-ethereum/common"
	"github.com/stretchr/testify/assert"
)

var (
	dbPath         string
	badgerDB       *badger.DB
	testRepository worker_list.Repository
	initWorkerList = []common.Address{
		common.HexToAddress("0x709CEFa1EF25336a3e625E63F4aB886c267a184D"),
		common.HexToAddress("0x15a306FAC0C6931aB93DD435d11B0d93B1326e0D"),
		common.HexToAddress("0xf4742960e188763D1D5cF544d1f9b6dC8131D1cf"),
		common.HexToAddress("0x8f1efA7939ad423416677eB3A4b176DB907e7cE8"),
	}
	testWorkerList = []common.Address{
		common.HexToAddress("0xf4742960e188763D1D5cF544d1f9b6dC8131D1cf"),
		common.HexToAddress("0x8f1efA7939ad423416677eB3A4b176DB907e7cE8"),
		common.HexToAddress("0xDa5cf2C3Fe7Bc1BF652eB842D2e6Aa18B13Fd493"),
		common.HexToAddress("0xD165984bb3dB61C01274f80289BBa318d0aD4A7c"),
		common.HexToAddress("0xEa5bb6B964DdE7442F78Ebe938766f361BA99188"),
	}
)

func TestMain(m *testing.M) {
	setDBPath()
	retCode := m.Run()
	os.Exit(retCode)
}

func setDBPath() {
	goPath := os.Getenv("GOPATH")
	dbPath = goPath + "/src/gitlab.com/arpachain/mpc/eth/test/tmp_worker_list_repo"
}

type setupTestCase func(t *testing.T, db *badger.DB, workerList []common.Address)

func setup(t *testing.T, fns ...setupTestCase) func(t *testing.T) {
	log.Println("setting up test...")
	opts := badger.DefaultOptions(dbPath)
	var err error
	badgerDB, err = badger.Open(opts)
	testRepository = worker_list.NewBadgerRepository(badgerDB)
	if err != nil {
		t.Fatalf("setting up test failed: %v", err)
	}
	// calls setupTestCase functions if passed in
	for _, fn := range fns {
		fn(t, badgerDB, initWorkerList)
	}
	return func(t *testing.T) {
		log.Println("tearing down test...")
		testRepository = nil
		badgerDB.Close()
		badgerDB = nil
		err := os.RemoveAll(dbPath)
		if err != nil {
			t.Fatalf("tearing down test failed: %v", err)
		}
	}
}

func TestStoreWorkerList(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t)
	defer tearDown(t)

	t.Logf("Storing initial worker list...\n")
	err := testRepository.StoreWorkerList(initWorkerList)
	assert.NoError(err)
	workerList, exists, err := searchInDatabase(t, badgerDB)
	assert.NoError(err)
	assert.True(exists)
	assert.Equal(initWorkerList, workerList)

	t.Logf("Storing updated worker list...\n")
	err = testRepository.StoreWorkerList(testWorkerList)
	assert.NoError(err)
	workerList, exists, err = searchInDatabase(t, badgerDB)
	assert.NoError(err)
	assert.True(exists)
	assert.Equal(testWorkerList, workerList)
}

func TestGetWorkerList(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, insertToDatabase)
	defer tearDown(t)

	workerList, exists, err := testRepository.GetWorkerList()
	assert.NoError(err)
	assert.True(exists)
	assert.Equal(initWorkerList, workerList)
}

func insertToDatabase(t *testing.T, db *badger.DB, workerList []common.Address) {
	txn := db.NewTransaction(true)
	defer txn.Discard()
	workerListBytes, err := json.Marshal(workerList)
	if err != nil {
		t.Fatalf("setting up worker list failed: %v", err)
	}
	err = txn.Set([]byte(worker_list.KeyPrefix+worker_list.Key), workerListBytes)
	if err != nil {
		t.Fatalf("setting up worker list failed: %v", err)
	}
	if err := txn.Commit(); err != nil {
		t.Fatalf("setting up worker list failed: %v", err)
	}
}

// Look up the worker list
// If the worker list is not found in the db, the second argument returned will be false, without an error.
// Therefore, first check the error, then check if the value is found, finally use the value.
func searchInDatabase(t *testing.T, db *badger.DB) ([]common.Address, bool, error) {
	workerList := make([]common.Address, 0)
	err := db.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(worker_list.KeyPrefix + worker_list.Key))
		if err != nil {
			return err
		}
		err = item.Value(func(v []byte) error {
			err := json.Unmarshal(v, &workerList)
			return err
		})
		return err
	})
	if err != nil {
		if err == badger.ErrKeyNotFound {
			return nil, false, nil
		}
		return nil, false, err
	}
	return workerList, true, nil
}
