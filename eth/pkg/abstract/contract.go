package abstract

import (
	"math/big"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
)

type Contract interface {
	UpdateWorkerList(opts *bind.TransactOpts, workersToAdd, workersToRemove []common.Address) (*types.Transaction, error)
	RequestMpc(
		opts *bind.TransactOpts,
		numWorkers *big.Int,
		programHash string,
		_dataProviderWorkers []common.Address,
		delegatedWorker common.Address,
		feeForDataProviderWorker []*big.Int,
		feePerAdditionalWorker *big.Int,
	) (*types.Transaction, error)
	StartComputation(opts *bind.TransactOpts, reqID *big.Int, workerList []common.Address) (*types.Transaction, error)
	PostResult(opts *bind.TransactOpts, reqID *big.Int, result string) (*types.Transaction, error)
}
