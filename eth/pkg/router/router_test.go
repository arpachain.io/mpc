package router_test

import (
	"sync"
	"testing"
	"time"

	"gitlab.com/arpachain/mpc/eth/pkg/model"
	"gitlab.com/arpachain/mpc/eth/pkg/router"
	"gitlab.com/arpachain/mpc/eth/pkg/transaction"
	"gitlab.com/arpachain/mpc/eth/test/fake"
	"github.com/ethereum/go-ethereum/common"
	"github.com/stretchr/testify/assert"
)

const waitingTime = 10 * time.Millisecond

var testHashes = [4]string{ // has to be different
	"Test Hash 1",
	"Test Hash 2",
	"Test Hash 3",
	"Test Hash 4",
}

func TestRouter(t *testing.T) {
	assert := assert.New(t)

	eventChan := make(chan int)
	fakeRepository := newFakeRepository()
	logSigHashMap := model.NewLogSigHashMap()
	fakeHandler := fake.NewFakeHandler(eventChan, assert, logSigHashMap)
	router := router.NewRouter(fakeHandler, logSigHashMap, fakeRepository)

	t.Log("Sending a requestReceived event...\n")
	event := model.Event{}
	event.Log.Topics = append(event.Log.Topics, logSigHashMap.GetRequestReceivedSigHash())
	event.Log.TxHash = common.BytesToHash([]byte(testHashes[fake.RequestReceived]))
	go router.Route(&event)
	select {
	case eventName := <-eventChan:
		assert.Equal(fake.RequestReceived, eventName)
	case <-time.After(waitingTime):
		t.Fatalf("No response from requestReceived after a reasonable amount of time\n")
	}
	t.Log("Sending a duplicated requestReceived event...\n")
	go router.Route(&event)
	select {
	case <-eventChan:
		t.Fatalf("Handler handles a duplicated event\n")
	case <-time.After(waitingTime):
	}

	t.Log("Sending a computationStarted event...\n")
	event = model.Event{}
	event.Log.Topics = append(event.Log.Topics, logSigHashMap.GetComputationStartedSigHash())
	event.Log.TxHash = common.BytesToHash([]byte(testHashes[fake.ComputationStarted]))
	go router.Route(&event)
	select {
	case eventName := <-eventChan:
		assert.Equal(fake.ComputationStarted, eventName)
	case <-time.After(waitingTime):
		t.Fatalf("No response from computationStarted after a reasonable amount of time\n")
	}
	t.Log("Sending a duplicated computationStarted event...\n")
	go router.Route(&event)
	select {
	case <-eventChan:
		t.Fatalf("Handler handles a duplicated event\n")
	case <-time.After(waitingTime):
	}

	t.Log("Sending a resultPosted event...\n")
	event = model.Event{}
	event.Log.Topics = append(event.Log.Topics, logSigHashMap.GetResultPostedSigHash())
	event.Log.TxHash = common.BytesToHash([]byte(testHashes[fake.ResultPosted]))
	go router.Route(&event)
	select {
	case eventName := <-eventChan:
		assert.Equal(fake.ResultPosted, eventName)
	case <-time.After(waitingTime):
		t.Fatalf("No response from resultPosted after a reasonable amount of time\n")
	}
	t.Log("Sending a duplicated resultPosted event...\n")
	go router.Route(&event)
	select {
	case <-eventChan:
		t.Fatalf("Handler handles a duplicated event\n")
	case <-time.After(waitingTime):
	}

	t.Log("Sending a workerListUpdated event...\n")
	event = model.Event{}
	event.Log.Topics = append(event.Log.Topics, logSigHashMap.GetWorkerListUpdatedSigHash())
	event.Log.TxHash = common.BytesToHash([]byte(testHashes[fake.WorkerListUpdated]))
	go router.Route(&event)
	select {
	case eventName := <-eventChan:
		assert.Equal(fake.WorkerListUpdated, eventName)
	case <-time.After(waitingTime):
		t.Fatalf("No response from workerListUpdated after a reasonable amount of time\n")
	}
	t.Log("Sending a duplicated workerListUpdated event...\n")
	go router.Route(&event)
	select {
	case <-eventChan:
		t.Fatalf("Handler handles a duplicated event\n")
	case <-time.After(waitingTime):
	}

}

type fakeRepository struct {
	mu      sync.Mutex
	hashMap map[common.Hash]bool
}

func newFakeRepository() transaction.Repository {
	hashMap := make(map[common.Hash]bool)
	return &fakeRepository{
		hashMap: hashMap,
	}
}

func (f *fakeRepository) Insert(hash common.Hash) error {
	f.mu.Lock()
	f.hashMap[hash] = true
	f.mu.Unlock()
	return nil
}

func (f *fakeRepository) HasKey(hash common.Hash) (bool, error) {
	f.mu.Lock()
	value, ok := f.hashMap[hash]
	f.mu.Unlock()
	if !ok {
		return false, nil
	}
	return value, nil
}
