package parser_test

import (
	"encoding/hex"
	"log"
	"math/big"
	"strings"
	"testing"

	"gitlab.com/arpachain/mpc/eth/pkg/model"
	"gitlab.com/arpachain/mpc/eth/pkg/parser"
	contract "gitlab.com/arpachain/mpc/eth/sol"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"
	"github.com/stretchr/testify/assert"
)

var (
	testLogRequestReceived = model.LogRequestReceived{
		ReqId:                     big.NewInt(1), // it seems that assert.Equal has problem with two bit.Int(0)
		NumWorkers:                big.NewInt(3),
		ProgramHash:               "Test",
		DataConsumer:              common.HexToAddress("0x54c946765fe7553a26d91ddebf3df90435700673"),
		DataProviderWorkers:       []common.Address{common.HexToAddress("0x605a9beb2de5dc92fe0df269ec43b810d8a5063e")},
		DelegatedWorker:           common.HexToAddress("0xf4742960e188763d1d5cf544d1f9b6dc8131d1cf"),
		FeeForDataProviderWorkers: []*big.Int{new(big.Int).Exp(big.NewInt(10), big.NewInt(18), nil)},
		FeePerAdditionalWorker:    new(big.Int).Exp(big.NewInt(10), big.NewInt(18), nil),
	}
	testLogComputationStarted = model.LogComputationStarted{
		ReqId: big.NewInt(1),
	}
	testLogResultPosted = model.LogResultPosted{
		ReqId:  big.NewInt(1),
		Result: "Test String",
	}
	testLogWorkerListUpdated = model.LogWorkerListUpdated{
		WorkerList: []common.Address{
			common.HexToAddress("0x605a9beb2de5dc92fe0df269ec43b810d8a5063e"),
			common.HexToAddress("0x7a6380c018e9ded3d41cc441a76fc58d803eb773"),
			common.HexToAddress("0x54c946765fe7553a26d91ddebf3df90435700673"),
			common.HexToAddress("0x709cefa1ef25336a3e625e63f4ab886c267a184d"),
			common.HexToAddress("0x15a306fac0c6931ab93dd435d11b0d93b1326e0d"),
		},
	}
	testLogRequestReceivedHex    = "00000000000000000000000000000000000000000000000000000000000000010000000000000000000000000000000000000000000000000000000000000003000000000000000000000000000000000000000000000000000000000000010000000000000000000000000054c946765fe7553a26d91ddebf3df904357006730000000000000000000000000000000000000000000000000000000000000140000000000000000000000000f4742960e188763d1d5cf544d1f9b6dc8131d1cf00000000000000000000000000000000000000000000000000000000000001800000000000000000000000000000000000000000000000000de0b6b3a7640000000000000000000000000000000000000000000000000000000000000000000454657374000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001000000000000000000000000605a9beb2de5dc92fe0df269ec43b810d8a5063e00000000000000000000000000000000000000000000000000000000000000010000000000000000000000000000000000000000000000000de0b6b3a7640000"
	testLogComputationStartedHex = "0000000000000000000000000000000000000000000000000000000000000001"
	testLogResultPostedHex       = "00000000000000000000000000000000000000000000000000000000000000010000000000000000000000000000000000000000000000000000000000000040000000000000000000000000000000000000000000000000000000000000000b5465737420537472696e67000000000000000000000000000000000000000000"
	testLogWorkerListUpdatedHex  = "00000000000000000000000000000000000000000000000000000000000000200000000000000000000000000000000000000000000000000000000000000005000000000000000000000000605a9beb2de5dc92fe0df269ec43b810d8a5063e0000000000000000000000007a6380c018e9ded3d41cc441a76fc58d803eb77300000000000000000000000054c946765fe7553a26d91ddebf3df90435700673000000000000000000000000709cefa1ef25336a3e625e63f4ab886c267a184d00000000000000000000000015a306fac0c6931ab93dd435d11b0d93b1326e0d"
)

func TestParseRequestReceivedEvent(t *testing.T) {

	assert := assert.New(t)
	event := model.Event{}

	data, err := hex.DecodeString(testLogRequestReceivedHex)
	if err != nil {
		log.Fatal(err)
	}
	event.Log.Data = data

	contractAbi, err := abi.JSON(strings.NewReader(string(contract.ContractABI)))
	assert.NoError(err)

	parser := parser.NewParser(contractAbi)

	logRequestReceived, err := parser.ParseRequestReceivedEvent(&event)
	assert.NoError(err)
	assert.Equal(testLogRequestReceived, *logRequestReceived)

}

func TestParseComputationStartedEvent(t *testing.T) {

	assert := assert.New(t)
	event := model.Event{}

	data, err := hex.DecodeString(testLogComputationStartedHex)
	if err != nil {
		log.Fatal(err)
	}
	event.Log.Data = data

	contractAbi, err := abi.JSON(strings.NewReader(string(contract.ContractABI)))
	assert.NoError(err)

	parser := parser.NewParser(contractAbi)

	logComputationStarted, err := parser.ParseComputationStartedEvent(&event)
	assert.NoError(err)
	assert.Equal(testLogComputationStarted, *logComputationStarted)

}

func TestParseResultPostedEvent(t *testing.T) {

	assert := assert.New(t)
	event := model.Event{}

	data, err := hex.DecodeString(testLogResultPostedHex)
	if err != nil {
		log.Fatal(err)
	}
	event.Log.Data = data

	contractAbi, err := abi.JSON(strings.NewReader(string(contract.ContractABI)))
	assert.NoError(err)

	parser := parser.NewParser(contractAbi)

	logResultPosted, err := parser.ParseResultPostedEvent(&event)
	assert.NoError(err)
	assert.Equal(testLogResultPosted, *logResultPosted)

}

func TestParseWorkerListUpdatedEvent(t *testing.T) {

	assert := assert.New(t)
	event := model.Event{}

	data, err := hex.DecodeString(testLogWorkerListUpdatedHex)
	if err != nil {
		log.Fatal(err)
	}
	event.Log.Data = data

	contractAbi, err := abi.JSON(strings.NewReader(string(contract.ContractABI)))
	assert.NoError(err)

	parser := parser.NewParser(contractAbi)

	logWorkerListUpdated, err := parser.ParseWorkerListUpdatedEvent(&event)
	assert.NoError(err)
	assert.Equal(testLogWorkerListUpdated, *logWorkerListUpdated)

}
