package listener_test

import (
	"context"
	"fmt"
	"gitlab.com/arpachain/mpc/eth/pkg/abstract"
	"testing"
	"time"

	"gitlab.com/arpachain/mpc/eth/pkg/listener"
	"gitlab.com/arpachain/mpc/eth/pkg/model"
	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/stretchr/testify/assert"
)

const (
	numEvents = 10
)

func TestListenerMultipleEventsAtOnce(t *testing.T) {
	assert := assert.New(t)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	client := NewFakeEthClientForListener(true)
	router := NewFakeRouter()
	contractAddress := common.Address{}
	listener := listener.NewListener(client, contractAddress, router)
	go listener.Start(ctx, numEvents)
	t.Log("waiting for all events to arrive...")
	time.Sleep(time.Millisecond * 100)
	close(router.TestLogChan)
	eventCounter := 0
	for log := range router.TestLogChan {
		assert.Equal("fake_log_data", log)
		eventCounter++
	}
	assert.Equal(numEvents, eventCounter)
}

func TestListenerSequentialEvents(t *testing.T) {
	assert := assert.New(t)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	client := NewFakeEthClientForListener(false)
	router := NewFakeRouter()
	contractAddress := common.Address{}
	listener := listener.NewListener(client, contractAddress, router)
	go listener.Start(ctx, numEvents)
	// Wait for 30ms as offset to leave enough time for go routine finish after sleep.
	time.Sleep(30 * time.Millisecond)
	// Event arrives roughly every 100ms
	interval := 100 * time.Millisecond
	for i := 0; i < numEvents; i++ {
		time.Sleep(interval)
		assert.Equal(1, len(router.TestLogChan), "event should have arrived")
		assert.Equal("fake_log_data", <-router.TestLogChan, "event should be routed")
		assert.Equal(0, len(router.TestLogChan), "channel should now be empty")
	}
}

type fakeEthClientForListener struct {
	multiEventsAtOnce bool
}

func NewFakeEthClientForListener(multiEventsAtOnce bool) abstract.ClientForListener {
	return &fakeEthClientForListener{
		multiEventsAtOnce: multiEventsAtOnce,
	}
}

func (c *fakeEthClientForListener) SubscribeFilterLogs(
	ctx context.Context,
	q ethereum.FilterQuery,
	ch chan<- types.Log,
) (ethereum.Subscription, error) {
	sub := &fakeSubscription{}
	// simulates sending n events to the listener
	for i := 0; i < numEvents; i++ {
		go func(i int) {
			if !c.multiEventsAtOnce {
				// event is sent every 100ms
				multiplier := 100 * (i + 1)
				interval := time.Millisecond * time.Duration(multiplier)
				time.Sleep(interval)
			}
			fakeLog := types.Log{
				Data: []byte("fake_log_data"),
			}
			ch <- fakeLog
		}(i)
	}
	return sub, nil
}

type fakeSubscription struct{}

func (s *fakeSubscription) Unsubscribe() {
	fmt.Println("mock subscription Unsubscribe called. No-op.")
}

func (s *fakeSubscription) Err() <-chan error {
	return make(<-chan error)
}

type fakeRouter struct {
	TestLogChan chan string
}

func NewFakeRouter() *fakeRouter {
	return &fakeRouter{
		TestLogChan: make(chan string, numEvents),
	}
}

func (r *fakeRouter) Route(event *model.Event) {
	logData := string(event.Log.Data)
	fmt.Printf("event routed by mock router with log: %v\n", logData)
	r.TestLogChan <- logData
}
