package listener

import (
	"context"
	"log"
	"time"

	"gitlab.com/arpachain/mpc/eth/pkg/abstract"
	"gitlab.com/arpachain/mpc/eth/pkg/model"
	"gitlab.com/arpachain/mpc/eth/pkg/router"
	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
)

const retryInterval = time.Minute

// Listener listens to the events emitted from Ethereum Smart Contracts
type Listener interface {
	Start(ctx context.Context, bufferSize int) error
}

// Reference implementation:
type listener struct {
	client          abstract.ClientForListener
	contractAddress common.Address
	router          router.Router
}

// NewListener returns an instance of eth.Listener
func NewListener(
	client abstract.ClientForListener,
	contractAddress common.Address,
	router router.Router,
) Listener {
	return &listener{
		client:          client,
		contractAddress: contractAddress,
		router:          router,
	}
}

func (l *listener) Start(ctx context.Context, bufferSize int) error {
	query := ethereum.FilterQuery{
		Addresses: []common.Address{l.contractAddress},
	}
	logChan := make(chan types.Log, bufferSize)
	log.Println("Eth listener: subscribing to event chan...")
	sub, err := l.client.SubscribeFilterLogs(ctx, query, logChan)
	if err != nil {
		log.Println("Eth listener: Subscribe Failed: ", err)
		return err
	}
	for {
		select {
		case <-ctx.Done():
			log.Println("Eth listener: received signal, aborting...")
			return nil
		case err := <-sub.Err():
			log.Printf("Eth listener: received an err from event subscription: %v", err)
			for {
				sub, err = l.client.SubscribeFilterLogs(ctx, query, logChan)
				if err != nil {
					log.Printf("Eth listener: re-subscription failed: %v", err)
					time.Sleep(retryInterval)
					continue
				}
				log.Println("Eth listener: re-subscription succeeded")
				break
			}

		case eventLog := <-logChan:
			event := &model.Event{
				ContractAddress: l.contractAddress,
				Log:             eventLog,
			}
			log.Printf("Eth listener: Rounting event of tx: %v", eventLog.TxHash.Hex())
			go l.router.Route(event)
		}
	}
}
