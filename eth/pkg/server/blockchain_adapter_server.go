package server

import (
	"context"
	"log"
	"sync"

	pb "gitlab.com/arpachain/mpc/eth/api"
	"gitlab.com/arpachain/mpc/eth/pkg/handler"
	"gitlab.com/arpachain/mpc/eth/pkg/util"
)

type blochchainAdapterServer struct {
	handler          handler.Handler
	formerWorkerList []string
	mu               sync.Mutex
}

func NewBlochchainAdapterServer(handler handler.Handler) pb.BlochchainAdapterServer {
	emptyStrList := []string{}
	return &blochchainAdapterServer{
		handler:          handler,
		formerWorkerList: emptyStrList,
	}
}

func (s *blochchainAdapterServer) ReportMpcResult(ctx context.Context, req *pb.ReportMpcResultRequest) (*pb.ReportMpcResultResponse, error) {
	go s.handler.HandleReportMpcResultBlockchain(req)
	return &pb.ReportMpcResultResponse{
		Success: true,
	}, nil
}

func (s *blochchainAdapterServer) UpdateWorkerList(ctx context.Context, req *pb.UpdateWorkerListRequest) (*pb.UpdateWorkerListResponse, error) {
	log.Println("BlochchainAdapter server: handling update worker list request...")
	s.mu.Lock()
	defer s.mu.Unlock()
	if !util.SameStringSlice(req.WorkerAddresses, s.formerWorkerList) {
		s.formerWorkerList = make([]string, len(req.WorkerAddresses))
		copy(s.formerWorkerList, req.WorkerAddresses)
		go s.handler.HandleUpdateWorkerListBlockchain(req)
	}
	return &pb.UpdateWorkerListResponse{
		Success: true,
	}, nil
}
