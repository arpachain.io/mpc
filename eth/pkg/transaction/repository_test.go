package transaction_test

import (
	"encoding/json"
	"log"
	"os"
	"testing"

	"gitlab.com/arpachain/mpc/eth/pkg/transaction"
	"github.com/dgraph-io/badger"
	"github.com/ethereum/go-ethereum/common"
	"github.com/stretchr/testify/assert"
)

var (
	dbPath           string
	badgerDB         *badger.DB
	testRepository   transaction.Repository
	testHashes       []common.Hash
	notInsertedHashs []common.Hash
	testHashStrings  = []string{
		"Inserted Test Hash 1",
		"Inserted Test Hash 2",
	}
	notInsertedHashStrings = []string{
		"Not Inserted Test Hash 1",
		"Not Inserted Test Hash 2",
	}
)

func TestMain(m *testing.M) {
	setDBPath()
	setTestHashes()
	retCode := m.Run()
	os.Exit(retCode)
}

func setDBPath() {
	goPath := os.Getenv("GOPATH")
	dbPath = goPath + "/src/gitlab.com/arpachain/mpc/eth/test/tmp_transaction_repo"
}

func setTestHashes() {
	testHashes = make([]common.Hash, 0)
	for _, s := range testHashStrings {
		testHashes = append(testHashes, common.BytesToHash([]byte(s)))
	}
	notInsertedHashs = make([]common.Hash, 0)
	for _, s := range notInsertedHashStrings {
		notInsertedHashs = append(notInsertedHashs, common.BytesToHash([]byte(s)))
	}
}

type setupTestCase func(t *testing.T, db *badger.DB, testHashes []common.Hash)

func setup(t *testing.T, fns ...setupTestCase) func(t *testing.T) {
	log.Println("setting up test...")
	opts := badger.DefaultOptions(dbPath)
	var err error
	badgerDB, err = badger.Open(opts)
	testRepository = transaction.NewBadgerRepository(badgerDB)
	if err != nil {
		t.Fatalf("setting up test failed: %v", err)
	}
	// calls setupTestCase functions if passed in
	for _, fn := range fns {
		fn(t, badgerDB, testHashes)
	}
	return func(t *testing.T) {
		log.Println("tearing down test...")
		testRepository = nil
		badgerDB.Close()
		badgerDB = nil
		err := os.RemoveAll(dbPath)
		if err != nil {
			t.Fatalf("tearing down test failed: %v", err)
		}
	}
}

func TestInsert(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t)
	defer tearDown(t)

	for _, hash := range testHashes {
		err := testRepository.Insert(hash)
		assert.NoError(err)
	}

	for _, hash := range testHashes {
		exists, err := searchHashInDatabase(
			t,
			badgerDB,
			transaction.KeyPrefix,
			hash,
		)
		assert.NoError(err)
		assert.True(exists)
	}

	t.Log("Ensure inserting the same hashs again will not return an error...")
	for _, hash := range testHashes {
		err := testRepository.Insert(hash)
		assert.NoError(err)
	}

}
func TestHasKey(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, insertHashsToDatabase)
	defer tearDown(t)

	for _, hash := range testHashes {
		exists, err := testRepository.HasKey(hash)
		assert.NoError(err)
		assert.True(exists)
	}
}

func TestHasKey_KeyNotFound(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, insertHashsToDatabase)
	defer tearDown(t)

	for _, hash := range notInsertedHashs {
		exists, err := testRepository.HasKey(hash)
		assert.NoError(err)
		assert.False(exists)
	}
}

func insertHashsToDatabase(t *testing.T, db *badger.DB, testHashes []common.Hash) {
	for _, hash := range testHashes {
		txn := db.NewTransaction(true)
		defer txn.Discard()
		trueBytes, err := json.Marshal(true)
		if err != nil {
			t.Fatalf("setting up test case failed: %v", err)
		}
		err = txn.Set([]byte(transaction.KeyPrefix+hash.String()), trueBytes)
		if err != nil {
			t.Fatalf("setting up test case failed: %v", err)
		}
		if err := txn.Commit(); err != nil {
			t.Fatalf("setting up test case failed: %v", err)
		}
	}
}

func searchHashInDatabase(t *testing.T, db *badger.DB, keyPrefix string, hash common.Hash) (bool, error) {
	var exists bool
	err := db.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(keyPrefix + hash.String()))
		if err != nil {
			return err
		}
		err = item.Value(func(v []byte) error {
			err := json.Unmarshal(v, &exists)
			return err
		})
		return err
	})
	if err != nil {
		if err == badger.ErrKeyNotFound {
			return false, nil
		}
		return false, err
	}
	return exists, nil
}
