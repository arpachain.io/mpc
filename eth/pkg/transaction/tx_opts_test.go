package transaction_test

import (
	"context"
	"math/big"
	"testing"

	mock_abstract "gitlab.com/arpachain/mpc/eth/mock/abstract"
	"gitlab.com/arpachain/mpc/eth/pkg/transaction"
	"gitlab.com/arpachain/mpc/eth/pkg/util"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

var (
	defaultGasPrice    = big.NewInt(12345)
	defaultNonce       = big.NewInt(555)
	customizedGasLimit = uint64(54321)
)

func TestGenerateDefaultOpts(t *testing.T) {
	assert := assert.New(t)
	ctrl := gomock.NewController(t)
	client := mock_abstract.NewMockClientForTxOptsGenerator(ctrl)
	setMockClientForTxOptsGeneratorBehavior_valid(client, defaultGasPrice, defaultNonce)

	sk, err := crypto.GenerateKey()
	assert.NoError(err)
	address, err := util.GetAddrFromSk(sk)
	assert.NoError(err)
	g := transaction.NewOptsGenerator(sk, client)
	value := new(big.Int).Exp(big.NewInt(10), big.NewInt(18), nil) // 10^18
	opts, err := g.GenerateDefaultOpts(context.Background(), value)
	assert.NoError(err)
	assert.Equal(opts.From, address)
	assert.Equal(opts.GasPrice, defaultGasPrice)
	assert.Equal(opts.Nonce, defaultNonce)
	assert.Equal(opts.Value, value)

}

func TestGenerateOpts(t *testing.T) {
	assert := assert.New(t)
	ctrl := gomock.NewController(t)
	client := mock_abstract.NewMockClientForTxOptsGenerator(ctrl)
	setMockClientForTxOptsGeneratorBehavior_valid(client, defaultGasPrice, defaultNonce)

	sk, err := crypto.GenerateKey()
	assert.NoError(err)
	address, err := util.GetAddrFromSk(sk)
	assert.NoError(err)
	g := transaction.NewOptsGenerator(sk, client)

	value := new(big.Int).Exp(big.NewInt(10), big.NewInt(18), nil) // 10^18
	opts, err := g.GenerateOpts(context.Background(), value, customizedGasLimit)
	assert.NoError(err)
	assert.Equal(opts.From, address)
	assert.Equal(opts.GasPrice, defaultGasPrice)
	assert.Equal(opts.Nonce, defaultNonce)
	assert.Equal(opts.Value, value)
	assert.Equal(opts.GasLimit, customizedGasLimit)
}

func setMockClientForTxOptsGeneratorBehavior_valid(
	g *mock_abstract.MockClientForTxOptsGenerator,
	gasPrice *big.Int,
	pendingNonce *big.Int) {

	g.
		EXPECT().
		PendingNonceAt(gomock.Any(), gomock.Any()).
		Return(pendingNonce.Uint64(), nil).
		AnyTimes()

	g.
		EXPECT().
		SuggestGasPrice(gomock.Any()).
		Return(gasPrice, nil).
		AnyTimes()
}
