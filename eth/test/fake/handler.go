package fake

import (
	pb "gitlab.com/arpachain/mpc/eth/api"
	"gitlab.com/arpachain/mpc/eth/pkg/handler"
	"gitlab.com/arpachain/mpc/eth/pkg/model"
	"github.com/stretchr/testify/assert"
)

const (
	RequestReceived int = iota
	ComputationStarted
	ResultPosted
	WorkerListUpdated
	UpdateWorkerListBlockchainHandled
)

type fakeHandler struct {
	eventChan     chan int
	assert        *assert.Assertions
	logSigHashMap model.LogSigHashMap
}

func NewFakeHandler(
	eventChan chan int,
	assert *assert.Assertions,
	logSigHashMap model.LogSigHashMap,
) handler.Handler {
	return &fakeHandler{
		eventChan:     eventChan,
		assert:        assert,
		logSigHashMap: logSigHashMap,
	}
}

func (f *fakeHandler) HandleRequestReceivedEvent(event *model.Event) {
	f.assert.Equal(f.logSigHashMap.GetRequestReceivedSigHash(), event.Log.Topics[0])
	f.eventChan <- RequestReceived // to simulate that the event has been handled
	return
}

func (f *fakeHandler) HandleComputationStartedEvent(event *model.Event) {
	f.assert.Equal(f.logSigHashMap.GetComputationStartedSigHash(), event.Log.Topics[0])
	f.eventChan <- ComputationStarted // to simulate that the event has been handled
	return
}

func (f *fakeHandler) HandleResultPostedEvent(event *model.Event) {
	f.assert.Equal(f.logSigHashMap.GetResultPostedSigHash(), event.Log.Topics[0])
	f.eventChan <- ResultPosted // to simulate that the event has been handled
	return
}

func (f *fakeHandler) HandleWorkerListUpdatedEvent(event *model.Event) {
	f.assert.Equal(f.logSigHashMap.GetWorkerListUpdatedSigHash(), event.Log.Topics[0])
	f.eventChan <- WorkerListUpdated // to simulate that the event has been handled
	return
}

func (f *fakeHandler) HandleReportMpcResultBlockchain(req *pb.ReportMpcResultRequest) {
	return
}

func (f *fakeHandler) HandleUpdateWorkerListBlockchain(req *pb.UpdateWorkerListRequest) {
	f.eventChan <- UpdateWorkerListBlockchainHandled
	return
}
