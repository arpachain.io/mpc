package main

import (
	"context"
	"flag"
	"log"
	"math/big"
	"os"
	"time"

	"gitlab.com/arpachain/mpc/eth/pkg/account"
	"gitlab.com/arpachain/mpc/eth/pkg/model"
	contract "gitlab.com/arpachain/mpc/eth/sol"
	"gitlab.com/arpachain/mpc/eth/test"
	"gitlab.com/arpachain/mpc/eth/test/data"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
)

var (
	ethClientAddress   = flag.String("ethClient", "ws://127.0.0.1:8545", "The ip address and prot number of eth client (websocket)")
	contractAddressStr = flag.String("contract", "0x0", "The address of ArpaMpc contract")
	keyJSONPath        = flag.String("keyJSON", "./keyJSON", "Path for keyJSON")
	passphrase         = flag.String("pass", "", "passphrase for keyJSON")
	programHash        = flag.String("program", "", "program hash of the request")
)

const (
	Coordinator int = iota
	DataProviderWorker
	User
	Worker1
	Worker2
	DelegatedWorker
)

var (
	mpcRequest *test.MpcRequest
	workerList []common.Address
	accounts   []model.Account
)

func init() {

	goPath := os.Getenv("GOPATH")
	keyFile := goPath + data.RelativePath
	var err error
	accounts, err = test.LoadAccounts(keyFile)
	if err != nil {
		log.Fatal(err)
	}

	finney := new(big.Int).Exp(big.NewInt(10), big.NewInt(15), nil) // 0.001 ether
	totalFee := new(big.Int).Mul(finney, big.NewInt(4))             // 4 finney

	mpcRequest = &test.MpcRequest{
		NumWorkers:                big.NewInt(4),
		ProgramHash:               "",
		DataProviderWorkers:       []common.Address{accounts[DataProviderWorker].Address},
		DelegatedWorker:           accounts[DelegatedWorker].Address,
		FeeForDataProviderWorkers: []*big.Int{finney},
		FeePerWorker:              finney,
		TotalFee:                  totalFee,
	}

	workerList = make([]common.Address, len(accounts))
	for index, account := range accounts {
		workerList[index] = account.Address
	}
}

func main() {
	flag.Parse()
	mpcRequest.ProgramHash = *programHash

	accountRepo := account.NewRepository(*keyJSONPath, *passphrase)
	*passphrase = ""
	account, err := accountRepo.GetAccount()
	if err != nil {
		log.Fatal(err)
	}

	ethClient, err := ethclient.Dial(*ethClientAddress)
	if err != nil {
		log.Fatal(err)
	}
	contractAddress := common.HexToAddress(*contractAddressStr)
	instance, err := contract.NewContract(contractAddress, ethClient)
	if err != nil {
		log.Fatal(err)
	}

	ctx := context.Background()

	log.Println("Sending update worker list tx...")
	tx, err := test.UpdateWorkerList(ctx, ethClient, account, instance, workerList, []common.Address{})
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Update worker tx: %v\n", tx.Hash().Hex())

	log.Println("Waiting for the tx to be mined")
	time.Sleep(time.Minute)

	log.Println("Sending MPC request tx...")
	tx, err = test.SendMpcRequest(ctx, ethClient, account, instance, mpcRequest)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Mpc Request Sent, tx: %v\n", tx.Hash().Hex())

}
