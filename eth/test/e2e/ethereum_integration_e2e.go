package main

import (
	"context"
	"log"
	"math/big"
	"os"
	"os/exec"
	"reflect"
	"strings"
	"syscall"
	"time"

	pb "gitlab.com/arpachain/mpc/eth/api"
	"gitlab.com/arpachain/mpc/eth/pkg/handler"
	"gitlab.com/arpachain/mpc/eth/pkg/listener"
	"gitlab.com/arpachain/mpc/eth/pkg/model"
	"gitlab.com/arpachain/mpc/eth/pkg/parser"
	"gitlab.com/arpachain/mpc/eth/pkg/request"
	"gitlab.com/arpachain/mpc/eth/pkg/router"
	"gitlab.com/arpachain/mpc/eth/pkg/server"
	"gitlab.com/arpachain/mpc/eth/pkg/transaction"
	"gitlab.com/arpachain/mpc/eth/pkg/worker_list"
	contract "gitlab.com/arpachain/mpc/eth/sol"
	"gitlab.com/arpachain/mpc/eth/test"
	"gitlab.com/arpachain/mpc/eth/test/data"
	"gitlab.com/arpachain/mpc/eth/test/fake"
	"github.com/dgraph-io/badger"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
)

const (
	Coordinator int = iota
	DataProviderWorker
	User
	Worker1
	Worker2
	DelegatedWorker
)

var (
	programHash = "Test"
	mpcRequest  *test.MpcRequest
	accounts    []model.Account
	workerList  []common.Address
	reqRepo     request.Repository
	txRepo      transaction.Repository
	workerRepo  worker_list.Repository
)

func assertEqual(a, b interface{}) {
	if !reflect.DeepEqual(a, b) {
		log.Fatalf("assertion failed: require \n%#v\nequals to\n%#v\n", a, b)
	}
}

func setupDB() func() {
	goPath := os.Getenv("GOPATH")
	dbPath := goPath + "/src/gitlab.com/arpachain/mpc/eth/test/testdata"
	log.Println("setting up DB")
	opts := badger.DefaultOptions(dbPath)
	var err error
	badgerDB, err := badger.Open(opts)
	reqRepo = request.NewBadgerRepository(badgerDB)
	txRepo = transaction.NewBadgerRepository(badgerDB)
	workerRepo = worker_list.NewBadgerRepository(badgerDB)
	if err != nil {
		log.Fatalf("setting up DB failed: %v", err)
	}
	return func() {
		log.Println("cleaning DB")
		reqRepo = nil
		txRepo = nil
		workerRepo = nil
		badgerDB.Close()
		badgerDB = nil
		err := os.RemoveAll(dbPath)
		if err != nil {
			log.Fatalf("cleaning DB failed: %v", err)
		}
	}
}

func init() {

	goPath := os.Getenv("GOPATH")
	keyFile := goPath + data.RelativePath
	var err error
	accounts, err = test.LoadAccounts(keyFile)
	if err != nil {
		log.Fatal(err)
	}

	ether := new(big.Int).Exp(big.NewInt(10), big.NewInt(18), nil)
	totalFee := new(big.Int).Mul(ether, big.NewInt(4))

	mpcRequest = &test.MpcRequest{
		NumWorkers:                big.NewInt(4),
		ProgramHash:               programHash,
		DataProviderWorkers:       []common.Address{accounts[DataProviderWorker].Address},
		DelegatedWorker:           accounts[DelegatedWorker].Address,
		FeeForDataProviderWorkers: []*big.Int{ether},
		FeePerWorker:              ether,
		TotalFee:                  totalFee,
	}

	workerList = []common.Address{
		accounts[DataProviderWorker].Address,
		accounts[Coordinator].Address,
		accounts[Worker1].Address,
		accounts[Worker2].Address,
		accounts[DelegatedWorker].Address,
	}

}

func main() {

	cleanDB := setupDB()
	defer cleanDB()

	err := os.Chdir("..")
	if err != nil {
		log.Fatal(err)
	}
	// start the simulated ethereum node
	cmd := exec.Command("npm", "run", "ganache")
	cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
	err = cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	// clean up the simulated ethereum node
	defer func() {
		pgid, err := syscall.Getpgid(cmd.Process.Pid)
		if err == nil {
			if err := syscall.Kill(-pgid, syscall.SIGKILL); err != nil {
				log.Panic("failed to kill group processes")
			}
		}
		log.Println("Done clean up simulated ethereum node.")
	}()
	// ganache may need some time to start up
	time.Sleep(time.Second * 3)

	client, err := ethclient.Dial("ws://127.0.0.1:8545")
	if err != nil {
		log.Fatal(err)
	}
	ctx := context.Background()
	contractAddress, instance, err := test.DeployArpaMpc(ctx, client, accounts[Coordinator], accounts[Coordinator].Address)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Contract deployed")

	contractAbi, err := abi.JSON(strings.NewReader(string(contract.ContractABI)))
	if err != nil {
		log.Fatal(err)
	}

	arpaMpcClient := test.NewFakeArpaMPCClient()
	parser := parser.NewParser(contractAbi)
	optsGenerator := transaction.NewOptsGenerator(accounts[Coordinator].PrivateKey, client)
	// fake dispathcer is required because currently we cannot get response
	// from ganache-cli when calling TransactionReceipt
	dispatcher := fake.NewFakeNoConfirmationDispatcher(optsGenerator)
	handler := handler.NewHandler(arpaMpcClient, parser, instance, reqRepo, workerRepo, dispatcher)
	router := router.NewRouter(handler, model.NewLogSigHashMap(), txRepo)
	listener := listener.NewListener(client, contractAddress, router)
	blochchainAdapterServer := server.NewBlochchainAdapterServer(handler)

	go listener.Start(ctx, 5)

	// update worker list on chain / at listener
	workerAddresses := make([]string, len(workerList))
	for index, worker := range workerList {
		workerAddresses[index] = worker.Hex()
	}
	req := &pb.UpdateWorkerListRequest{
		WorkerAddresses: workerAddresses,
	}
	log.Println("Calling server to update worker list...")
	res, err := blochchainAdapterServer.UpdateWorkerList(ctx, req)
	if err != nil {
		log.Fatal(err)
	}
	if !res.Success {
		log.Fatalln("Update worker list failed")
	}
	time.Sleep(time.Millisecond * 500) // update worker list may need some time

	// check for worker list from smart contract
	for i := 0; i < len(workerList); i++ {
		worker, err := instance.WorkerList(nil, big.NewInt(int64(i)))
		if err != nil {
			log.Fatal(err)
		}
		assertEqual(worker, workerList[i])
	}
	log.Println("Eth e2e: Worker list updated")

	// send several dummy requests
	for i := 0; i < 3; i++ {
		_, err = test.SendMpcRequest(ctx, client, accounts[User], instance, mpcRequest)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("Mpc Request %d Sent\n", i)
		go func() {
			mpcResult := &pb.MpcResult{
				Data: "mpc_result_from_fake_ArpaMpcServer",
			}
			mpcResults := make([]*pb.MpcResult, 0)
			mpcResults = append(mpcResults, mpcResult)
			req := &pb.ReportMpcResultRequest{
				MpcId:  "Test MpcId",
				Result: mpcResults,
			}
			time.Sleep(time.Millisecond * 500) // simulate Run Mpc process
			blochchainAdapterServer.ReportMpcResult(context.Background(), req)
		}()
		time.Sleep(time.Millisecond * 1500)

		// check for final request mpc struct from smart contract
		result, err := instance.Requests(nil, big.NewInt(int64(i)))
		if err != nil {
			log.Fatal(err)
		}

		expectedResult := struct {
			Id                     *big.Int
			NumWorkers             *big.Int
			ProgramHash            string
			DataConsumer           common.Address
			DelegatedWorker        common.Address
			FeePerAdditionalWorker *big.Int
			TotalFee               *big.Int
			ReceivedTime           *big.Int
			CurrentStage           uint8
		}{
			big.NewInt(int64(i)),
			mpcRequest.NumWorkers,
			mpcRequest.ProgramHash,
			accounts[User].Address,
			mpcRequest.DelegatedWorker,
			mpcRequest.FeePerWorker,
			mpcRequest.TotalFee,
			result.ReceivedTime,
			2, // Stage.Done
		}
		if i != 0 { // a 0 *big.Int is not equal to it self
			assertEqual(result, expectedResult)
		} else {
			assertEqual(result.Id.Int64(), expectedResult.Id.Int64())
			assertEqual(result.NumWorkers, expectedResult.NumWorkers)
			assertEqual(result.ProgramHash, expectedResult.ProgramHash)
			assertEqual(result.DataConsumer, expectedResult.DataConsumer)
			assertEqual(result.DelegatedWorker, expectedResult.DelegatedWorker)
			assertEqual(result.FeePerAdditionalWorker, expectedResult.FeePerAdditionalWorker)
			assertEqual(result.TotalFee, expectedResult.TotalFee)
			assertEqual(result.CurrentStage, expectedResult.CurrentStage)
		}
	}

	log.Println("Done e2e")

}
