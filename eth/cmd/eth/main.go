package main

import (
	"context"
	"flag"
	"log"
	"net"
	"strings"
	"time"

	pb "gitlab.com/arpachain/mpc/eth/api"
	"gitlab.com/arpachain/mpc/eth/pkg/account"
	"gitlab.com/arpachain/mpc/eth/pkg/handler"
	"gitlab.com/arpachain/mpc/eth/pkg/listener"
	"gitlab.com/arpachain/mpc/eth/pkg/model"
	"gitlab.com/arpachain/mpc/eth/pkg/parser"
	"gitlab.com/arpachain/mpc/eth/pkg/request"
	"gitlab.com/arpachain/mpc/eth/pkg/router"
	"gitlab.com/arpachain/mpc/eth/pkg/server"
	"gitlab.com/arpachain/mpc/eth/pkg/transaction"
	"gitlab.com/arpachain/mpc/eth/pkg/worker_list"
	contract "gitlab.com/arpachain/mpc/eth/sol"
	"github.com/dgraph-io/badger"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const (
	infuraHeartbeatInterval = 15 * time.Minute
	retryInterval           = time.Minute
	retryTimes              = 3
)

var (
	address             = flag.String("address", "127.0.0.1:40000", "The ip address and port number of the blochchainAdapter server")
	arpaMpcAddress      = flag.String("arpaMpcAddress", "127.0.0.1:10000", "The ip address and port number of the ArpaMpc server")
	ethClientAddress    = flag.String("ethClient", "ws://127.0.0.1:8545", "The ip address and prot number of eth client (websocket)")
	contractAddressStr  = flag.String("contract", "0x0", "The address of ArpaMpc contract")
	dbPath              = flag.String("db", "./db", "Path for database")
	keyJSONPath         = flag.String("keyJSON", "./keyJSON", "Path for keyJSON")
	passphrase          = flag.String("pass", "", "passphrase for keyJSON")
	numConfirmations    = flag.Int64("confirmations", 3, "Number of confirmation required for a tx to be considered mined")
	sendInfuraHeartbeat = flag.Bool("infura_heartbeat", true, "If true, send heartbeat message to infura in order to avoid the infura server closes connection. Default to true.")
	reqRepo             request.Repository
	txRepo              transaction.Repository
	workerRepo          worker_list.Repository
)

func setupDB(dbPath string) func() {
	log.Println("Eth: setting up DB")
	opts := badger.DefaultOptions(dbPath)
	var err error
	badgerDB, err := badger.Open(opts)
	reqRepo = request.NewBadgerRepository(badgerDB)
	txRepo = transaction.NewBadgerRepository(badgerDB)
	workerRepo = worker_list.NewBadgerRepository(badgerDB)
	if err != nil {
		log.Fatalf("Eth: setting up DB failed: %v", err)
	}
	return func() {
		log.Println("Eth: closing DB...")
		reqRepo = nil
		txRepo = nil
		workerRepo = nil
		badgerDB.Close()
		badgerDB = nil
	}
}

func main() {
	flag.Parse()
	lis, err := net.Listen("tcp", *address)

	if err != nil {
		log.Fatalf("Eth: Failed to listen: %v", err)
	}

	log.Printf("Eth: Server listening on : %v", *address)
	s := grpc.NewServer()

	// Dependency composition:
	closeDB := setupDB(*dbPath)
	defer closeDB()

	log.Println("Eth: loading account...")
	accountRepo := account.NewRepository(*keyJSONPath, *passphrase)
	*passphrase = ""
	account, err := accountRepo.GetAccount()
	if err != nil {
		log.Fatalf("Eth: err: %v", err)
	}

	log.Println("Eth: connecting to web3 endpoint...")
	ethClient, err := ethclient.Dial(*ethClientAddress)
	if err != nil {
		log.Fatalf("Eth: err: %v", err)
	}
	ctx := context.Background() // TODO(John): use the right ctx
	// heartbeat when using infura
	if *sendInfuraHeartbeat {
		go func() {
			for {
				time.Sleep(infuraHeartbeatInterval)
				// fail and retry to ensure the heartbeat is sent
				for i := 0; i < retryTimes; i++ {
					log.Println("Eth: sending a heartbeat to Infura...")
					_, err := ethClient.HeaderByNumber(ctx, nil)
					if err != nil {
						log.Println("Eth: warning: failed to send a heartbeat to Infura")
						time.Sleep(retryInterval)
						continue
					}
					log.Println("Eth: heartbeat to Infura sent")
					break
				}
			}
		}()
	}

	contractAddress := common.HexToAddress(*contractAddressStr)
	instance, err := contract.NewContract(contractAddress, ethClient)
	if err != nil {
		log.Fatalf("Eth: err: %v", err)
	}

	contractAbi, err := abi.JSON(strings.NewReader(string(contract.ContractABI)))
	if err != nil {
		log.Fatalf("Eth: err: %v", err)
	}
	parser := parser.NewParser(contractAbi)

	log.Println("Eth: starting ArpaMpc client...")
	conn, err := grpc.Dial(*arpaMpcAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Eth: failed to start gRPC connection: %v", err)
	}
	log.Printf("Eth: created gRPC connection to server at %s", *arpaMpcAddress)
	arpaMpcClient := pb.NewArpaMpcClient(conn)
	defer conn.Close()

	optsGenerator := transaction.NewOptsGenerator(account.PrivateKey, ethClient)
	dispatcher := transaction.NewDispatcher(
		ethClient,
		optsGenerator,
		*numConfirmations, /* number of tx confirmations */
		5*time.Second,     /* polling interval before tx mined */
		30*time.Second,    /* polling interval before tx confirmed */
	)
	handler := handler.NewHandler(arpaMpcClient, parser, instance, reqRepo, workerRepo, dispatcher)
	router := router.NewRouter(handler, model.NewLogSigHashMap(), txRepo)
	listener := listener.NewListener(ethClient, contractAddress, router)

	log.Println("Eth: starting listener...")
	go listener.Start(ctx, 5 /*bufferSize*/)

	blochchainAdapterServer := server.NewBlochchainAdapterServer(handler)
	pb.RegisterBlochchainAdapterServer(s, blochchainAdapterServer)
	reflection.Register(s)
	log.Println("Eth: starting blochchainAdapter server...")
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Eth: Failed to serve: %v", err)
	}
}
