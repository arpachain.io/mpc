#!/bin/bash

set -ex

# Clean up all existing processes.
pkill datagateway.x || true
pkill coordinator.x || true
pkill worker.x || true

# hardcode port number for docker
FREE_PORT="4572"

echo $FREE_PORT

./coordinator/coordinator.x -address localhost:$FREE_PORT &

./datagateway/datagateway.x --address localhost:11000 --dataset Test1 &
./datagateway/datagateway.x --address localhost:11001 --dataset Test2 &
./datagateway/datagateway.x --address localhost:11002 --dataset Test3 &

# Sleep a while to wait for the servers to be ready.
sleep 2

cd worker
./compile.py Programs/demo
./worker.x --coordinator_addr=localhost:$FREE_PORT -rpc_addr=localhost:6000 -datagateway_addr=localhost:11000 -csr_path="configure/Worker_0.csr" -crt_path="configure/Worker_0.crt" -private_key_path="configure/Worker_0_pri.key" -public_key_path="configure/Worker_0_pub.key" &
./worker.x --coordinator_addr=localhost:$FREE_PORT -rpc_addr=localhost:6001 -datagateway_addr=localhost:11001 -csr_path="configure/Worker_1.csr" -crt_path="configure/Worker_1.crt" -private_key_path="configure/Worker_1_pri.key" -public_key_path="configure/Worker_1_pub.key" &
./worker.x --coordinator_addr=localhost:$FREE_PORT -rpc_addr=localhost:6002 -datagateway_addr=localhost:11002 -csr_path="configure/Worker_2.csr" -crt_path="configure/Worker_2.crt" -private_key_path="configure/Worker_2_pri.key" -public_key_path="configure/Worker_2_pub.key" &

sleep 3

grpc_cli call localhost:$FREE_PORT arpa_mpc.ArpaMpc.RunMpc 'num_nodes:3 program_hash:"4a9016baa5b0a199a328b6ca2df9d1587aa95e954a6ec52b1a36e42346bc644c"'

# Clean up all existing processes.
pkill datagateway.x || true
pkill coordinator.x || true
pkill worker.x || true
