package server_test

import (
	"context"
	"os"
	"testing"

	pb "gitlab.com/arpachain/mpc/datagateway/api"
	"gitlab.com/arpachain/mpc/datagateway/internal/data"
	"gitlab.com/arpachain/mpc/datagateway/internal/server"
	"github.com/stretchr/testify/assert"
)

const (
	relativePathTestData = "/src/gitlab.com/arpachain/mpc/datagateway/internal/data/test_data/mpc_data.json"
	dataSetName          = "Test1"
)

func TestGetData(t *testing.T) {
	assert := assert.New(t)
	goPath := os.Getenv("GOPATH")
	repository := data.NewRepository(goPath + relativePathTestData)
	server := server.NewDataGatewayServer(repository, dataSetName)

	t.Log("case 1: testing GetData happy path...")
	fakeCTX := context.Background()
	mockRequest := &pb.GetDataRequest{
		Key: int64(1),
	}
	response, err := server.GetData(fakeCTX, mockRequest)
	assert.NoError(err)
	assert.NotEmpty(response)
	assert.Equal(int64(10001), response.Result)

	t.Log("case 2: testing GetData with an invalid key...")
	invalidRequest := &pb.GetDataRequest{
		Key: int64(11111),
	}
	response, err = server.GetData(fakeCTX, invalidRequest)
	assert.Error(err)
	assert.Empty(response)
}

func TestGetDataInvalidDataSetName(t *testing.T) {
	assert := assert.New(t)
	goPath := os.Getenv("GOPATH")
	repository := data.NewRepository(goPath + relativePathTestData)
	server := server.NewDataGatewayServer(repository, "invalid_data_set_name")

	t.Log("case 3: testing GetData happy path...")
	fakeCTX := context.Background()
	mockRequest := &pb.GetDataRequest{
		Key: int64(1),
	}
	response, err := server.GetData(fakeCTX, mockRequest)
	assert.Error(err)
	assert.Empty(response)
}

func TestGetAllData(t *testing.T) {
	assert := assert.New(t)
	goPath := os.Getenv("GOPATH")
	repository := data.NewRepository(goPath + relativePathTestData)
	server := server.NewDataGatewayServer(repository, dataSetName)

	t.Log("case 1: testing GetAllData happy path...")
	fakeCTX := context.Background()
	mockRequest := &pb.GetAllDataRequest{}
	response, err := server.GetAllData(fakeCTX, mockRequest)
	assert.NoError(err)
	assert.NotEmpty(response)
	assert.Equal(int64(10001), response.Result[int64(1)])
	assert.Equal(int64(20001), response.Result[int64(2)])
	assert.Equal(int64(30001), response.Result[int64(3)])
}

func TestGetAllDataInvalidDataSetName(t *testing.T) {
	assert := assert.New(t)
	goPath := os.Getenv("GOPATH")
	repository := data.NewRepository(goPath + relativePathTestData)
	server := server.NewDataGatewayServer(repository, "invalid_data_set_name")

	t.Log("case 2: testing GetAllData with an invalid data set name...")
	fakeCTX := context.Background()
	mockRequest := &pb.GetAllDataRequest{}
	response, err := server.GetAllData(fakeCTX, mockRequest)
	assert.Error(err)
	assert.Empty(response)
}
