package data

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"sync"
)

// Repository for data provides CRUD operations to persist MPC data
// (data is currently only persisted in memory, not to file)
type Repository interface {
	GetAll() ([]Data, error)
	GetByName(name string) (Data, error)
	Create(data *Data) (string, error)
	Update(name string, rawData map[int64]int64) error
	Remove(name string)
}

// reference implementation:

type repository struct {
	mu      *sync.Mutex
	dataMap map[string]Data
}

// NewRepository returns an instance of data.Repository
func NewRepository(path string) Repository {
	dataMap := loadDataFromFile(path)
	return &repository{
		mu:      &sync.Mutex{},
		dataMap: dataMap,
	}
}

func (r *repository) GetAll() ([]Data, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	dataList := make([]Data, 0)
	for _, data := range r.dataMap {
		dataList = append(dataList, data)
	}
	return dataList, nil
}

func (r *repository) GetByName(name string) (Data, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	if data, exists := r.dataMap[name]; exists {
		return data, nil
	}
	return Data{}, fmt.Errorf("data with name: %v does not exist", name)
}

func (r *repository) Create(data *Data) (string, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	name := data.Name
	if _, exists := r.dataMap[name]; exists {
		return "", errors.New("data with same name already exists")
	}
	r.dataMap[name] = Data{
		Name:    name,
		RawData: data.RawData,
	}
	return name, nil
}

func (r *repository) Update(name string, rawData map[int64]int64) error {
	r.mu.Lock()
	defer r.mu.Unlock()
	if _, exists := r.dataMap[name]; exists {
		r.dataMap[name] = Data{
			Name:    name,
			RawData: rawData,
		}
		return nil
	}
	return fmt.Errorf("data with name: %v does not exist", name)
}

func (r *repository) Remove(name string) {
	r.mu.Lock()
	defer r.mu.Unlock()
	delete(r.dataMap, name)
}

// Using Panic because the data source is a hard dependency of data gateway
func loadDataFromFile(path string) map[string]Data {
	jsonBytes, err := ioutil.ReadFile(path)
	if err != nil {
		log.Panicf("Failed to read data file: %v", err)
		return nil
	}
	dataList := make([]Data, 0)
	err = json.Unmarshal(jsonBytes, &dataList)
	if err != nil {
		log.Panicf("Failed to parse json string from file: %v", err)
		return nil
	}
	dataMap := make(map[string]Data)
	for _, data := range dataList {
		if _, exists := dataMap[data.Name]; exists {
			log.Panicf("please make sure all the names in your data file is unique. Duplicated name: %v", data.Name)
			return nil
		}
		dataMap[data.Name] = data
	}
	return dataMap
}
