package data_test

import (
	"os"
	"testing"

	"gitlab.com/arpachain/mpc/datagateway/internal/data"

	"github.com/stretchr/testify/assert"
)

const (
	relativePathTestData = "/src/gitlab.com/arpachain/mpc/datagateway/internal/data/test_data/mpc_data.json"
)

func TestGetByName(t *testing.T) {
	assert := assert.New(t)
	goPath := os.Getenv("GOPATH")
	repository := data.NewRepository(goPath + relativePathTestData)

	t.Log("case 1: testing GetByName happy path...")
	mpcData, err := repository.GetByName("Test1")
	assert.NoError(err)
	assert.NotEmpty(mpcData)
	assert.Equal(mpcData.RawData[1], int64(10001))
	assert.Equal(mpcData.RawData[2], int64(20001))
	assert.Equal(mpcData.RawData[3], int64(30001))

	t.Log("case 2: testing GetByName with an invalid name")
	mpcData, err = repository.GetByName("invalid_name")
	assert.Error(err)
	assert.Empty(mpcData)
}

func TestGetAll(t *testing.T) {
	assert := assert.New(t)
	goPath := os.Getenv("GOPATH")
	repository := data.NewRepository(goPath + relativePathTestData)

	t.Log("case 1: testing GetAll happy path...")
	mpcDataList, err := repository.GetAll()
	mpcData := mpcDataList[0]
	assert.NoError(err)
	assert.NotEmpty(mpcDataList)
	assert.Equal(len(mpcDataList), 5)
	assert.NotEmpty(mpcData)
	assert.NotEmpty(mpcData.RawData)
}

func TestCreate(t *testing.T) {
	assert := assert.New(t)
	goPath := os.Getenv("GOPATH")
	repository := data.NewRepository(goPath + relativePathTestData)

	t.Log("case 1: testing Create happy path...")
	oldDataList, _ := repository.GetAll()
	assert.Equal(len(oldDataList), 5, "originally there is 5 mpc data")

	newDataName := "NewData1"
	newRawData := make(map[int64]int64)
	newRawData[int64(1)] = int64(10006)
	newRawData[int64(2)] = int64(20006)
	newRawData[int64(3)] = int64(30006)
	newMPCData := &data.Data{
		Name:    newDataName,
		RawData: newRawData,
	}
	name, err := repository.Create(newMPCData)
	assert.NoError(err)
	assert.NotEmpty(name)
	assert.Equal(name, newDataName)
	newDataList, err := repository.GetAll()
	assert.NoError(err)
	assert.NotEmpty(newDataList)
	assert.Equal(len(newDataList), 6, "now there should be 6 mpc data")
	newData, err := repository.GetByName(name)
	assert.NoError(err)
	assert.NotEmpty(newData)
	assert.Equal(newData.Name, newDataName)
	assert.Equal(newData.RawData[1], int64(10006))
	assert.Equal(newData.RawData[2], int64(20006))
	assert.Equal(newData.RawData[3], int64(30006))

	t.Log("case 2: testing Create with same name...")
	dupeNameData := &data.Data{
		Name:    "Test1",
		RawData: make(map[int64]int64),
	}
	name, err = repository.Create(dupeNameData)
	assert.Error(err)
	assert.Empty(name)
}

func TestUpdate(t *testing.T) {
	assert := assert.New(t)
	goPath := os.Getenv("GOPATH")
	repository := data.NewRepository(goPath + relativePathTestData)

	t.Log("case 1: testing Update happy path...")
	dataToUpdate, _ := repository.GetByName("Test1")
	newRawData := make(map[int64]int64)
	newRawData[int64(1)] = int64(1)
	newRawData[int64(2)] = int64(2)
	newRawData[int64(3)] = int64(3)
	assert.NotEqual(dataToUpdate.RawData[int64(1)], newRawData[int64(1)])
	assert.NotEqual(dataToUpdate.RawData[int64(2)], newRawData[int64(2)])
	assert.NotEqual(dataToUpdate.RawData[int64(3)], newRawData[int64(3)])
	err := repository.Update(dataToUpdate.Name, newRawData)
	assert.NoError(err)
	updatedData, err := repository.GetByName(dataToUpdate.Name)
	assert.NoError(err)
	assert.NotEmpty(updatedData)
	assert.Equal(updatedData.RawData[int64(1)], newRawData[int64(1)])
	assert.Equal(updatedData.RawData[int64(2)], newRawData[int64(2)])
	assert.Equal(updatedData.RawData[int64(3)], newRawData[int64(3)])

	t.Log("case 2: testing Update with an invalid name...")
	err = repository.Update("invalid_name", newRawData)
	assert.Error(err)
}

func TestRemove(t *testing.T) {
	assert := assert.New(t)
	goPath := os.Getenv("GOPATH")
	repository := data.NewRepository(goPath + relativePathTestData)

	t.Log("case 1: testing Remove happy path...")
	// ensure the data exists
	mpcData, err := repository.GetByName("Test1")
	assert.NoError(err)
	assert.NotEmpty(mpcData)
	// try to remove it
	repository.Remove(mpcData.Name)
	emptyData, err := repository.GetByName(mpcData.Name)
	assert.Error(err)
	assert.Empty(emptyData)
}

func TestLoadDataFromFileInvalidPath(t *testing.T) {
	assert := assert.New(t)
	goPath := os.Getenv("GOPATH")
	assert.Panics(func() { data.NewRepository(goPath + relativePathTestData + "corrupt_path") })
}

func TestLoadDataFromFileInvalidJSONFile(t *testing.T) {
	assert := assert.New(t)
	goPath := os.Getenv("GOPATH")
	assert.Panics(func() { data.NewRepository(goPath + relativePathTestData + ".corrupted") })
}

func TestLoadDataFromFileDuplicatedDataName(t *testing.T) {
	assert := assert.New(t)
	goPath := os.Getenv("GOPATH")
	assert.Panics(func() { data.NewRepository(goPath + relativePathTestData + ".duplicated") })
}
