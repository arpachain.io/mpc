#!/bin/bash

set -ex
cd $GOPATH/src/gitlab.com/arpachain/mpc/datagateway/cmd/datagateway/
go run main.go
