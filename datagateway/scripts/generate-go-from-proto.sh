#!/bin/bash

set -ex

API_DIR=$GOPATH/src/gitlab.com/arpachain/mpc/datagateway/api
mkdir -p $API_DIR
protoc -I $GOPATH/src/gitlab.com/arpachain/mpc/proto/ $GOPATH/src/gitlab.com/arpachain/mpc/proto/arpa_mpc.proto --go_out=plugins=grpc:$API_DIR
