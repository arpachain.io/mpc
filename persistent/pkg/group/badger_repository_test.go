package group_test

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"testing"

	"gitlab.com/arpachain/mpc/coordinator/pkg/group"
	badger_group "gitlab.com/arpachain/mpc/persistent/pkg/group"
	"github.com/dgraph-io/badger"
	"github.com/stretchr/testify/assert"
)

const (
	numGroups = 3
	keyPrefix = "group_"
)

var (
	dbPath           string
	badgerDB         *badger.DB
	repositoryToTest group.Repository
	expectedGroups   []group.Group
)

func TestMain(m *testing.M) {
	setDBPath()
	retCode := m.Run()
	os.Exit(retCode)
}

func setDBPath() {
	goPath := os.Getenv("GOPATH")
	dbPath = goPath + "/src/gitlab.com/arpachain/mpc/persistent/test/temp_db_group"
}

type setupTestCase func(t *testing.T, db *badger.DB, expectedGroups []group.Group)

func setup(t *testing.T, numGroups int, fns ...setupTestCase) func(t *testing.T) {
	log.Println("setting up test...")
	opts := badger.DefaultOptions(dbPath)
	var err error
	badgerDB, err = badger.Open(opts)
	repositoryToTest = badger_group.NewBadgerRepository(badgerDB)
	if err != nil {
		t.Fatalf("setting up test failed: %v", err)
	}
	expectedGroups = generateExpectedGroups(numGroups)
	// calls setupTestCase functions if passed in
	for _, fn := range fns {
		fn(t, badgerDB, expectedGroups)
	}
	return func(t *testing.T) {
		log.Println("tearing down test...")
		repositoryToTest = nil
		expectedGroups = nil
		badgerDB.Close()
		badgerDB = nil
		err := os.RemoveAll(dbPath)
		if err != nil {
			t.Fatalf("tearing down test failed: %v", err)
		}
	}
}

func TestGetByID(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, 1, createGroupsToDatabase)
	defer tearDown(t)

	expectedGroup := expectedGroups[0]
	group, err := repositoryToTest.GetByID(expectedGroup.ID)
	assert.NoError(err)
	assert.NotEmpty(group)
	assert.Equal(expectedGroup.ID, group.ID)
	assert.Equal(expectedGroup.IsBusy, group.IsBusy)
	assert.Equal(expectedGroup.NodeIDs, group.NodeIDs)
}

func TestGetByID_InvalidID(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, 1)
	defer tearDown(t)

	group, err := repositoryToTest.GetByID("invalid_id")
	assert.Error(err)
	assert.Empty(group)
}

func TestGetAll(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, numGroups, createGroupsToDatabase)
	defer tearDown(t)

	groups, err := repositoryToTest.GetAll()
	assert.NoError(err)
	assert.NotEmpty(groups)
	assert.Equal(len(expectedGroups), len(groups))
	for i := 0; i < numGroups; i++ {
		assert.Contains(groups, expectedGroups[i])
	}
}

func TestCreate(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, 1)
	defer tearDown(t)

	expectedGroup := expectedGroups[0]
	id, err := repositoryToTest.Create(&expectedGroup)
	assert.NoError(err)
	assert.Equal(expectedGroup.ID, id)
	createdGroup, err := getGroupFromDatabase(t, badgerDB, keyPrefix, expectedGroup.ID)
	if err != nil {
		t.Fatalf("retriving group from database failed: %v", err)
	}
	assert.Equal(expectedGroup.ID, createdGroup.ID)
	assert.Equal(expectedGroup.IsBusy, createdGroup.IsBusy)
	assert.Equal(expectedGroup.NodeIDs, createdGroup.NodeIDs)
}

func TestRemove(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, 1, createGroupsToDatabase)
	defer tearDown(t)

	expectedGroup := expectedGroups[0]
	currentGroup, err := getGroupFromDatabase(t, badgerDB, keyPrefix, expectedGroup.ID)
	if err != nil {
		t.Fatalf("retriving group from database failed: %v", err)
	}
	assert.NotEmpty(currentGroup)
	repositoryToTest.Remove(expectedGroup.ID)
	currentGroup, err = getGroupFromDatabase(t, badgerDB, keyPrefix, expectedGroup.ID)
	assert.Error(err)
	assert.Empty(currentGroup)
}

func TestUpdateIsBusy(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, 1, createGroupsToDatabase)
	defer tearDown(t)

	expectedGroup := expectedGroups[0]
	err := repositoryToTest.UpdateIsBusy(expectedGroup.ID, true)
	assert.NoError(err)
	updatedGroup, err := getGroupFromDatabase(t, badgerDB, keyPrefix, expectedGroup.ID)
	if err != nil {
		t.Fatalf("retriving group from database failed: %v", err)
	}
	assert.Equal(true, updatedGroup.IsBusy)
}

func TestUpdateIsBusy_InvalidSameState(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, 1, createGroupsToDatabase)
	defer tearDown(t)

	expectedGroup := expectedGroups[0]
	err := repositoryToTest.UpdateIsBusy(expectedGroup.ID, false)
	assert.Error(err)
}

func TestUpdateNodeIDs(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, 1, createGroupsToDatabase)
	defer tearDown(t)

	expectedGroup := expectedGroups[0]
	updatedNodeIDs := []string{"updated_fake_node_1", "updated_fake_node_2", "updated_fake_node_3"}

	err := repositoryToTest.UpdateNodeIDs(expectedGroup.ID, updatedNodeIDs)
	assert.NoError(err)
	updatedGroup, err := getGroupFromDatabase(t, badgerDB, keyPrefix, expectedGroup.ID)
	if err != nil {
		t.Fatalf("retriving group from database failed: %v", err)
	}
	assert.Contains(updatedGroup.NodeIDs[0], "updated")
	assert.Contains(updatedGroup.NodeIDs[1], "updated")
	assert.Contains(updatedGroup.NodeIDs[2], "updated")
}

func TestUpdateNodeIDs_InvalidEmptyNodeIDs(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, 1, createGroupsToDatabase)
	defer tearDown(t)

	expectedGroup := expectedGroups[0]
	emptyNodeIDs := make([]string, 0)

	err := repositoryToTest.UpdateNodeIDs(expectedGroup.ID, emptyNodeIDs)
	assert.Error(err)
}

func TestGetAvailableGroup(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, 1, createGroupsToDatabase)
	defer tearDown(t)

	availableGroup, err := repositoryToTest.GetAvailableGroup(3)
	assert.NoError(err)
	assert.NotEmpty(availableGroup)
}

func generateExpectedGroups(numGroups int) []group.Group {
	groups := make([]group.Group, 0)
	for i := 0; i < numGroups; i++ {
		group := generateExpectedGroup(i)
		groups = append(groups, group)
	}
	return groups
}

func TestGetAvailableGroup_NoAvailableGroup(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, 1, createGroupsToDatabase)
	defer tearDown(t)

	availableGroup, err := repositoryToTest.GetAvailableGroup(-1)
	assert.Error(err)
	assert.Empty(availableGroup)
}

func generateExpectedGroup(index int) group.Group {
	return group.Group{
		ID:      fmt.Sprintf("eth-address-%d", index),
		NodeIDs: []string{"fake_node_1", "fake_node_2", "fake_node_3"},
		IsBusy:  false,
	}
}

func createGroupsToDatabase(t *testing.T, db *badger.DB, expectedGroups []group.Group) {
	for _, group := range expectedGroups {
		txn := db.NewTransaction(true)
		defer txn.Discard()
		groupBytes, err := json.Marshal(group)
		if err != nil {
			t.Fatalf("setting up test case failed: %v", err)
		}
		err = txn.Set([]byte(keyPrefix+group.ID), groupBytes)
		if err != nil {
			t.Fatalf("setting up test case failed: %v", err)
		}
		if err := txn.Commit(); err != nil {
			t.Fatalf("setting up test case failed: %v", err)
		}
	}
}

func getGroupFromDatabase(t *testing.T, db *badger.DB, keyPrefix, id string) (group.Group, error) {
	var g group.Group
	err := db.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(keyPrefix + id))
		if err != nil {
			return err
		}
		err = item.Value(func(v []byte) error {
			err := json.Unmarshal(v, &g)
			return err
		})
		return err
	})
	if err != nil {
		return group.Group{}, err
	}
	return g, nil
}

func updateGroupToDatabase(t *testing.T, db *badger.DB, keyPrefix string, g group.Group) {
	groupBytes, err := json.Marshal(g)
	if err != nil {
		t.Fatalf("marshalling group failed: %v", err)
	}
	err = db.Update(func(txn *badger.Txn) error {
		err := txn.Set([]byte(keyPrefix+g.ID), groupBytes)
		return err
	})
	if err != nil {
		t.Fatalf("updating group to database failed: %v", err)
	}
}
