package db_utils

import (
	"encoding/json"
	"fmt"
	"reflect"

	"github.com/dgraph-io/badger"
)

// GetObject retrives object using keyPrefix+id then uses receivingPointer to identify the object type it should unmarshal to
// receivingPointer will then point to the value of the retrived object
// for return values:
// bool: true when the id (key) is found, false otherwise
// error: errors occurred during retrival process, nil if there is no error
func GetObject(db *badger.DB, keyPrefix, id string, receivingPointer interface{}) (bool, error) {
	err := db.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(keyPrefix + id))
		if err != nil {
			return err
		}
		err = item.Value(func(v []byte) error {
			err := json.Unmarshal(v, receivingPointer)
			return err
		})
		return err
	})
	if err == badger.ErrKeyNotFound {
		return false, nil
	}
	if err != nil {
		return false, err
	}
	return true, nil
}

// GetAllObjects retrives all objects with the keys of a specific prefix
// it uses typePointer to identify the object type it should unmarshal to
func GetAllObjects(db *badger.DB, keyPrefix string, typePointer interface{}) ([]interface{}, error) {
	var receivingPointers []interface{}
	err := db.View(func(txn *badger.Txn) error {
		it := txn.NewIterator(badger.DefaultIteratorOptions)
		defer it.Close()
		prefix := []byte(keyPrefix)
		for it.Seek(prefix); it.ValidForPrefix(prefix); it.Next() {
			item := it.Item()
			err := item.Value(func(v []byte) error {
				copiedPtr := reflect.New(reflect.ValueOf(typePointer).Elem().Type()).Interface()
				err := json.Unmarshal(v, copiedPtr)
				if err != nil {
					return err
				}
				receivingPointers = append(receivingPointers, copiedPtr)
				return nil
			})
			if err != nil {
				return err
			}
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return receivingPointers, nil
}

// CreateObject creates a mapping (keyPrefix+id => object) into the database
func CreateObject(db *badger.DB, keyPrefix, id string, object []byte) error {
	txn := db.NewTransaction(true)
	defer txn.Discard()
	err := txn.Set([]byte(keyPrefix+id), object)
	if err != nil {
		return fmt.Errorf("create object failed: %v", err)
	}
	err = txn.Commit()
	if err != nil {
		return fmt.Errorf("create object failed: %v", err)
	}
	return nil
}

// UpdateObject uses keyPrefix+id to find an existing mapping and updates its value to object
func UpdateObject(db *badger.DB, keyPrefix, id string, object []byte) error {
	return db.Update(func(txn *badger.Txn) error {
		err := txn.Set([]byte(keyPrefix+id), object)
		return err
	})
}

// DeleteObject uses keyPrefix+id to delete an existing mapping
func DeleteObject(db *badger.DB, keyPrefix, id string) error {
	txn := db.NewTransaction(true)
	defer txn.Discard()
	err := txn.Delete([]byte(keyPrefix + id))
	if err != nil {
		return fmt.Errorf("delete object failed for key: %v with error: %v", keyPrefix+id, err)
	}
	err = txn.Commit()
	if err != nil {
		return fmt.Errorf("delete object failed for key: %v with error: %v", keyPrefix+id, err)
	}
	return nil
}
