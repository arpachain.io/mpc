package mpc_test

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"testing"

	// TODO(bomo): There is a dependency issue needs fixed
	// where there are multiple copies of golang.org/x/net/trace
	// one in $GOPATH, one in each vendor folder
	// currently the manual workaround is to manually delete
	// golang.org/x/net/trace in vendor folders
	"gitlab.com/arpachain/mpc/coordinator/api"
	"gitlab.com/arpachain/mpc/coordinator/pkg/mpc"
	badger_mpc "gitlab.com/arpachain/mpc/persistent/pkg/mpc"
	"github.com/dgraph-io/badger"
	"github.com/stretchr/testify/assert"
)

const (
	numMPC    = 3
	keyPrefix = "mpc_"
)

var (
	dbPath           string
	badgerDB         *badger.DB
	repositoryToTest mpc.Repository
	expectedMPCList  []mpc.MPC
)

func TestMain(m *testing.M) {
	setDBPath()
	retCode := m.Run()
	os.Exit(retCode)
}

func setDBPath() {
	goPath := os.Getenv("GOPATH")
	dbPath = goPath + "/src/gitlab.com/arpachain/mpc/persistent/test/temp_db_mpc"
}

type setupTestCase func(t *testing.T, db *badger.DB, expectedMPCList []mpc.MPC)

func setup(t *testing.T, numMPC int, fns ...setupTestCase) func(t *testing.T) {
	log.Println("setting up test...")
	opts := badger.DefaultOptions(dbPath)
	var err error
	badgerDB, err = badger.Open(opts)
	repositoryToTest = badger_mpc.NewBadgerRepository(badgerDB)
	if err != nil {
		t.Fatalf("setting up test failed: %v", err)
	}
	expectedMPCList = generateExpectedMPCList(numMPC)
	// calls setupTestCase functions if passed in
	for _, fn := range fns {
		fn(t, badgerDB, expectedMPCList)
	}
	return func(t *testing.T) {
		log.Println("tearing down test...")
		repositoryToTest = nil
		expectedMPCList = nil
		badgerDB.Close()
		badgerDB = nil
		err := os.RemoveAll(dbPath)
		if err != nil {
			t.Fatalf("tearing down test failed: %v", err)
		}
	}
}

func TestGetByID(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, 1, createMPCListToDatabase)
	defer tearDown(t)

	expectedMPC := expectedMPCList[0]
	mpc, err := repositoryToTest.GetByID(expectedMPC.ID)
	assert.NoError(err)
	assert.NotEmpty(mpc)
	assert.Equal(expectedMPC.ID, mpc.ID)
	assert.Equal(expectedMPC.NodeIDs, mpc.NodeIDs)
}

func TestGetByID_InvalidID(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, 1)
	defer tearDown(t)

	mpc, err := repositoryToTest.GetByID("invalid_id")
	assert.Error(err)
	assert.Empty(mpc)
}
func TestGetAll(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, numMPC, createMPCListToDatabase)
	defer tearDown(t)

	mpcList, err := repositoryToTest.GetAll()
	assert.NoError(err)
	assert.NotEmpty(mpcList)
	assert.Equal(len(expectedMPCList), len(mpcList))
	for i := 0; i < numMPC; i++ {
		assert.Contains(expectedMPCList, mpcList[i])
	}
}

func TestCreate(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, 1)
	defer tearDown(t)

	expectedMPC := expectedMPCList[0]
	id, err := repositoryToTest.Create(&expectedMPC)
	assert.NoError(err)
	assert.Equal(expectedMPC.ID, id)
	createdMPC, err := getMPCFromDatabase(t, badgerDB, keyPrefix, expectedMPC.ID)
	if err != nil {
		t.Fatalf("retriving mpc from database failed: %v", err)
	}
	assert.Equal(expectedMPC.ID, createdMPC.ID)
	assert.Equal(expectedMPC.NodeIDs, createdMPC.NodeIDs)
}

func TestRemove(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, 1, createMPCListToDatabase)
	defer tearDown(t)

	expectedMPC := expectedMPCList[0]
	currentMPC, err := getMPCFromDatabase(t, badgerDB, keyPrefix, expectedMPC.ID)
	if err != nil {
		t.Fatalf("retriving mpc from database failed: %v", err)
	}
	assert.NotEmpty(currentMPC)
	repositoryToTest.Remove(expectedMPC.ID)
	currentMPC, err = getMPCFromDatabase(t, badgerDB, keyPrefix, expectedMPC.ID)
	assert.Error(err)
	assert.Empty(currentMPC)
}

func TestFinishMPC(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, 1, createMPCListToDatabase)
	defer tearDown(t)

	expectedMPC := expectedMPCList[0]
	nodeID := "fake_node_1"
	mpcResult := &arpa_mpc.MpcResult{
		Data: "fake_node_1",
	}
	err := repositoryToTest.FinishMPC(expectedMPC.ID, nodeID, mpcResult)
	assert.NoError(err)
	finishedMPC, err := getMPCFromDatabase(t, badgerDB, keyPrefix, expectedMPC.ID)
	if err != nil {
		t.Fatalf("retriving mpc from database failed: %v", err)
	}
	assert.Equal(true, finishedMPC.FinishFlagMap[nodeID])
	assert.Equal("fake_node_1", finishedMPC.ResultMap[nodeID].Data)
}

func TestFinishMPC_CalledMoreThanOnce(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, 1, createMPCListToDatabase)
	defer tearDown(t)

	expectedMPC := expectedMPCList[0]
	existingMPC, err := getMPCFromDatabase(t, badgerDB, keyPrefix, expectedMPC.ID)
	if err != nil {
		t.Fatalf("retriving mpc from database failed: %v", err)
	}
	nodeID := "fake_node_1"
	existingMPC.FinishFlagMap[nodeID] = true
	mpcResult := &arpa_mpc.MpcResult{
		Data: "fake_node_1",
	}
	updateMPCToDatabase(t, badgerDB, keyPrefix, existingMPC)

	err = repositoryToTest.FinishMPC(expectedMPC.ID, nodeID, mpcResult)
	assert.Error(err)
}

func generateExpectedMPC(index int) mpc.MPC {
	nodeIDs := []string{"fake_node_1", "fake_node_2", "fake_node_3"}
	resultMap := make(map[string]arpa_mpc.MpcResult)
	finishFlagMap := make(map[string]bool)
	return mpc.MPC{
		ID:            fmt.Sprintf("eth-address-%d", index),
		NodeIDs:       nodeIDs,
		ResultMap:     resultMap,
		FinishFlagMap: finishFlagMap,
	}
}

func generateExpectedMPCList(numMPC int) []mpc.MPC {
	mpcList := make([]mpc.MPC, 0)
	for i := 0; i < numMPC; i++ {
		mpc := generateExpectedMPC(i)
		mpcList = append(mpcList, mpc)
	}
	return mpcList
}

func createMPCListToDatabase(t *testing.T, db *badger.DB, expectedMPCList []mpc.MPC) {
	for _, mpc := range expectedMPCList {
		txn := db.NewTransaction(true)
		defer txn.Discard()
		mpcBytes, err := json.Marshal(mpc)
		if err != nil {
			t.Fatalf("setting up test case failed: %v", err)
		}
		err = txn.Set([]byte(keyPrefix+mpc.ID), mpcBytes)
		if err != nil {
			t.Fatalf("setting up test case failed: %v", err)
		}
		if err := txn.Commit(); err != nil {
			t.Fatalf("setting up test case failed: %v", err)
		}
	}
}

func getMPCFromDatabase(t *testing.T, db *badger.DB, keyPrefix, id string) (mpc.MPC, error) {
	var m mpc.MPC
	err := db.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(keyPrefix + id))
		if err != nil {
			return err
		}
		err = item.Value(func(v []byte) error {
			err := json.Unmarshal(v, &m)
			return err
		})
		return err
	})
	if err != nil {
		return mpc.MPC{}, err
	}
	return m, nil
}

func updateMPCToDatabase(t *testing.T, db *badger.DB, keyPrefix string, m mpc.MPC) {
	mpcBytes, err := json.Marshal(m)
	if err != nil {
		t.Fatalf("marshalling mpc failed: %v", err)
	}
	err = db.Update(func(txn *badger.Txn) error {
		err := txn.Set([]byte(keyPrefix+m.ID), mpcBytes)
		return err
	})
	if err != nil {
		t.Fatalf("updating mpc to database failed: %v", err)
	}
}
