package node

import (
	"encoding/json"
	"fmt"
	"log"

	"gitlab.com/arpachain/mpc/coordinator/pkg/node"
	"gitlab.com/arpachain/mpc/coordinator/pkg/util"
	"gitlab.com/arpachain/mpc/persistent/pkg/db"
	"github.com/dgraph-io/badger"
)

const (
	keyPrefix = "node_"
)

// BadgerRepository implements interface:
// gitlab.com/arpachain/mpc/coordinator/pkg/node/Repository
type badgerRepository struct {
	db *badger.DB
}

// NewBadgerRepository returns a BadgerDB-based implementation of interface:
// gitlab.com/arpachain/mpc/coordinator/pkg/node/Repository
func NewBadgerRepository(db *badger.DB) node.Repository {
	return &badgerRepository{
		db: db,
	}
}

func (r *badgerRepository) GetAll() ([]node.Node, error) {
	nodes := make([]node.Node, 0)
	objects, err := db_utils.GetAllObjects(r.db, keyPrefix, &node.Node{})
	if err != nil {
		return nil, err
	}
	for _, obj := range objects {
		n := obj.(*node.Node)
		nodes = append(nodes, *n)
	}
	return nodes, nil
}

// GetByID returns empty node and throws error when id does not exist
func (r *badgerRepository) GetByID(id string) (node.Node, error) {
	var n node.Node
	exists, err := db_utils.GetObject(r.db, keyPrefix, id, &n)
	if err != nil {
		return node.Node{}, err
	}
	if !exists {
		return node.Node{}, fmt.Errorf("the node with id: %v does not exist", id)
	}
	return n, nil
}

func (r *badgerRepository) Create(node *node.Node) (string, error) {
	id, err := util.GenerateID()
	if err != nil {
		return "", err
	}
	node.ID = id
	nodeBytes, err := json.Marshal(node)
	if err != nil {
		return "", err
	}
	err = db_utils.CreateObject(r.db, keyPrefix, node.ID, nodeBytes)
	if err != nil {
		return "", err
	}
	return node.ID, nil
}

func (r *badgerRepository) GroupNode(id string) error {
	return r.readAndUpdate(id, func(oldNode *node.Node) (*node.Node, error) {
		if oldNode.IsGrouped {
			return nil, fmt.Errorf("the node with id: %v is already grouped", id)
		}
		oldNode.IsGrouped = true
		return oldNode, nil
	})
}

func (r *badgerRepository) UngroupNode(id string) error {
	return r.readAndUpdate(id,
		func(oldNode *node.Node) (*node.Node, error) {
			if !oldNode.IsGrouped {
				return nil, fmt.Errorf("the node with id: %v is already ungrouped", id)
			}
			oldNode.IsGrouped = false
			return oldNode, nil
		})
}

func (r *badgerRepository) Remove(id string) {
	err := db_utils.DeleteObject(r.db, keyPrefix, id)
	// TODO(bomo) need to change the interface behavior for error handling
	if err != nil {
		log.Printf("deleting node : %v failed for reason: %v", id, err)
	}
}

func (r *badgerRepository) readAndUpdate(
	id string,
	updateFn func(*node.Node) (*node.Node, error),
) error {
	err := r.db.Update(func(txn *badger.Txn) error {
		var oldNode node.Node
		item, err := txn.Get([]byte(keyPrefix + id))
		if err != nil {
			return err
		}
		err = item.Value(func(v []byte) error {
			err := json.Unmarshal(v, &oldNode)
			return err
		})
		if err != nil {
			return err
		}
		newNode, err := updateFn(&oldNode)
		if err != nil {
			return err
		}
		nodeBytes, err := json.Marshal(newNode)
		if err != nil {
			return err
		}
		return txn.Set([]byte(keyPrefix+newNode.ID), nodeBytes)
	})
	return err
}
