package node_test

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"testing"

	"gitlab.com/arpachain/mpc/coordinator/pkg/node"
	badger_node "gitlab.com/arpachain/mpc/persistent/pkg/node"
	"github.com/dgraph-io/badger"
	"github.com/stretchr/testify/assert"
)

const (
	numNodes  = 3
	keyPrefix = "node_"
)

var (
	dbPath           string
	badgerDB         *badger.DB
	repositoryToTest node.Repository
	expectedNodes    []node.Node
	testNodeIDPrefix = "test_node_id_"
)

func TestMain(m *testing.M) {
	setDBPath()
	retCode := m.Run()
	os.Exit(retCode)
}

func setDBPath() {
	goPath := os.Getenv("GOPATH")
	dbPath = goPath + "/src/gitlab.com/arpachain/mpc/persistent/test/temp_db_node"
}

type setupTestCase func(t *testing.T, db *badger.DB, expectedNodes []node.Node)

func setup(t *testing.T, numNodes int, fns ...setupTestCase) func(t *testing.T) {
	log.Println("setting up test...")
	opts := badger.DefaultOptions(dbPath)
	var err error
	badgerDB, err = badger.Open(opts)
	repositoryToTest = badger_node.NewBadgerRepository(badgerDB)
	if err != nil {
		t.Fatalf("setting up test failed: %v", err)
	}
	expectedNodes = generateExpectedNodes(numNodes)
	// calls setupTestCase functions if passed in
	for _, fn := range fns {
		fn(t, badgerDB, expectedNodes)
	}
	return func(t *testing.T) {
		log.Println("tearing down test...")
		repositoryToTest = nil
		expectedNodes = nil
		badgerDB.Close()
		badgerDB = nil
		err := os.RemoveAll(dbPath)
		if err != nil {
			t.Fatalf("tearing down test failed: %v", err)
		}
	}
}

func TestGetByID(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, 1, createNodesToDatabase)
	defer tearDown(t)

	expectedNode := expectedNodes[0]
	node, err := repositoryToTest.GetByID(expectedNode.ID)
	assert.NoError(err)
	assert.NotEmpty(node)
	assert.Equal(expectedNode.ID, node.ID)
	assert.Equal(expectedNode.Name, node.Name)
	assert.Equal(expectedNode.Address, node.Address)
	assert.Equal(expectedNode.IsGrouped, node.IsGrouped)
	assert.Equal(expectedNode.Certificate, node.Certificate)
}

func TestGetByID_InvalidID(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, 1)
	defer tearDown(t)

	node, err := repositoryToTest.GetByID("invalid_id")
	assert.Error(err)
	assert.Empty(node)
}

func TestGetAll(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, numNodes, createNodesToDatabase)
	defer tearDown(t)

	nodes, err := repositoryToTest.GetAll()
	assert.NoError(err)
	assert.NotEmpty(nodes)
	assert.Equal(len(expectedNodes), len(nodes))
	for i := 0; i < numNodes; i++ {
		assert.Contains(expectedNodes, nodes[i])
	}
}

func TestCreate(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, 1)
	defer tearDown(t)

	expectedNode := expectedNodes[0]
	id, err := repositoryToTest.Create(&expectedNode)
	assert.NoError(err)
	assert.Equal(expectedNode.ID, id)
	createdNode, err := getNodeFromDatabase(t, badgerDB, keyPrefix, expectedNode.ID)
	if err != nil {
		t.Fatalf("retriving node from database failed: %v", err)
	}
	assert.Equal(expectedNode, createdNode)
}

func TestGroupNode(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, 1, createNodesToDatabase)
	defer tearDown(t)

	expectedNode := expectedNodes[0]
	err := repositoryToTest.GroupNode(expectedNode.ID)
	assert.NoError(err)
	updatedNode, err := getNodeFromDatabase(t, badgerDB, keyPrefix, expectedNode.ID)
	if err != nil {
		t.Fatalf("retriving node from database failed: %v", err)
	}
	assert.Equal(true, updatedNode.IsGrouped)
}

func TestUngroupNode(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, 1, createNodesToDatabase)
	defer tearDown(t)

	expectedNode := expectedNodes[0]
	expectedNode.IsGrouped = true
	updateNodeToDatabase(t, badgerDB, keyPrefix, expectedNode)

	err := repositoryToTest.UngroupNode(expectedNode.ID)
	assert.NoError(err)
	updatedNode, err := getNodeFromDatabase(t, badgerDB, keyPrefix, expectedNode.ID)
	if err != nil {
		t.Fatalf("retriving node from database failed: %v", err)
	}
	assert.Equal(false, updatedNode.IsGrouped)
}

func TestRemove(t *testing.T) {
	assert := assert.New(t)
	tearDown := setup(t, 1, createNodesToDatabase)
	defer tearDown(t)

	expectedNode := expectedNodes[0]
	currentNode, err := getNodeFromDatabase(t, badgerDB, keyPrefix, expectedNode.ID)
	if err != nil {
		t.Fatalf("retriving node from database failed: %v", err)
	}
	assert.NotEmpty(currentNode)
	repositoryToTest.Remove(expectedNode.ID)
	currentNode, err = getNodeFromDatabase(t, badgerDB, keyPrefix, expectedNode.ID)
	assert.Error(err)
	assert.Empty(currentNode)
}

func generateExpectedNode(index int) node.Node {
	return node.Node{
		ID:                fmt.Sprintf(testNodeIDPrefix, index),
		Name:              fmt.Sprintf("test_node_%d", index),
		Address:           fmt.Sprintf("127.0.0.1:600%d", index),
		IsGrouped:         false,
		Certificate:       []byte("fake_cert"),
		BlockchainAddress: fmt.Sprintf("blockchain-address-%d", index),
	}
}

func generateExpectedNodes(numNodes int) []node.Node {
	nodes := make([]node.Node, 0)
	for i := 0; i < numNodes; i++ {
		node := generateExpectedNode(i)
		nodes = append(nodes, node)
	}
	return nodes
}

func createNodesToDatabase(t *testing.T, db *badger.DB, expectedNodes []node.Node) {
	for i, node := range expectedNodes {
		txn := db.NewTransaction(true)
		defer txn.Discard()
		node.ID = fmt.Sprintf(testNodeIDPrefix, i)
		nodeBytes, err := json.Marshal(node)
		if err != nil {
			t.Fatalf("setting up test case failed: %v", err)
		}
		err = txn.Set([]byte(keyPrefix+node.ID), nodeBytes)
		if err != nil {
			t.Fatalf("setting up test case failed: %v", err)
		}
		if err := txn.Commit(); err != nil {
			t.Fatalf("setting up test case failed: %v", err)
		}
	}
}

func getNodeFromDatabase(t *testing.T, db *badger.DB, keyPrefix, id string) (node.Node, error) {
	var n node.Node
	err := db.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(keyPrefix + id))
		if err != nil {
			return err
		}
		err = item.Value(func(v []byte) error {
			err := json.Unmarshal(v, &n)
			return err
		})
		return err
	})
	if err != nil {
		return node.Node{}, err
	}
	return n, nil
}

func updateNodeToDatabase(t *testing.T, db *badger.DB, keyPrefix string, n node.Node) {
	nodeBytes, err := json.Marshal(n)
	if err != nil {
		t.Fatalf("marshalling node failed: %v", err)
	}
	err = db.Update(func(txn *badger.Txn) error {
		err := txn.Set([]byte(keyPrefix+n.ID), nodeBytes)
		return err
	})
	if err != nil {
		t.Fatalf("updating node to database failed: %v", err)
	}
}
