FROM debian:latest
EXPOSE 4000-9000

# install required tools
RUN set -ex \
  && apt-get update \
  && apt-get --yes upgrade \
  && apt-get --yes install unzip \
  && apt-get --yes install wget \
  && apt-get --yes install curl \
  && apt-get --yes install bzip2 \
  && apt-get --yes install git \
  && apt-get --yes install build-essential autoconf libtool pkg-config \
  && apt-get --yes install software-properties-common

# install gRPC and protobuf
RUN set -ex \
  && apt-get update \
  && apt-get --yes install libgflags-dev libgtest-dev \
  && apt-get --yes install clang libc++-dev \
  && cd \
  && git clone -b $(curl -L https://grpc.io/release) https://github.com/grpc/grpc \
  && cd grpc \
  && git submodule update --init \
  && make && make install \
  && make grpc_cli \
  && ln -s /root/grpc/bins/opt/grpc_cli /usr/bin/grpc_cli \
  && cd third_party/protobuf \
  && make install

# install prerequisite for SCALE-MAMBA (yasm, mpir, gmpy2, openssl)
# build and test-run SCALE-MAMBA
RUN set -ex \
  && apt-get update \
  && apt-get --yes install yasm \
  && cd \
  && wget http://mpir.org/mpir-3.0.0.tar.bz2 \
  && tar -xvf mpir-3.0.0.tar.bz2 \
  && cd mpir-3.0.0 \
  && ./configure --enable-cxx \
  && make && make check && make install \
  && apt-get --yes install python-dev \
  && apt-get --yes install python-gmpy2 \
  && cd \
  && wget https://www.openssl.org/source/openssl-1.1.0i.tar.gz \
  && tar -xvf openssl-1.1.0i.tar.gz \
  && cd openssl-1.1.0i \
  && ./config && make && make install \
  && cd \
  && git clone https://github.com/KULeuven-COSIC/SCALE-MAMBA.git scale \
  && cd scale \
  && echo 'ROOT = /root/scale' >> CONFIG.mine \
  && echo 'OSSL = /root/openssl-1.1.0i' >> CONFIG.mine \
  && echo 'FLAGS = -DMAX_MOD_SZ=7' >> CONFIG.mine \
  && echo 'OPT = -O3' >> CONFIG.mine \
  && echo 'LDFLAGS =' >> CONFIG.mine \
  && make \
  && cp Auto-Test-Data/Cert-Store/* Cert-Store/ \
  && cp Auto-Test-Data/1/* Data/ \
  && echo '/usr/local/lib' >> /etc/ld.so.conf.d/libs.conf \
  && ldconfig \
  && Scripts/test.sh test_sqrt
