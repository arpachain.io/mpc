/*
Copyright (c) 2017, The University of Bristol, Senate House, Tyndall Avenue,
Bristol, BS8 1TH, United Kingdom.
Copyright (c) 2018, COSIC-KU Leuven, Kasteelpark Arenberg 10, bus 2452, B-3001
Leuven-Heverlee, Belgium.

All rights reserved
*/

#include <google/protobuf/map.h>
#include <grpcpp/grpcpp.h>
#include <fstream>

#include "Exceptions/Exceptions.h"
#include "schedule.h"

using ::google::protobuf::Map;
using grpc::Status;

void print_hex(const char *s, int n) {
  for (int i = 0; i < n; i++) {
    printf("%02x", (unsigned int)s[i]);
  }
}

string read_file(ifstream &in) {
  string contents;
  in.seekg(0, std::ios::end);
  contents.resize(in.tellg());
  in.seekg(0, std::ios::beg);
  in.read(&contents[0], contents.size());
  return contents;
}

unsigned int Schedule::Load_Programs() {
  char filename[2048];
  sprintf(filename, "%s/%s.sch", progname.c_str(), name.c_str());
  fprintf(stderr, "Opening file %s\n", filename);
  ifstream ifs;
  ifs.open(filename);
  if (ifs.fail()) {
    throw file_error("Missing '" + string(filename) + "'. Did you compile '" +
                     name + "'?");
  }
  i_schedule = istringstream(read_file(ifs));
  ifs.close();
  /*
  printf("i_schedule : \n------\n%s\n------\n",i_schedule.str().c_str());
*/

  unsigned int nthreads;
  i_schedule >> nthreads;
  fprintf(stderr, "Number of online threads I will run in parallel =  %d\n",
          nthreads);

  int nprogs;
  i_schedule >> nprogs;
  fprintf(stderr, "Number of program sequences I need to load =  %d\n", nprogs);

  // Load in the programs
  progs.resize(nprogs);
  tnames.resize(nprogs);
  char threadname[1024];
  for (int i = 0; i < nprogs; i++) {
    i_schedule >> threadname;
    sprintf(filename, "%s/%s.bc", progname.c_str(), threadname);
    tnames[i] = string(threadname);
    fprintf(stderr, "Loading program %d from %s\n", i, filename);
    ifs.open(filename, ifstream::binary);
    if (ifs.fail()) {
      throw file_error(filename);
    }
    progs[i] = stringstream(read_file(ifs));
    ifs.close();
    /*
    printf("%d\n",progs[i].str().size());
    printf("progs[%d] : \n------\n",i);
    print_hex(progs[i].str().c_str(),progs[i].str().size());
    printf("\n------\n");
*/
  }

  return nthreads;
}

unsigned int Schedule::Load_Programs(const string &pname) {
  progname = pname;
  /* First find progname itself, minus the directory path */
  int pos = progname.size() - 1, tot = progname.size() - 1;
  if (progname.c_str()[pos] == '/') {
    pos--;
    tot--;
  }
  while (progname.c_str()[pos] != '/' && pos >= 0) {
    pos--;
  }
  name = progname.substr(pos + 1, tot - pos);

  unsigned int nthreads = Load_Programs();

  return nthreads;
}

Status Schedule::LoadPrograms(string schedule,
                              const Map<string, string> &prog_map,
                              int *num_threads) {
  i_schedule = istringstream(schedule);

  i_schedule >> *num_threads;
  fprintf(stderr, "Number of online threads I will run in parallel =  %d\n",
          *num_threads);

  int nprogs;
  i_schedule >> nprogs;
  fprintf(stderr, "Number of program sequences I need to load =  %d\n", nprogs);

  // Load in the programs
  progs.resize(nprogs);
  tnames.resize(nprogs);
  std::string thread_name;
  for (int i = 0; i < nprogs; i++) {
    i_schedule >> thread_name;
    tnames[i] = thread_name;
    fprintf(stderr, "Loading program %d from %s\n", i, thread_name.c_str());
    auto it = prog_map.find(thread_name);

    if (it == prog_map.end()) {
      return Status(grpc::StatusCode::INVALID_ARGUMENT,
                    "Program for %s is not found in request.", tnames[i]);
    }
    string program = prog_map.find(thread_name)->second;
    progs[i] = stringstream(program);
  }

  return Status::OK;
}
