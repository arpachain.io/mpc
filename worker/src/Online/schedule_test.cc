#include "schedule.h"

#include <limits.h>

#include "Exceptions/Exceptions.h"
#include "arpa_mpc.grpc.pb.h"
#include "gmock/gmock.h"  // Brings in Google Mock.
#include "gtest/gtest.h"

namespace {
using ::google::protobuf::Map;
using grpc::Status;
using ::testing::_;
using ::testing::AtLeast;

class MockSchedule : public Schedule {
 public:
  MOCK_METHOD3(LoadPrograms,
               Status(string schedule, const Map<string, string> &prog_map,
                      int *num_threads));
};

const string kTestSchedule = R"(
        1
        1
        demo-0
        1 0
        0
        ./compile.py Programs/demo/)";

const int kTestScheduleLargeNumThreads = 11;
const int kTestScheduleLargeNumPrograms = 62;
const string kTestScheduleLarge = R"(
        11
        62
        test_new_threads-0 test_new_threads-f-1 test_new_threads-f-2 test_new_threads-f-3 test_new_threads-f-4 test_new_threads-f-5 test_new_threads-f-6 test_new_threads-f-7 test_new_threads-f-8 test_new_threads-f-9 test_new_threads-f-10 test_new_threads-f-11 test_new_threads-f-12 test_new_threads-f-13 test_new_threads-f-14 test_new_threads-f-15 test_new_threads-f-16 test_new_threads-f-17 test_new_threads-f-18 test_new_threads-f-19 test_new_threads-f-20 test_new_threads-f-21 test_new_threads-f-22 test_new_threads-f-23 test_new_threads-f-24 test_new_threads-f-25 test_new_threads-f-26 test_new_threads-f-27 test_new_threads-f-28 test_new_threads-f-29 test_new_threads-f-30 test_new_threads-f-31 test_new_threads-f-32 test_new_threads-f-33 test_new_threads-f-34 test_new_threads-f-35 test_new_threads-f-36 test_new_threads-f-37 test_new_threads-f-38 test_new_threads-f-39 test_new_threads-f-40 test_new_threads-f-41 test_new_threads-f-42 test_new_threads-f-43 test_new_threads-f-44 test_new_threads-f-45 test_new_threads-f-46 test_new_threads-f-47 test_new_threads-f-48 test_new_threads-f-49 test_new_threads-f-50 test_new_threads-f-51 test_new_threads-f-52 test_new_threads-f-53 test_new_threads-f-54 test_new_threads-f-55 test_new_threads-f-56 test_new_threads-f-57 test_new_threads-f-58 test_new_threads-f-59 test_new_threads-f-60 test_new_threads-f-61
        1 0
        0
        ./compile.py Programs/test_new_threads)";
const string kTestScheduleLargeProgramNames = R"(
 test_new_threads-0 test_new_threads-f-1 test_new_threads-f-2 test_new_threads-f-3 test_new_threads-f-4 test_new_threads-f-5 test_new_threads-f-6 test_new_threads-f-7 test_new_threads-f-8 test_new_threads-f-9 test_new_threads-f-10 test_new_threads-f-11 test_new_threads-f-12 test_new_threads-f-13 test_new_threads-f-14 test_new_threads-f-15 test_new_threads-f-16 test_new_threads-f-17 test_new_threads-f-18 test_new_threads-f-19 test_new_threads-f-20 test_new_threads-f-21 test_new_threads-f-22 test_new_threads-f-23 test_new_threads-f-24 test_new_threads-f-25 test_new_threads-f-26 test_new_threads-f-27 test_new_threads-f-28 test_new_threads-f-29 test_new_threads-f-30 test_new_threads-f-31 test_new_threads-f-32 test_new_threads-f-33 test_new_threads-f-34 test_new_threads-f-35 test_new_threads-f-36 test_new_threads-f-37 test_new_threads-f-38 test_new_threads-f-39 test_new_threads-f-40 test_new_threads-f-41 test_new_threads-f-42 test_new_threads-f-43 test_new_threads-f-44 test_new_threads-f-45 test_new_threads-f-46 test_new_threads-f-47 test_new_threads-f-48 test_new_threads-f-49 test_new_threads-f-50 test_new_threads-f-51 test_new_threads-f-52 test_new_threads-f-53 test_new_threads-f-54 test_new_threads-f-55 test_new_threads-f-56 test_new_threads-f-57 test_new_threads-f-58 test_new_threads-f-59 test_new_threads-f-60 test_new_threads-f-61
    )";

TEST(Schedule, TestMock) {
  MockSchedule schedule;
  EXPECT_CALL(schedule, LoadPrograms(_, _, _)).Times(AtLeast(1));

  Map<string, string> prog_map;
  prog_map["demo-0"] = "fake test program";

  int num_online_threads = 0;
  schedule.LoadPrograms(kTestSchedule, prog_map, &num_online_threads);
  schedule.LoadPrograms(kTestSchedule, prog_map, &num_online_threads);
}

TEST(Schedule, LoadProgramsSucceed) {
  Schedule schedule;
  Map<string, string> prog_map;
  prog_map["demo-0"] = "fake test program";

  int num_online_threads = 0;
  Status s =
      schedule.LoadPrograms(kTestSchedule, prog_map, &num_online_threads);

  EXPECT_TRUE(s.ok());
  EXPECT_EQ(1, num_online_threads);
}

TEST(Schedule, LoadProgramsFail) {
  Schedule schedule;
  Map<string, string> wrong_prog_map;
  // Expect LoadPrograms to return error as the key in prog_map is wrong.
  wrong_prog_map["wrong_key"] = "fake test program";

  int num_online_threads = 0;
  Status s =
      schedule.LoadPrograms(kTestSchedule, wrong_prog_map, &num_online_threads);
  EXPECT_EQ(grpc::StatusCode::INVALID_ARGUMENT, s.error_code());
}

TEST(Schedule, LoadProgramsManyThreads) {
  Schedule schedule;
  Map<string, string> prog_map;

  auto nameStream = istringstream(kTestScheduleLargeProgramNames);

  for (int i = 0; i < kTestScheduleLargeNumPrograms; i++) {
    string prog_key;
    nameStream >> prog_key;
    prog_map[prog_key] = "fake test program";
  }

  int num_online_threads = 0;
  Status s =
      schedule.LoadPrograms(kTestScheduleLarge, prog_map, &num_online_threads);

  EXPECT_TRUE(s.ok());
  EXPECT_EQ(kTestScheduleLargeNumThreads, num_online_threads);
}
}  // namespace
