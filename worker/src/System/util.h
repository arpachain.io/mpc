/*
 *  Copyright (c) 2018, ARPACORP LTD
 *  All rights reserved
 */

#ifndef ARPA_MPC_SYSTEM_UTIL_H
#define ARPA_MPC_SYSTEM_UTIL_H

#include <grpcpp/grpcpp.h>
#include <string>
#include <vector>

namespace arpa_mpc {

grpc::Status parseAddr(std::string_view addr, std::string *ip, int *port);

}  // end namespace arpa_mpc
#endif  // ARPA_MPC_SYSTEM_UTIL_H
