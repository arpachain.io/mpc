/*
 *  Copyright (c) 2018, ARPACORP LTD
 *  All rights reserved
 */

#ifndef ARPA_MPC_SYSTEM_MPC_RUNTIME_H
#define ARPA_MPC_SYSTEM_MPC_RUNTIME_H

// This file holds the main ARPA MPC runtime in a class.

#include <grpcpp/grpcpp.h>

#include "FHE/FHE_Keys.h"
#include "Offline/offline_data.h"
#include "Online/Machine.h"
#include "System/SystemData.h"

class MpcRuntime {
 public:
  grpc::Status Run(unsigned int my_number, unsigned int no_online_threads,
                   const FFT_Data &PTD, const FHE_PK &pk, const FHE_SK &sk,
                   const vector<gfp> &MacK, SSL_CTX *ctx, const SystemData &SD,
                   Machine &machine, offline_control_data &OCD,
                   unsigned int number_FHE_threads, int verbose);
};

#endif  // ARPA_MPC_SYSTEM_MPC_RUNTIME_H
