#include <limits.h>

#include <grpcpp/grpcpp.h>
#include "gtest/gtest.h"

#include "System/util.h"

namespace {
using arpa_mpc::parseAddr;
using grpc::Status;
using grpc::StatusCode;
using std::string;

// Test that InputOutputRpc can handle public input from proto message.
TEST(Util, ParserTest) {
  string ip;
  int port;
  Status s = parseAddr("localhost:5000", &ip, &port);
  EXPECT_TRUE(s.ok());
  EXPECT_EQ("localhost", ip);
  EXPECT_EQ(5000, port);
}

TEST(Util, ParserTestIp) {
  string ip;
  int port;
  Status s = parseAddr("192.168.1.1:12345", &ip, &port);
  EXPECT_TRUE(s.ok());
  EXPECT_EQ("192.168.1.1", ip);
  EXPECT_EQ(12345, port);
}

TEST(Util, ParserTestInvalid) {
  string ip;
  int port;
  Status s = parseAddr("abcde", &ip, &port);
  EXPECT_EQ(StatusCode::INVALID_ARGUMENT, s.error_code());
}
}  // namespace
