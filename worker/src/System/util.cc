/*
 *  Copyright (c) 2018, ARPACORP LTD
 *  All rights reserved
 */

#include <grpcpp/grpcpp.h>
#include <iostream>
#include <sstream>
#include <string>

namespace arpa_mpc {

using grpc::Status;
using std::string;

Status parseAddr(std::string_view addr, string *ip, int *port) {
  std::string host(addr);

  size_t colon_pos = host.find(':');
  if (colon_pos == std::string::npos) {
    return Status(grpc::StatusCode::INVALID_ARGUMENT,
                  "Invalid host name: " + host);
  }
  *ip = host.substr(0, colon_pos);
  std::string portPart = host.substr(colon_pos + 1);

  std::stringstream parser(portPart);
  *port = -1;
  if (parser >> *port && *port >= 0 && *port < 65536) {
    return Status::OK;
  }
  return Status(grpc::StatusCode::INVALID_ARGUMENT,
                "Port not convertible to an integer. Host name: " + host);
}

}  // end namespace arpa_mpc
