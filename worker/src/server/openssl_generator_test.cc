#include "openssl_generator.h"

#include <stdio.h>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>
#include <string_view>

#include "glog/logging.h"
#include "grpcpp/grpcpp.h"
#include "gtest/gtest.h"
#include "openssl/pem.h"
#include "openssl/rsa.h"

namespace arpa_mpc {
using arpa_mpc::OpensslGenerator;
using grpc::Status;
using grpc::StatusCode;

// Variables for testing use
// cmd for generate expected private key:
// `openssl genrsa -des3 -out expected_private.key 2048`
// password is: `password`

// cmd for generate expected public key:
// `openssl rsa -pubout -in expected_private.key -out expected_public.key`

// cmd for generate expected csr:
// `openssl req -new -key expected_private.key -out expected_csr.csr`

const std::string kTestValidPublicKeyPath =
    "server/test_data/test_valid_public.key";
const std::string kTestValidPrivateKeyPath =
    "server/test_data/test_valid_private.key";
const std::string kTestValidPrivateKeyPassword = "password";
const std::string kTestOutputCsrPath = "server/test_data/test_output_csr.csr";
const std::string kTestOutputCrtPath = "server/test_data/test_output_crt.crt";

const std::string kTestEmptyPrivateKeyPath =
    "server/test_data/test_empty_private.key";
const std::string kTestCorruptedPrivateKeyPath =
    "server/test_data/test_corrupted_private.key";
const std::string kTestEmptyPublicKeyPath =
    "server/test_data/test_empty_public.key";
const std::string kTestCorruptedPublicKeyPath =
    "server/test_data/test_corrupted_public.key";
const std::string kTestWrongPrivateKeyPassword = "wrong_password";
const std::string kTestExpectedPrivateKey =
    "server/test_data/expected_private.key";
const std::string kTestExpectedPublicKey =
    "server/test_data/expected_public.key";
const std::string kTestExpectedCsrPath = "server/test_data/expected_csr.csr";
const std::string kTestCorruptedCsrPath =
    "server/test_data/test_corrupted_csr.csr";
const std::string kTestEmptyCsrPath = "server/test_data/test_empty_csr.csr";

Status RemoveLocalFile(std::string_view target_file_sv) {
  std::string target_file(target_file_sv);
  std::string error_msg;

  std::ifstream file_checker(target_file);
  if (file_checker.fail()) {
    error_msg = "There's no file need be delete";
    LOG(INFO) << error_msg;
    return Status(StatusCode::OK, error_msg);
  } else {
    if (remove(target_file.c_str()) != 0) {
      error_msg = "Error deleting file";
      LOG(ERROR) << error_msg;
      return Status(StatusCode::INTERNAL, error_msg);
    } else {
      error_msg = "Delete successfully: " + target_file;
      LOG(INFO) << error_msg;
      return Status(StatusCode::OK, error_msg);
    }
  }
}

// This function can execute a command line and
// gather the return infomation in cmd
Status ExecuteCmd(std::string_view cmd_string_view, std::string *response) {
  std::string result;            // Variable for contain the response content
  std::array<char, 128> buffer;  // Buffer to load the response
  std::string cmd(cmd_string_view);
  std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd.c_str(), "r"),
                                                pclose);
  std::string error_msg;
  if (!pipe) {
    error_msg = "Run command error";
    LOG(ERROR) << error_msg;
    return Status(StatusCode::INTERNAL, error_msg);
  }
  while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
    result += buffer.data();
  }
  *response = result;
  error_msg = "Success execute cmd";
  return Status(StatusCode::OK, error_msg);
}

Status VerifyCsrByCmd(std::string_view csr_path_sv, bool *success) {
  std::string test_csr_path_str(csr_path_sv);
  std::string error_msg;
  if (test_csr_path_str.empty()) {
    *success = false;
    error_msg = "Csr path can't be null";
    LOG(ERROR) << error_msg;
    return Status(StatusCode::INTERNAL, error_msg);
  }

  std::string cmd_str;   // Command lin::string
  std::string response;  // Return content after cmd finish

  cmd_str = "openssl req -text -noout -verify -in " + test_csr_path_str;

  Status s = ExecuteCmd(cmd_str, &response);
  if (!s.ok()) {
    *success = false;
    return s;
  }
  // Prevent the empty string blocking the program
  if (response == "") {
    *success = false;
    error_msg = "Response is empty";
    LOG(ERROR) << error_msg;
    return Status(StatusCode::INTERNAL, error_msg);
  } else {
    // If the csr file be verified success,
    // the beginning of response will be "Certificate Request"
    std::string flag = response.substr(0, 19);
    if (flag != "Certificate Request") {
      *success = false;
      error_msg = "Certificate request error";
      LOG(INFO) << error_msg;
      return Status();
    }
  }
  *success = true;
  return Status();
}

Status VerifyCsrContentNotEmpty(std::string_view csr_content_sv,
                                bool *success) {
  std::string error_msg;
  std::string csr_content(csr_content_sv);
  if (csr_content.empty()) {
    *success = false;
    error_msg = "Csr return string is empty";
    LOG(ERROR) << error_msg;
    return Status(StatusCode::INTERNAL, error_msg);
  }
  *success = true;
  error_msg = "Csr content verify pass";
  LOG(INFO) << error_msg;
  return Status();
}

class CrtTest : public ::testing::Test {
 protected:
  void SetUp() override {
    FLAGS_public_key_path = kTestValidPublicKeyPath;
    FLAGS_private_key_path = kTestValidPrivateKeyPath;
    FLAGS_private_key_password = kTestValidPrivateKeyPassword;
    FLAGS_csr_path = kTestOutputCsrPath;
    FLAGS_crt_path = kTestOutputCrtPath;
    RemoveLocalFile(FLAGS_crt_path);
  }
  void TearDown() override { RemoveLocalFile(FLAGS_crt_path); }
};

TEST_F(CrtTest, SaveCrtWithEmptyCrtContent) {
  OpensslGenerator gen;
  Status s = gen.SaveCertificate("");
  ASSERT_EQ(StatusCode::INTERNAL, s.error_code());
}

TEST_F(CrtTest, SaveCrt) {
  const char *kCrtContent = "Only for test use, this is crt content";
  OpensslGenerator gen;
  Status s = gen.SaveCertificate(kCrtContent);
  ASSERT_TRUE(s.ok());
}

class CsrTestNoLocalCsrNoPasswordPrivateKey : public ::testing::Test {
 protected:
  void SetUp() override {
    FLAGS_public_key_path = kTestValidPublicKeyPath;
    FLAGS_private_key_path = kTestValidPrivateKeyPath;
    FLAGS_private_key_password = "";
    FLAGS_csr_path = kTestOutputCsrPath;
    FLAGS_crt_path = kTestOutputCrtPath;
    // Clean up csr file in case there is left over one from other tests.
    OpensslGenerator gen;
    gen.GenerateKeyPair(FLAGS_private_key_path, FLAGS_private_key_password,
                        FLAGS_public_key_path);
    RemoveLocalFile(FLAGS_csr_path);
  }
  void TearDown() override { RemoveLocalFile(FLAGS_csr_path); }
  std::string test_csr_content_;
};

TEST_F(CsrTestNoLocalCsrNoPasswordPrivateKey,
       LoadCsrWithNoLocalCsrEmptyPrivatekeyFile) {
  FLAGS_private_key_path = kTestEmptyPrivateKeyPath;

  OpensslGenerator gen;
  Status s = gen.GetCsr(&test_csr_content_);
  EXPECT_EQ(StatusCode::INTERNAL, s.error_code());
  bool success;
  Status s_verify = VerifyCsrByCmd(FLAGS_csr_path, &success);
  EXPECT_FALSE(success);
  ASSERT_FALSE(s_verify.ok());
}

TEST_F(CsrTestNoLocalCsrNoPasswordPrivateKey,
       LoadCsrWithNoLocalCsrEmptyPublickeyFile) {
  FLAGS_public_key_path = kTestEmptyPublicKeyPath;

  OpensslGenerator gen;
  Status s = gen.GetCsr(&test_csr_content_);
  EXPECT_FALSE(s.ok());
  bool success;
  Status s_verify = VerifyCsrByCmd(FLAGS_csr_path, &success);
  EXPECT_FALSE(success);
  ASSERT_FALSE(s_verify.ok());
}

TEST_F(CsrTestNoLocalCsrNoPasswordPrivateKey,
       LoadCsrWithNoLocalCsrErrorPrivatekeyFile) {
  FLAGS_private_key_path = kTestCorruptedPrivateKeyPath;

  OpensslGenerator gen;
  Status s = gen.GetCsr(&test_csr_content_);
  EXPECT_EQ(StatusCode::INTERNAL, s.error_code());
  bool success;
  Status s_verify = VerifyCsrByCmd(FLAGS_csr_path, &success);
  EXPECT_FALSE(success);
  ASSERT_EQ(StatusCode::INTERNAL, s_verify.error_code());
}

TEST_F(CsrTestNoLocalCsrNoPasswordPrivateKey,
       LoadCsrWithNoLocalCsrErrorPublickeyFile) {
  FLAGS_public_key_path = kTestCorruptedPublicKeyPath;

  OpensslGenerator gen;
  Status s = gen.GetCsr(&test_csr_content_);
  EXPECT_FALSE(s.ok());
  bool success;
  Status s_verify = VerifyCsrByCmd(FLAGS_csr_path, &success);
  EXPECT_FALSE(success);
  ASSERT_FALSE(s_verify.ok());
}

TEST_F(CsrTestNoLocalCsrNoPasswordPrivateKey,
       LoadCsrWithNoLocalCsrEmptyCsrFilePath) {
  FLAGS_csr_path = "";

  OpensslGenerator gen;
  Status s = gen.GetCsr(&test_csr_content_);
  ASSERT_EQ(StatusCode::INTERNAL, s.error_code());
}

TEST_F(CsrTestNoLocalCsrNoPasswordPrivateKey,
       LoadCsrWithNoLocalCsrEmptyPrivateKeyPath) {
  FLAGS_private_key_path = "";

  OpensslGenerator gen;
  Status s = gen.GetCsr(&test_csr_content_);
  EXPECT_EQ(StatusCode::INTERNAL, s.error_code());
  bool success;
  Status s_verify = VerifyCsrByCmd(FLAGS_csr_path, &success);
  EXPECT_FALSE(success);
  ASSERT_EQ(StatusCode::INTERNAL, s_verify.error_code());
}

TEST_F(CsrTestNoLocalCsrNoPasswordPrivateKey,
       LoadCsrWithNoLocalCsrEmptyPassword) {
  FLAGS_private_key_password = "";

  OpensslGenerator gen;
  Status s = gen.GetCsr(&test_csr_content_);
  EXPECT_TRUE(s.ok());
  bool success;
  Status s_verify = VerifyCsrByCmd(FLAGS_csr_path, &success);
  EXPECT_TRUE(success);
  ASSERT_TRUE(s_verify.ok());
}

TEST_F(CsrTestNoLocalCsrNoPasswordPrivateKey,
       LoadCsrWithNoLocalCsrEmptyPublicKeyPath) {
  FLAGS_public_key_path = "";

  OpensslGenerator gen;
  Status s = gen.GetCsr(&test_csr_content_);
  EXPECT_EQ(StatusCode::INTERNAL, s.error_code());
  bool success;
  Status s_verify = VerifyCsrByCmd(FLAGS_csr_path, &success);
  EXPECT_FALSE(success);

  ASSERT_EQ(StatusCode::INTERNAL, s_verify.error_code());
}

TEST_F(CsrTestNoLocalCsrNoPasswordPrivateKey, LoadCsrWithNoLocalCsr) {
  OpensslGenerator gen;
  Status s = gen.GetCsr(&test_csr_content_);
  EXPECT_TRUE(s.ok());
  bool success;
  Status s_verify = VerifyCsrByCmd(FLAGS_csr_path, &success);
  EXPECT_TRUE(success);

  ASSERT_TRUE(s_verify.ok());
}

class CsrTestNoLocalCsrPasswordPrivateKey : public ::testing::Test {
 protected:
  void SetUp() override {
    FLAGS_public_key_path = kTestValidPublicKeyPath;
    FLAGS_private_key_path = kTestValidPrivateKeyPath;
    FLAGS_private_key_password = kTestValidPrivateKeyPassword;
    FLAGS_csr_path = kTestOutputCsrPath;
    FLAGS_crt_path = kTestOutputCrtPath;
    // Clean up csr file in case there is left over one from other tests.
    OpensslGenerator gen;
    gen.GenerateKeyPair(FLAGS_private_key_path, FLAGS_private_key_password,
                        FLAGS_public_key_path);
    RemoveLocalFile(FLAGS_csr_path);
  }
  void TearDown() override { RemoveLocalFile(FLAGS_csr_path); }
  std::string test_csr_content_;
};

TEST_F(CsrTestNoLocalCsrPasswordPrivateKey,
       LoadCsrWithNoLocalCsrEmptyPrivatekeyFile) {
  FLAGS_private_key_path = kTestEmptyPrivateKeyPath;

  OpensslGenerator gen;
  Status s = gen.GetCsr(&test_csr_content_);
  EXPECT_EQ(StatusCode::INTERNAL, s.error_code());
  bool success;
  Status s_verify = VerifyCsrByCmd(FLAGS_csr_path, &success);
  EXPECT_FALSE(success);
  ASSERT_FALSE(s_verify.ok());
}

TEST_F(CsrTestNoLocalCsrPasswordPrivateKey,
       LoadCsrWithNoLocalCsrErrorPrivatekeyFile) {
  FLAGS_private_key_path = kTestCorruptedPrivateKeyPath;

  OpensslGenerator gen;
  Status s = gen.GetCsr(&test_csr_content_);
  EXPECT_EQ(StatusCode::INTERNAL, s.error_code());
  bool success;
  Status s_verify = VerifyCsrByCmd(FLAGS_csr_path, &success);
  EXPECT_FALSE(success);
  ASSERT_EQ(StatusCode::INTERNAL, s_verify.error_code());
}

TEST_F(CsrTestNoLocalCsrPasswordPrivateKey,
       LoadCsrWithNoLocalCsrEmptyPublickeyFile) {
  FLAGS_public_key_path = kTestEmptyPublicKeyPath;

  OpensslGenerator gen;
  Status s = gen.GetCsr(&test_csr_content_);
  EXPECT_FALSE(s.ok());
  bool success;
  Status s_verify = VerifyCsrByCmd(FLAGS_csr_path, &success);
  EXPECT_FALSE(success);
  ASSERT_FALSE(s_verify.ok());
}

TEST_F(CsrTestNoLocalCsrPasswordPrivateKey,
       LoadCsrWithNoLocalCsrErrorPublickeyFile) {
  FLAGS_public_key_path = kTestCorruptedPublicKeyPath;

  OpensslGenerator gen;
  Status s = gen.GetCsr(&test_csr_content_);
  EXPECT_FALSE(s.ok());
  bool success;
  Status s_verify = VerifyCsrByCmd(FLAGS_csr_path, &success);
  EXPECT_FALSE(success);
  ASSERT_FALSE(s_verify.ok());
}

TEST_F(CsrTestNoLocalCsrPasswordPrivateKey,
       LoadCsrWithNoLocalCsrEmptyCsrFilePath) {
  FLAGS_csr_path = "";

  OpensslGenerator gen;
  Status s = gen.GetCsr(&test_csr_content_);
  ASSERT_EQ(StatusCode::INTERNAL, s.error_code());
}

TEST_F(CsrTestNoLocalCsrPasswordPrivateKey,
       LoadCsrWithNoLocalCsrEmptyPrivateKeyPath) {
  FLAGS_private_key_path = "";

  OpensslGenerator gen;
  Status s = gen.GetCsr(&test_csr_content_);
  EXPECT_EQ(StatusCode::INTERNAL, s.error_code());
  bool success;
  Status s_verify = VerifyCsrByCmd(FLAGS_csr_path, &success);
  EXPECT_FALSE(success);
  ASSERT_EQ(StatusCode::INTERNAL, s_verify.error_code());
}

TEST_F(CsrTestNoLocalCsrPasswordPrivateKey,
       LoadCsrWithNoLocalCsrEmptyPassword) {
  FLAGS_private_key_password = "";

  OpensslGenerator gen;
  Status s = gen.GetCsr(&test_csr_content_);
  EXPECT_FALSE(s.ok());
  bool success;
  Status s_verify = VerifyCsrByCmd(FLAGS_csr_path, &success);
  EXPECT_FALSE(success);
  ASSERT_FALSE(s_verify.ok());
}

TEST_F(CsrTestNoLocalCsrPasswordPrivateKey,
       LoadCsrWithNoLocalCsrWrongPassword) {
  FLAGS_private_key_password = kTestWrongPrivateKeyPassword;

  OpensslGenerator gen;
  Status s = gen.GetCsr(&test_csr_content_);
  EXPECT_EQ(StatusCode::INTERNAL, s.error_code());
  bool success;
  Status s_verify = VerifyCsrByCmd(FLAGS_csr_path, &success);
  EXPECT_FALSE(success);
  ASSERT_EQ(StatusCode::INTERNAL, s_verify.error_code());
}

TEST_F(CsrTestNoLocalCsrPasswordPrivateKey,
       LoadCsrWithNoLocalCsrEmptyPublicKeyPath) {
  FLAGS_public_key_path = "";

  OpensslGenerator gen;
  Status s = gen.GetCsr(&test_csr_content_);
  EXPECT_EQ(StatusCode::INTERNAL, s.error_code());
  bool success;
  Status s_verify = VerifyCsrByCmd(FLAGS_csr_path, &success);
  EXPECT_FALSE(success);

  ASSERT_EQ(StatusCode::INTERNAL, s_verify.error_code());
}

TEST_F(CsrTestNoLocalCsrPasswordPrivateKey, LoadCsrWithNoLocalCsr) {
  OpensslGenerator gen;
  Status s = gen.GetCsr(&test_csr_content_);
  EXPECT_TRUE(s.ok());
  bool success;
  Status s_verify = VerifyCsrByCmd(FLAGS_csr_path, &success);
  EXPECT_TRUE(success);

  ASSERT_TRUE(s_verify.ok());
}

class CsrTestWithLocalCsr : public ::testing::Test {
 protected:
  void SetUp() override {
    FLAGS_public_key_path = kTestValidPublicKeyPath;
    FLAGS_private_key_path = kTestValidPrivateKeyPath;
    FLAGS_private_key_password = kTestValidPrivateKeyPassword;
    FLAGS_csr_path = kTestOutputCsrPath;
    FLAGS_crt_path = kTestOutputCrtPath;
  }
  std::string test_csr_content_;
};

TEST_F(CsrTestWithLocalCsr, LoadCsrWithEmptyCsr) {
  FLAGS_csr_path = kTestEmptyCsrPath;

  OpensslGenerator gen;
  Status s = gen.GetCsr(&test_csr_content_);
  EXPECT_TRUE(s.ok());
  bool success;
  Status s_verify = VerifyCsrByCmd(FLAGS_csr_path, &success);
  EXPECT_FALSE(success);
  ASSERT_EQ(StatusCode::INTERNAL, s_verify.error_code());
}

TEST_F(CsrTestWithLocalCsr, LoadCsrWithCorruptedCsr) {
  FLAGS_csr_path = kTestCorruptedCsrPath;

  OpensslGenerator gen;
  Status s = gen.GetCsr(&test_csr_content_);
  EXPECT_TRUE(s.ok());
  bool success;
  Status s_verify = VerifyCsrByCmd(FLAGS_csr_path, &success);
  EXPECT_FALSE(success);

  ASSERT_EQ(StatusCode::INTERNAL, s_verify.error_code());
}

TEST_F(CsrTestWithLocalCsr, LoadCsrWithExpectedCsr) {
  FLAGS_csr_path = kTestExpectedCsrPath;

  OpensslGenerator gen;
  Status s = gen.GetCsr(&test_csr_content_);
  ASSERT_TRUE(s.ok());
  bool success;
  Status s_verify = VerifyCsrContentNotEmpty(test_csr_content_, &success);
  EXPECT_TRUE(success);
  ASSERT_TRUE(s_verify.ok());
}
}  // namespace arpa_mpc
