#include "mpc_worker.h"
#include "openssl_generator.h"

#include <iostream>
#include <stdexcept>
#include <string>

#include "glog/logging.h"
#include "grpcpp/grpcpp.h"
#include "gtest/gtest.h"
#define BOOST_NO_CXX11_SCOPED_ENUMS
#include <boost/filesystem.hpp>
#undef BOOST_NO_CXX11_SCOPED_ENUMS

namespace arpa_mpc {
using grpc::Status;
using grpc::StatusCode;

const std::string kTestFolderPath = "server/test_data/test_folder";

class CrtFolderManageTest : public ::testing::Test {
 protected:
  void SetUp() override {
    boost::filesystem::path folder_path(kTestFolderPath);
    CHECK(!boost::filesystem::is_directory(folder_path))
        << "Found unexpected test folder: " + kTestFolderPath;
  }
  void TearDown() override {
    boost::filesystem::path folder_path(kTestFolderPath);
    if (boost::filesystem::is_directory(folder_path)) {
      boost::filesystem::remove_all(folder_path);
    }
  }
};

TEST_F(CrtFolderManageTest, CreateSimpleFolder) {
  Status s = CreateFolder(kTestFolderPath);
  LOG(INFO) << kTestFolderPath;
  ASSERT_TRUE(s.ok());
  ASSERT_TRUE(boost::filesystem::is_directory(kTestFolderPath));
}

TEST_F(CrtFolderManageTest, CreateEmptyFolder) {
  Status s = CreateFolder("");
  ASSERT_EQ(StatusCode::INTERNAL, s.error_code());
}

TEST_F(CrtFolderManageTest, CheckPathExist) {
  boost::filesystem::path folder_path(kTestFolderPath);
  boost::filesystem::create_directory(folder_path);
  bool folder_exist = false;
  Status s = CheckPath(kTestFolderPath, &folder_exist);
  ASSERT_TRUE(s.ok());
  ASSERT_TRUE(folder_exist);
}

TEST_F(CrtFolderManageTest, CheckPathNotExist) {
  bool folder_exist = true;
  boost::filesystem::path folder_path(kTestFolderPath);
  if (boost::filesystem::is_directory(folder_path)) {
    boost::filesystem::remove_all(folder_path);
  }
  Status s = CheckPath(kTestFolderPath, &folder_exist);
  ASSERT_TRUE(s.ok());
  ASSERT_FALSE(folder_exist);
}

TEST_F(CrtFolderManageTest, SetCoworkerCrtFolderVerify) {
  Status s = SetCoworkerCrtFolder(kTestFolderPath);
  ASSERT_TRUE(s.ok());
  ASSERT_TRUE(boost::filesystem::is_directory(kTestFolderPath));
}
TEST_F(CrtFolderManageTest, SetEmptyCoworkerCrtFolder) {
  Status s = SetCoworkerCrtFolder("");
  ASSERT_EQ(StatusCode::INTERNAL, s.error_code());
}

TEST_F(CrtFolderManageTest, CleanUpSimplePath) {
  boost::filesystem::path folder_path(kTestFolderPath);
  boost::filesystem::create_directory((folder_path));

  OpensslGenerator openssl_generator;
  FLAGS_crt_path = "server/test_data/config_test.crt";
  openssl_generator.SaveCertificate("test crt content");

  Status s = CleanUp(kTestFolderPath);
  ASSERT_TRUE(s.ok());
  ASSERT_FALSE(boost::filesystem::is_directory(folder_path));
}
TEST_F(CrtFolderManageTest, CleanUpEmptyPath) {
  Status s = CleanUp("");
  ASSERT_EQ(StatusCode::INTERNAL, s.error_code());
}
}  // namespace arpa_mpc