/*
 *  Copyright (c) 2018, ARPACORP LTD
 *  All rights reserved
 */
#ifndef ARPA_MPC_SERVER_MPC_WORKER_H
#define ARPA_MPC_SERVER_MPC_WORKER_H

#include <grpcpp/grpcpp.h>
#include <grpcpp/security/credentials.h>
#include <iostream>
#include <memory>
#include <string>

#include "gflags/gflags.h"
#include "grpcpp/grpcpp.h"
#include "sys/stat.h"

#include "System/Networking.h"
#include "System/mpc_runtime.h"
#include "Tools/ezOptionParser.h"
#include "arpa_mpc.grpc.pb.h"
#include "config.h"
#include "input_output/input_output_rpc.h"
#include "server/coordinator_client.h"

using arpa_mpc::CoordinatorClient;
using arpa_mpc::HeartbeatRequest;
using arpa_mpc::HeartbeatResponse;
using arpa_mpc::MpcWorker;
using arpa_mpc::StartMpcRequest;
using arpa_mpc::StartMpcResponse;
using grpc::ServerContext;
using grpc::Status;
using std::string;

namespace arpa_mpc {
class MpcContext {
 public:
  MpcContext(int verbose, const string &node_id, CoordinatorClient &coordinator,
             const vector<long> *input_data)
      : verbose(verbose),
        node_id(node_id),
        coordinator(coordinator),
        input_data(input_data) {}

  int verbose;
  const string &node_id;
  CoordinatorClient &coordinator;
  const vector<long> *input_data;
};

// Logic and data behind the server's behavior.
class MpcWorkerImpl final : public arpa_mpc::MpcWorker::Service {
 public:
  MpcWorkerImpl(MpcContext mpc_context) : mpc_context(mpc_context) {}

  Status StartMpc(ServerContext *context, const StartMpcRequest *req,
                  StartMpcResponse *resp) override;

  Status Heartbeat(ServerContext *context, const HeartbeatRequest *req,
                   HeartbeatResponse *resp) override;

  MpcContext mpc_context;
};

void RunServer(MpcContext mpc_context, std::string server_address);

grpc::Status CreateFolder(std::string_view folder_path_sv);

grpc::Status CheckPath(std::string_view file_path_sv, bool *path_exist);

grpc::Status SetCoworkerCrtFolder(std::string_view folder_path_sv);

grpc::Status CleanUp(std::string_view folder_path_sv);
}  // namespace arpa_mpc

#endif  // ARPA_MPC_SERVER_MPC_WORKER_H
