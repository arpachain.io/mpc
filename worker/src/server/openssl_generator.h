#include <string>

#include "gflags/gflags.h"
#include "grpcpp/grpcpp.h"

DECLARE_string(public_key_path);
DECLARE_string(private_key_path);
DECLARE_string(private_key_password);
DECLARE_string(csr_path);
DECLARE_string(crt_path);
DECLARE_string(ca_path);
DECLARE_string(crt_common_name);

namespace arpa_mpc {

class OpensslGenerator {
 private:
  grpc::Status VerifyPrivateKey(std::string_view private_key_path_sv,
                                std::string_view private_key_password_sv,
                                bool *succeed);
  grpc::Status VerifyPublicKey(std::string_view public_key_path_sv,
                               bool *succeed);
  grpc::Status GenerateCsr(std::string_view private_key_path_sv,
                           std::string_view private_key_password_sv,
                           std::string_view csr_path_sv);
  grpc::Status LoadCsr(std::string_view csr_path_sv, std::string *csr);

 public:
  grpc::Status GenerateKeyPair(std::string_view private_key_path_sv,
                               std::string_view private_key_password_sv,
                               std::string_view public_key_path_sv);
  grpc::Status GetCsr(std::string *csr);
  grpc::Status SaveCertificate(std::string_view crt);
  grpc::Status SaveCertificate(std::string_view crt_content_sv,
                               std::string_view crt_path_sv);
};

}  // namespace arpa_mpc
