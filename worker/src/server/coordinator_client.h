/*
 *  Copyright (c) 2018, ARPACORP LTD
 *  All rights reserved
 */

#ifndef ARPA_MPC_SERVER_COORDINATOR_CLIENT_H
#define ARPA_MPC_SERVER_COORDINATOR_CLIENT_H

#include <grpcpp/grpcpp.h>
#include <string_view>

#include "arpa_mpc.grpc.pb.h"

namespace arpa_mpc {

class CoordinatorClient {
 public:
  CoordinatorClient(std::shared_ptr<grpc::Channel> channel)
      : stub_(arpa_mpc::Coordinator::NewStub(channel)) {}

  grpc::Status RegisterNode(std::string_view name, std::string_view address,
                            std::string_view csr, std::string_view blockchain_address,
                            std::string *node_id);

  grpc::Status FinishMpc(std::string_view node_id, std::string_view mpc_id,
                         std::string_view result);

 private:
  std::unique_ptr<arpa_mpc::Coordinator::Stub> stub_;
};
}  // end namespace arpa_mpc
#endif  // ARPA_MPC_SERVER_COORDINATOR_CLIENT_H
