#include "openssl_generator.h"

#include <stdio.h>
#include <string.h>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <streambuf>

#include <glog/logging.h>
#include <grpcpp/grpcpp.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/rsa.h>
#include <openssl/ssl.h>

DEFINE_string(public_key_path, "configure/WorkerPublic.key",
              "worker public key file path");
DEFINE_string(private_key_path, "configure/WorkerPrivate.key",
              "worker private key file path");
DEFINE_string(private_key_password, "", "worker private key password");
DEFINE_string(csr_path, "configure/Worker.csr", "worker csr file path");
DEFINE_string(crt_path, "configure/Worker.crt", "worker crt file path");
DEFINE_string(ca_path, "configure/ca.crt", "ca crt file path");
DEFINE_string(crt_common_name, "arpachain.io", "crt sign process comman name");

namespace arpa_mpc {
using grpc::Status;
using grpc::StatusCode;

// Only use for test, DO NOT call this function individual
// It will override your local key pairs
Status OpensslGenerator::GenerateKeyPair(
    std::string_view private_key_path_sv,
    std::string_view private_key_password_sv,
    std::string_view public_key_path_sv) {
  std::string private_key_file(private_key_path_sv);
  std::string private_key_password(private_key_password_sv);
  std::string public_key_file(public_key_path_sv);
  std::string error_msg;

  if (private_key_file.empty()) {
    error_msg = "Private key path shouldn't be empty";
    LOG(ERROR) << error_msg;
    return Status(StatusCode::INTERNAL, error_msg);
  }

  if (public_key_file.empty()) {
    error_msg = "Public key path shouldn't be empty";
    LOG(ERROR) << error_msg;
    return Status(StatusCode::INTERNAL, error_msg);
  }

  // Parameters for genrate the private_bio key
  int bits = 2048;           // the bits for generate private_bio key
  int ret = 0;               // use as error flag
  BIGNUM *bne = NULL;        // big number
  RSA *rsa = NULL;           // rsa info
  unsigned long e = RSA_F4;  // number use to generate big number
  int private_key_password_length = private_key_password.size();

  // Create a big number
  bne = BN_new();
  ret = BN_set_word(bne, e);
  if (ret != 1) {
    // Add error information
    error_msg = "Generate bne error, with seed e=" + std::to_string(e);
    LOG(ERROR) << error_msg;
    free(bne);
    return Status(StatusCode::INTERNAL, error_msg);
  }
  rsa = RSA_new();
  ret = RSA_generate_key_ex(rsa, bits, bne, NULL);
  if (ret != 1) {
    // Add error infomation
    error_msg = "Generate rsa error, error with RSA_generate_key_ex()";
    LOG(ERROR) << error_msg;
    free(bne);
    RSA_free(rsa);
    return Status(StatusCode::INTERNAL, error_msg);
  }
  // BIO is the structure openssl use as `input` and `output`
  BIO *private_bio = BIO_new(BIO_s_file());
  BIO *public_bio = BIO_new(BIO_s_file());

  // Create private_key_file
  ret = BIO_write_filename(private_bio, (void *)(private_key_file.c_str()));

  if (ret != 1) {
    error_msg = "Create private_key_file error in BIO_write_filename()";
    LOG(ERROR) << error_msg;
    free(bne);
    RSA_free(rsa);
    BIO_free(private_bio);
    return Status(StatusCode::INTERNAL, error_msg);
  }

  // Create public_key_file
  ret = BIO_write_filename(public_bio, (void *)(public_key_file.c_str()));

  if (ret != 1) {
    error_msg = "Create public_key_file error in BIO_write_filename()";
    LOG(ERROR) << error_msg;
    free(bne);
    RSA_free(rsa);
    BIO_free(private_bio);
    BIO_free(public_bio);
    return Status(StatusCode::INTERNAL, error_msg);
  }

  // Generatet private_bio and public_bio by rsa
  if (private_key_password_length == 0) {
    ret = PEM_write_bio_RSAPrivateKey(private_bio, rsa, NULL, NULL,
                                      private_key_password_length, NULL, NULL);

  } else {
    ret = PEM_write_bio_RSAPrivateKey(
        private_bio, rsa, EVP_des_ede3_cbc(),
        (unsigned char *)(private_key_password.c_str()),
        private_key_password_length, NULL, NULL);
  }

  if (ret != 1) {
    error_msg = "Write private_bio error in PEM_write_bio_RSAPrivateKey()";
    LOG(ERROR) << error_msg;
    free(bne);
    RSA_free(rsa);
    BIO_free(private_bio);
    BIO_free(public_bio);
    return Status(StatusCode::INTERNAL, error_msg);
  }

  ret = PEM_write_bio_RSAPublicKey(public_bio, rsa);
  if (ret != 1) {
    error_msg = "Write public_bio error in PEM_write_bio_RSAPublicKey()";
    LOG(ERROR) << error_msg;
    free(bne);
    RSA_free(rsa);
    BIO_free(private_bio);
    BIO_free(public_bio);
    return Status(StatusCode::INTERNAL, error_msg);
  }
  // Manual free memory
  free(bne);
  RSA_free(rsa);
  BIO_free(private_bio);
  BIO_free(public_bio);
  error_msg = "Susseed generate key pairs";
  return Status(StatusCode::OK, error_msg);
}

Status OpensslGenerator::VerifyPrivateKey(
    std::string_view private_key_path_sv,
    std::string_view private_key_password_sv, bool *succeed) {
  std::string private_key_path(private_key_path_sv);
  std::string private_key_password(private_key_password_sv);
  std::string error_msg;
  RSA *rsa = NULL;

  std::ifstream file_checker(private_key_path);
  if (!file_checker.is_open()) {
    *succeed = false;
    error_msg = "Verify private key failed, can't open private key file";
    LOG(ERROR) << error_msg;
    return Status(StatusCode::INTERNAL, error_msg);
  }

  BIO *bio_private_key = BIO_new_file(private_key_path.c_str(), "rb");
  if (bio_private_key == NULL) {
    *succeed = false;
    error_msg = "Loading private key error, bio private key shouldn't be NULL";
    LOG(ERROR) << error_msg;
    BIO_free(bio_private_key);
    return Status(StatusCode::INTERNAL, error_msg);
  }

  rsa = PEM_read_bio_RSAPrivateKey(bio_private_key, &rsa, NULL,
                                   (void *)(private_key_password.c_str()));

  if (rsa == NULL) {
    *succeed = false;
    error_msg = "Read private key bio failed, rsa shouldn't be NULL";
    LOG(ERROR) << error_msg;
    RSA_free(rsa);
    BIO_free(bio_private_key);
    return Status(StatusCode::INTERNAL, error_msg);
  }

  *succeed = true;
  error_msg = "Private key valid";
  LOG(INFO) << error_msg;
  RSA_free(rsa);
  BIO_free(bio_private_key);

  return Status();
}

Status OpensslGenerator::VerifyPublicKey(std::string_view public_key_path_sv,
                                         bool *succeed) {
  std::string public_key_path(public_key_path_sv);
  std::string error_msg;
  BIO *bio_public_key = NULL;
  RSA *rsa = NULL;

  if (public_key_path.empty()) {
    *succeed = false;
    error_msg =
        "Verify public key failed, public key file path shouldn't be empty";
    LOG(ERROR) << error_msg;
    return Status(StatusCode::INTERNAL, error_msg);
  }

  std::ifstream file_checker(public_key_path);
  if (!file_checker.is_open()) {
    *succeed = false;
    error_msg = "Verify public key failed, can't open public key file";
    LOG(ERROR) << error_msg;
    return Status(StatusCode::INTERNAL, error_msg);
  }

  bio_public_key = BIO_new_file(public_key_path.c_str(), "rb");
  if (bio_public_key == NULL) {
    *succeed = false;
    error_msg = "Verify public key error, bio public key shouldn't be NULL";
    LOG(ERROR) << error_msg;
    BIO_free(bio_public_key);
    return Status(StatusCode::INTERNAL, error_msg);
  }
  rsa = PEM_read_bio_RSAPublicKey(bio_public_key, &rsa, NULL, NULL);

  if (!rsa) {
    *succeed = false;
    error_msg = "Verify public key error, rsa shouldn't be NULL";
    LOG(ERROR) << error_msg;
    RSA_free(rsa);
    BIO_free(bio_public_key);
    return Status(StatusCode::INTERNAL, error_msg);
  }
  *succeed = true;
  error_msg = "Public key valid";
  LOG(INFO) << error_msg;
  RSA_free(rsa);
  BIO_free(bio_public_key);
  return Status();
}

Status OpensslGenerator::GenerateCsr(std::string_view private_key_path_sv,
                                     std::string_view private_key_password_sv,
                                     std::string_view csr_path_sv) {
  std::string private_key_path(private_key_path_sv);
  std::string private_key_password(private_key_password_sv);
  std::string csr_path(csr_path_sv);
  std::string error_msg;

  if (private_key_path.empty()) {
    error_msg = "Privatekey path shouldn't be empty";
    LOG(ERROR) << error_msg;
    return Status(StatusCode::INTERNAL, error_msg);
  }
  if (csr_path.empty()) {
    error_msg = "Csr file path shouldn't be empty";
    LOG(ERROR) << error_msg;
    return Status(StatusCode::INTERNAL, error_msg);
  }

  // Variables for genrate the private_bio key
  const char *info_country = "CN";
  const char *info_province = "Beijing";
  const char *info_city = "Chaoyang";
  const char *info_organization = "ARPA";
  const char *info_common = "arpachain.io";

  int ret = 0;  // Error check flag
  int nVersion = 3;
  RSA *rsa = NULL;
  BIO *bio_private_key = NULL;
  EVP_PKEY *private_evp_key = NULL;
  X509_REQ *csr_request = NULL;
  X509_NAME *request_subject_name = NULL;
  BIO *bio_csr_output = NULL;  // BIO container for csr file

  // 1. Load private key
  bio_private_key = BIO_new_file(private_key_path.c_str(), "rb");
  if (bio_private_key == NULL) {
    error_msg = "Loading private key error, bio private key shouldn't be NULL";
    LOG(ERROR) << error_msg;
    return Status(StatusCode::INTERNAL, error_msg);
  }

  rsa = PEM_read_bio_RSAPrivateKey(bio_private_key, &rsa, NULL,
                                   (void *)(private_key_password.c_str()));
  if (rsa == NULL) {
    error_msg = "Read private key bio failed, rsa shouldn't be NULL";
    LOG(ERROR) << error_msg;
    RSA_free(rsa);
    BIO_free(bio_private_key);
    return Status(StatusCode::INTERNAL, error_msg);
  }

  private_evp_key = EVP_PKEY_new();
  if (private_evp_key == NULL) {
    error_msg = "Init evp key failed, privatek evp key shouldn't be NULL";
    LOG(ERROR) << error_msg;
    RSA_free(rsa);
    BIO_free(bio_private_key);
    EVP_PKEY_free(private_evp_key);
    return Status(StatusCode::INTERNAL, error_msg);
  }

  EVP_PKEY_assign_RSA(private_evp_key, rsa);

  // 2. Set version of x509 req
  csr_request = X509_REQ_new();
  // `X509_REQ_set_version` will return 1 if no errors
  ret = X509_REQ_set_version(csr_request, nVersion);
  if (ret != 1) {
    error_msg = "Set csr version error with X509_REQ_set_version()";
    LOG(ERROR) << error_msg;
    RSA_free(rsa);
    BIO_free(bio_private_key);
    EVP_PKEY_free(private_evp_key);
    free(csr_request);
    return Status(StatusCode::INTERNAL, error_msg);
  }

  // 3. Set subject of x509 req
  request_subject_name = X509_REQ_get_subject_name(csr_request);
  ret += X509_NAME_add_entry_by_txt(request_subject_name, "C", MBSTRING_ASC,
                                    (const unsigned char *)info_country, -1, -1,
                                    0);
  ret += X509_NAME_add_entry_by_txt(request_subject_name, "ST", MBSTRING_ASC,
                                    (const unsigned char *)info_province, -1,
                                    -1, 0);
  ret +=
      X509_NAME_add_entry_by_txt(request_subject_name, "L", MBSTRING_ASC,
                                 (const unsigned char *)info_city, -1, -1, 0);
  ret += X509_NAME_add_entry_by_txt(request_subject_name, "O", MBSTRING_ASC,
                                    (const unsigned char *)info_organization,
                                    -1, -1, 0);
  ret +=
      X509_NAME_add_entry_by_txt(request_subject_name, "CN", MBSTRING_ASC,
                                 (const unsigned char *)info_common, -1, -1, 0);

  // At the beginning of step 3, `s = 1` if no errors
  // There will do 5 times `s +=1`
  // So there will be `s = 6` with no errors during
  // `X509_NAME_add_entry_by_txt`
  if (ret != 6) {
    error_msg = "Add region information to csr has error.";
    LOG(ERROR) << error_msg;
    RSA_free(rsa);
    BIO_free(bio_private_key);
    EVP_PKEY_free(private_evp_key);
    free(csr_request);
    X509_NAME_free(request_subject_name);
    return Status(StatusCode::INTERNAL, error_msg);
  }

  // 4. Set public key of x509 req
  ret = X509_REQ_set_pubkey(csr_request, private_evp_key);
  if (ret != 1) {
    error_msg =
        "Generate csr request with private key error in "
        "X509_REQ_ser_pubkey().";
    LOG(ERROR) << error_msg;
    RSA_free(rsa);
    BIO_free(bio_private_key);
    EVP_PKEY_free(private_evp_key);
    free(csr_request);
    X509_NAME_free(request_subject_name);
    return Status(StatusCode::INTERNAL, error_msg);
  }

  // 5. Set sign key of x509 req
  ret = X509_REQ_sign(csr_request, private_evp_key,
                      EVP_sha1());  // return csr_request->signature->length
  if (ret <= 0) {
    error_msg = "Set csr request sign key error in X509_REQ_sig()";
    LOG(ERROR) << error_msg;
    RSA_free(rsa);
    BIO_free(bio_private_key);
    EVP_PKEY_free(private_evp_key);
    free(csr_request);
    X509_NAME_free(request_subject_name);
    return Status(StatusCode::INTERNAL, error_msg);
  }

  // 6. Write to csr file
  bio_csr_output = BIO_new_file(csr_path.c_str(), "w");
  ret = PEM_write_bio_X509_REQ(bio_csr_output, csr_request);
  if (ret <= 0) {
    error_msg = "Write csr to `csr_file` error in PEM_write_bie_X509_REQ()";
    LOG(ERROR) << error_msg;
    RSA_free(rsa);
    BIO_free(bio_private_key);
    EVP_PKEY_free(private_evp_key);
    free(csr_request);
    X509_NAME_free(request_subject_name);
    BIO_free(bio_csr_output);
    return Status(StatusCode::INTERNAL, error_msg);
  }

  // 7. Manual free memory
  RSA_free(rsa);
  BIO_free(bio_private_key);
  EVP_PKEY_free(private_evp_key);
  free(csr_request);
  X509_NAME_free(request_subject_name);
  BIO_free(bio_csr_output);

  return Status::OK;
}

Status OpensslGenerator::LoadCsr(std::string_view csr_path_sv,
                                 std::string *csr) {
  std::string csr_path(csr_path_sv);
  std::string error_msg;

  if (csr_path.empty()) {
    error_msg = "Csr file path shouldn't be empty";
    LOG(ERROR) << error_msg;
    return Status(StatusCode::INTERNAL, error_msg);
  }
  // Check the csr is exist or not
  std::ifstream csr_checker(csr_path);
  if (csr_checker.fail()) {
    error_msg = "Csr file path error with: " + csr_path;
    LOG(ERROR) << error_msg;
    return Status(StatusCode::INTERNAL, error_msg);
  }

  std::ifstream csr_loader(FLAGS_csr_path);
  *csr = std::string((std::istreambuf_iterator<char>(csr_loader)),
                     std::istreambuf_iterator<char>());
  return Status::OK;
}

Status OpensslGenerator::SaveCertificate(std::string_view crt_sv) {
  std::string crt(crt_sv);
  std::string error_msg;
  if (FLAGS_crt_path.empty()) {
    error_msg = "Crt file path shouldn't be empty";
    LOG(ERROR) << error_msg;
    return Status(StatusCode::INTERNAL, error_msg);
  }

  if ((crt).empty()) {
    error_msg = "Crt byte size is 0 :)";
    LOG(ERROR) << error_msg;
    return Status(StatusCode::INTERNAL, error_msg);
  }

  FILE *crt_file;
  crt_file = fopen(FLAGS_crt_path.c_str(), "wb");
  if (!crt_file) {
    error_msg = "Can't create crt file while open the target file";
    LOG(ERROR) << error_msg;
    return Status(StatusCode::INTERNAL, error_msg);
  }
  fwrite(crt.c_str(), sizeof(char), crt.size(), crt_file);

  // fclose will clean up any resources created by fopen
  fclose(crt_file);
  return Status::OK;
}

Status OpensslGenerator::SaveCertificate(std::string_view crt_content_sv,
                                         std::string_view crt_path_sv) {
  std::string crt(crt_content_sv);
  std::string error_msg;
  if (FLAGS_crt_path.empty()) {
    error_msg = "Crt file path shouldn't be empty";
    LOG(ERROR) << error_msg;
    return Status(StatusCode::INTERNAL, error_msg);
  }

  if ((crt).empty()) {
    error_msg = "Crt byte size is 0 :)";
    LOG(ERROR) << error_msg;
    return Status(StatusCode::INTERNAL, error_msg);
  }

  FILE *crt_file;
  std::string filename(crt_path_sv);
  crt_file = fopen(filename.c_str(), "wb");
  if (!crt_file) {
    error_msg = "Can't create crt file while open the target file";
    LOG(ERROR) << error_msg;
    return Status(StatusCode::INTERNAL, error_msg);
  }
  fwrite(crt.c_str(), sizeof(char), crt.size(), crt_file);

  // fclose will clean up any resources created by fopen
  fclose(crt_file);
  return Status::OK;
}

Status OpensslGenerator::GetCsr(std::string *csr) {
  std::string error_msg;
  bool succeed = false;

  if (FLAGS_private_key_path.empty()) {
    error_msg = "Private key path shouldn't be empty";
    LOG(ERROR) << error_msg;
    return Status(StatusCode::INTERNAL, error_msg);
  }
  if (FLAGS_public_key_path.empty()) {
    error_msg = "Public key path shouldn't be empty";
    LOG(ERROR) << error_msg;
    return Status(StatusCode::INTERNAL, error_msg);
  }
  if (FLAGS_csr_path.empty()) {
    error_msg = "Csr file path shouldn't be empty";
    LOG(ERROR) << error_msg;
    return Status(StatusCode::INTERNAL, error_msg);
  }
  // Check the csr is exist or not
  std::ifstream csr_checker(FLAGS_csr_path);
  if (csr_checker.fail()) {
    std::ifstream private_key_checker(FLAGS_private_key_path);
    std::ifstream public_key_checker(FLAGS_public_key_path);

    if (private_key_checker.fail() || public_key_checker.fail()) {
      LOG(INFO)
          << "Private key or public key lost, will generate new key pairs";
      Status s =
          GenerateKeyPair(FLAGS_private_key_path, FLAGS_private_key_password,
                          FLAGS_public_key_path);

      if (!s.ok()) {
        return s;
      }
    }

    Status s = VerifyPrivateKey(FLAGS_private_key_path,
                                FLAGS_private_key_password, &succeed);

    if (!s.ok() || !succeed) {
      return s;
    }

    s = VerifyPublicKey(FLAGS_public_key_path, &succeed);
    if (!s.ok() || !succeed) {
      return s;
    }

    s = GenerateCsr(FLAGS_private_key_path, FLAGS_private_key_password,
                    FLAGS_csr_path);
    if (!s.ok()) {
      return s;
    }
  }

  return LoadCsr(FLAGS_csr_path, csr);
}

}  // namespace arpa_mpc