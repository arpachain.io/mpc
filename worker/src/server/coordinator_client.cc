/*
 *  Copyright (c) 2018, ARPACORP LTD
 *  All rights reserved
 */

#include <grpcpp/grpcpp.h>
#include <iostream>
#include <string>
#include <string_view>

#include "arpa_mpc.grpc.pb.h"
#include "openssl_generator.h"
#include "server/coordinator_client.h"

namespace arpa_mpc {

using arpa_mpc::Coordinator;
using arpa_mpc::FinishMpcRequest;
using arpa_mpc::FinishMpcResponse;
using arpa_mpc::MpcWorker;
using arpa_mpc::RegisterMpcNodeRequest;
using arpa_mpc::RegisterMpcNodeResponse;
using arpa_mpc::StartMpcRequest;
using arpa_mpc::StartMpcResponse;
using grpc::Channel;
using grpc::ClientContext;
using grpc::SecureChannelCredentials;
using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;
using std::string;
using std::string_view;

// Register itself and return the assigned node id in 'node_id'.
// TODO(jiang): Implement certificate param.
Status CoordinatorClient::RegisterNode(string_view name, string_view address,
                                       string_view csr, string_view blockchain_address,
                                       string *node_id) {
  RegisterMpcNodeRequest req;
  req.set_node_name(string(name));
  req.set_address(string(address));
  req.set_csr(string(csr));
  if (!blockchain_address.empty()) {
    req.set_blockchain_address(string(blockchain_address));
  }

  // Container for the data we expect from the server.
  RegisterMpcNodeResponse resp;

  // Context for the client. It could be used to convey extra information to
  // the server and/or tweak certain RPC behaviors.
  ClientContext context;

  // The actual RPC.
  Status status = stub_->RegisterMpcNode(&context, req, &resp);

  // Act upon its status.
  if (status.ok()) {
    *node_id = resp.node_id();

    // read response to get crt
    const std::string crt = resp.cert();
    arpa_mpc::OpensslGenerator generator;
    Status ret = generator.SaveCertificate(crt);
    if (!ret.ok()) {
      return Status(
          grpc::StatusCode::INTERNAL,
          "Register node failed caused by save crt from response failed: " +
              ret.error_message());
    }
  } else {
    std::cout << "Register node failed with error: " << status.error_code()
              << ": " << status.error_message() << std::endl;
  }
  return status;
}

Status CoordinatorClient::FinishMpc(string_view node_id, string_view mpc_id,
                                    string_view result) {
  FinishMpcRequest req;
  req.set_node_id(string(node_id));
  req.set_mpc_id(string(mpc_id));
  req.mutable_result()->set_data(string(result));

  FinishMpcResponse resp;

  // Context for the client. It could be used to convey extra information to
  // the server and/or tweak certain RPC behaviors.
  ClientContext context;

  // The actual RPC.
  Status status = stub_->FinishMpc(&context, req, &resp);

  // Act upon its status.
  if (status.ok()) {
    if (resp.success()) {
      return status;
    }
    std::cout << "FinishMpc is unsuccessful." << std::endl;
    return Status(grpc::StatusCode::UNKNOWN, "FinishMpc is unsuccessful.");
  }
  std::cout << "FinishMpc node failed with error: " << status.error_code()
            << ": " << status.error_message() << std::endl;

  return status;
}
}  // namespace arpa_mpc
