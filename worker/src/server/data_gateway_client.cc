/*
 *  Copyright (c) 2018, ARPACORP LTD
 *  All rights reserved
 */

#include <grpcpp/grpcpp.h>
#include <iostream>
#include <string>

#include "arpa_mpc.grpc.pb.h"
#include "openssl_generator.h"
#include "server/data_gateway_client.h"

namespace arpa_mpc {

using arpa_mpc::Coordinator;
using arpa_mpc::FinishMpcRequest;
using arpa_mpc::FinishMpcResponse;
using arpa_mpc::MpcWorker;
using arpa_mpc::RegisterMpcNodeRequest;
using arpa_mpc::RegisterMpcNodeResponse;
using arpa_mpc::StartMpcRequest;
using arpa_mpc::StartMpcResponse;
using grpc::Channel;
using grpc::ClientContext;
using grpc::SecureChannelCredentials;
using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;
using std::string;

Status DataGatewayClient::GetAllData(std::vector<long>* all_data) {
  GetAllDataRequest req;

  // Container for the data we expect from the server.
  GetAllDataResponse resp;

  // Context for the client. It could be used to convey extra information to
  // the server and/or tweak certain RPC behaviors.
  ClientContext context;

  // The actual RPC.
  Status status = stub_->GetAllData(&context, req, &resp);

  // Act upon its status.
  if (status.ok()) {
    // Hack, assume all data are keyed by their index starting from 1
    // and strip off the key.
    const auto& result = resp.result();
    for (int i = 1; i <= resp.result_size(); i++) {
      auto it = result.find(i);
      if (it == result.end()) {
        return Status(grpc::StatusCode::INTERNAL,
                      "Failed to find data for index " + std::to_string(i) +
                          "resp: " + resp.DebugString());
      }
      all_data->push_back(it->second);
    }
  } else {
    std::cout << "GetAllData failed with error: " << status.error_code()
              << status.error_message() << std::endl;
  }
  return status;
}

}  // end namespace arpa_mpc
