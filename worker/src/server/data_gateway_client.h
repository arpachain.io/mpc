/*
 *  Copyright (c) 2018, ARPACORP LTD
 *  All rights reserved
 */

#ifndef ARPA_MPC_SERVER_DATA_GATEWAY_CLIENT_H
#define ARPA_MPC_SERVER_DATA_GATEWAY_CLIENT_H

#include <grpcpp/grpcpp.h>
#include <string>
#include <vector>

#include "arpa_mpc.grpc.pb.h"

namespace arpa_mpc {

using arpa_mpc::DataGateway;
using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using std::string;

class DataGatewayClient {
 public:
  DataGatewayClient(std::shared_ptr<Channel> channel)
      : stub_(DataGateway::NewStub(channel)) {}

  Status GetAllData(std::vector<long> *all_data);

 private:
  std::unique_ptr<DataGateway::Stub> stub_;
};
}  // end namespace arpa_mpc
#endif  // ARPA_MPC_SERVER_DATA_GATEWAY_CLIENT_H
