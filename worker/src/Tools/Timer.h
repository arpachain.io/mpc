#ifndef _timer
#define _timer

#include <sys/time.h>
#include <sys/wait.h> /* Wait for Process Termination */
#include <time.h>
#include <iostream>

#include "Exceptions/Exceptions.h"

long long timeval_diff(struct timeval *start_time, struct timeval *end_time);
double timeval_diff_in_seconds(struct timeval *start_time,
                               struct timeval *end_time);

// no clock_gettime() on OS X
#ifdef __MACH__
#define timespec timeval
#define clockid_t int
#define CLOCK_MONOTONIC 0
#define CLOCK_PROCESS_CPUTIME_ID 0
#define CLOCK_THREAD_CPUTIME_ID 0
#define timespec_diff timeval_diff
#define clock_gettime(x, y) gettimeofday(y, 0)
#else
long long timespec_diff(struct timespec *start_time, struct timespec *end_time);
#endif

class Timer {
  timespec startv;
  bool running;
  long long elapsed_time;
  clockid_t clock_id;

  long long elapsed_since_last_start() {
    timespec endv;
    clock_gettime(clock_id, &endv);
    return timespec_diff(&startv, &endv);
  }

 public:
  Timer(clockid_t clock_id = CLOCK_MONOTONIC) : clock_id(clock_id) { init(); }

  void init() {
    running = false;
    elapsed_time = 0, clock_gettime(clock_id, &startv);
  }

  void start() {
    if (running) {
      std::cout << "Error: Timer already running." << std::endl;
      throw Timer_Error("Timer already running.");
    }
    // clock() is not suitable in threaded programs so time using something else
    clock_gettime(clock_id, &startv);
    running = true;
  }

  void stop() {
    if (!running) {
      std::cout << "Error: Time not running." << std::endl;
      throw Timer_Error("Time not running.");
    }
    elapsed_time += elapsed_since_last_start();

    running = false;
    clock_gettime(clock_id, &startv);
  }

  void reset() {
    elapsed_time = 0;
    clock_gettime(clock_id, &startv);
  }

  double elapsed();
};

#endif
