#include "input_output/input_output_rpc.h"
#include <limits.h>
#include "Exceptions/Exceptions.h"
#include "arpa_mpc.grpc.pb.h"
#include "gtest/gtest.h"

namespace {
using arpa_mpc::MpcData;

// Test that InputOutputRpc can handle public input from proto message.
TEST(InputOutputRpc, PublicInputInt) {
  MpcData mpcData;
  mpcData.add_public_int(123);

  stringstream output;
  InputOutputRpc io(mpcData, output, true);

  EXPECT_EQ(123, io.PublicInputInt(/*channel=*/1));
}
}  // namespace
