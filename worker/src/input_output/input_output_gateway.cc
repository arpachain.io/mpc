#include "input_output/input_output_gateway.h"
#include "Exceptions/Exceptions.h"

void InputOutputGateway::OpenChannel(unsigned int channel) {
  cout << "Opening channel " << channel << endl;
}

void InputOutputGateway::CloseChannel(unsigned int channel) {
  cout << "Closing channel " << channel << endl;
}

gfp InputOutputGateway::PrivateInputGfp(unsigned int channel) {
  // cout << "PrivateInputGfp Input channel " << channel << " : " << endl;
  long data = GetNextData();
  (*outf) << "PrivateInput: " << data << endl;
  gfp y;
  y.assign(data);
  return y;
}

void InputOutputGateway::PrivateOutputGfp(const gfp &output,
                                          unsigned int channel) {
  (*outf) << "PrivateOutput: ";
  output.output(*outf, true);
  (*outf) << endl;
}

gfp InputOutputGateway::PublicInputGfp(unsigned int channel) {
  long data = GetNextData();
  (*outf) << "PublicInput: " << data << endl;
  gfp y;
  y.assign(data);

  // Important to have this call in each version of PublicInputGfp
  UpdateChecker(y, channel);

  return y;
}

void InputOutputGateway::PublicOutputGfp(const gfp &output,
                                         unsigned int channel) {
  (*outf) << "PublicOutput: ";
  output.output(*outf, true);
  (*outf) << endl;
}

long InputOutputGateway::PublicInputInt(unsigned int channel) {
  long x = GetNextData();
  (*outf) << "PublicInputInt: " << x;

  // Important to have this call in each version of PublicInputGfp
  UpdateChecker(x, channel);

  return x;
}

void InputOutputGateway::PublicOutputInt(const long output,
                                         unsigned int channel) {
  (*outf) << "PublicOutputInt: " << output << endl;
}

void InputOutputGateway::OutputShare(const Share &S, unsigned int channel) {
  (*outf) << "OutputShare: ";
  S.output(*outf, human);
}

Share InputOutputGateway::InputShare(unsigned int channel) {
  cout << "InputShare: RPC share input not implemented yet."
       << "Enter value on channel " << channel << " : ";
  Share S;
  S.input(cin, human);
  return S;
}

void InputOutputGateway::Trigger(Schedule &schedule) {
  printf("Restart requested: Enter a number to proceed\n");
  int i;
  cin >> i;

  // Load new schedule file program streams, using the original
  // program name
  //
  // Here you could define programatically what the new
  // programs you want to run are, by directly editing the
  // public variables in the schedule object.
  unsigned int nthreads = schedule.Load_Programs();
  if (schedule.max_n_threads() < nthreads) {
    throw Processor_Error("Restart requires more threads, cannot do this");
  }
}

void InputOutputGateway::DebugOutput(const stringstream &ss) {
  (*outf) << ss.str().c_str();
}

void InputOutputGateway::Crash(unsigned int PC, unsigned int thread_num) {
  printf("Crashing in thread %d at PC value %d\n", thread_num, PC);
  throw crash_requested();
}

int InputOutputGateway::GetNextData() {
  if (data_index >= input.size()) {
    throw Processor_Error("InputOutputGateway: data index exceeded size.");
  }
  int res = input[data_index];
  data_index++;
  return res;
}
