#include "input_output/input_output_rpc.h"
#include "Exceptions/Exceptions.h"

void InputOutputRpc::OpenChannel(unsigned int channel) {
  cout << "Opening channel " << channel << endl;
}

void InputOutputRpc::CloseChannel(unsigned int channel) {
  cout << "Closing channel " << channel << endl;
}

gfp InputOutputRpc::PrivateInputGfp(unsigned int channel) {
  cout << "PrivateInputGfp Input channel " << channel << " : " << endl;
  gfp y;
  cout << "get input from " << input.ShortDebugString() << endl;
  y.assign(input.private_gfp(private_gfp_index));
  private_gfp_index++;
  return y;
}

void InputOutputRpc::PrivateOutputGfp(const gfp &output, unsigned int channel) {
  (*outf) << "PrivateOutputGfp Output channel " << channel << " : ";
  output.output(*outf, true);
  (*outf) << endl;
}

gfp InputOutputRpc::PublicInputGfp(unsigned int channel) {
  cout << "PublicInputGfp Input on channel " << channel << " : ";
  gfp y;
  y.assign(input.public_gfp(public_gfp_index));
  public_gfp_index++;

  // Important to have this call in each version of PublicInputGfp
  UpdateChecker(y, channel);

  return y;
}

void InputOutputRpc::PublicOutputGfp(const gfp &output, unsigned int channel) {
  (*outf) << "PublicOutputGfp Output channel " << channel << " : ";
  output.output(*outf, true);
  (*outf) << endl;
}

long InputOutputRpc::PublicInputInt(unsigned int channel) {
  cout << "PublicInputInt Input on channel " << channel << " : ";
  long x = input.public_int(public_int_index);
  public_int_index++;

  // Important to have this call in each version of PublicInputGfp
  UpdateChecker(x, channel);

  return x;
}

void InputOutputRpc::PublicOutputInt(const long output, unsigned int channel) {
  (*outf) << "PublicOutputInt Output channel " << channel << " : " << output
          << endl;
}

void InputOutputRpc::OutputShare(const Share &S, unsigned int channel) {
  (*outf) << "OutputShare Output channel " << channel << " : ";
  S.output(*outf, human);
}

Share InputOutputRpc::InputShare(unsigned int channel) {
  cout << "InputShare: RPC share input not implemented yet."
       << "Enter value on channel " << channel << " : ";
  Share S;
  S.input(cin, human);
  return S;
}

void InputOutputRpc::Trigger(Schedule &schedule) {
  printf("Restart requested: Enter a number to proceed\n");
  int i;
  cin >> i;

  // Load new schedule file program streams, using the original
  // program name
  //
  // Here you could define programatically what the new
  // programs you want to run are, by directly editing the
  // public variables in the schedule object.
  unsigned int nthreads = schedule.Load_Programs();
  if (schedule.max_n_threads() < nthreads) {
    throw Processor_Error("Restart requires more threads, cannot do this");
  }
}

void InputOutputRpc::DebugOutput(const stringstream &ss) {
  printf("%s", ss.str().c_str());
  fflush(stdout);
}

void InputOutputRpc::Crash(unsigned int PC, unsigned int thread_num) {
  printf("Crashing in thread %d at PC value %d\n", thread_num, PC);
  throw crash_requested();
}
