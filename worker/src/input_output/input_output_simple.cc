/*
Copyright (c) 2017, The University of Bristol, Senate House, Tyndall Avenue,
Bristol, BS8 1TH, United Kingdom.
Copyright (c) 2018, COSIC-KU Leuven, Kasteelpark Arenberg 10, bus 2452, B-3001
Leuven-Heverlee, Belgium.

All rights reserved
*/

#include "input_output_simple.h"
#include "Exceptions/Exceptions.h"

void InputOutputSimple::OpenChannel(unsigned int channel) {
  cout << "Opening channel " << channel << endl;
}

void InputOutputSimple::CloseChannel(unsigned int channel) {
  cout << "Closing channel " << channel << endl;
}

gfp InputOutputSimple::PrivateInputGfp(unsigned int channel) {
  cout << "Input channel " << channel << " : ";
  word x;
  cin >> x;
  gfp y;
  y.assign(x);
  return y;
}

void InputOutputSimple::PrivateOutputGfp(const gfp &output,
                                         unsigned int channel) {
  cout << "Output channel " << channel << " : ";
  output.output(cout, true);
  cout << endl;
}

gfp InputOutputSimple::PublicInputGfp(unsigned int channel) {
  cout << "Enter value on channel " << channel << " : ";
  word x;
  cin >> x;
  gfp y;
  y.assign(x);

  // Important to have this call in each version of PublicInputGfp
  UpdateChecker(y, channel);

  return y;
}

void InputOutputSimple::PublicOutputGfp(const gfp &output,
                                        unsigned int channel) {
  cout << "Output channel " << channel << " : ";
  output.output(cout, true);
  cout << endl;
}

long InputOutputSimple::PublicInputInt(unsigned int channel) {
  cout << "Enter value on channel " << channel << " : ";
  long x;
  cin >> x;
  // Important to have this call in each version of PublicInputGfp
  UpdateChecker(x, channel);

  return x;
}

void InputOutputSimple::PublicOutputInt(const long output,
                                        unsigned int channel) {
  cout << "Output channel " << channel << " : " << output << endl;
}

void InputOutputSimple::OutputShare(const Share &S, unsigned int channel) {
  (*outf) << "Output channel " << channel << " : ";
  S.output(*outf, human);
}

Share InputOutputSimple::InputShare(unsigned int channel) {
  cout << "Enter value on channel " << channel << " : ";
  Share S;
  S.input(*inpf, human);
  return S;
}

void InputOutputSimple::Trigger(Schedule &schedule) {
  printf("Restart requested: Enter a number to proceed\n");
  int i;
  cin >> i;

  // Load new schedule file program streams, using the original
  // program name
  //
  // Here you could define programatically what the new
  // programs you want to run are, by directly editing the
  // public variables in the schedule object.
  unsigned int nthreads = schedule.Load_Programs();
  if (schedule.max_n_threads() < nthreads) {
    throw Processor_Error("Restart requires more threads, cannot do this");
  }
}

void InputOutputSimple::DebugOutput(const stringstream &ss) {
  printf("%s", ss.str().c_str());
  fflush(stdout);
}

void InputOutputSimple::Crash(unsigned int PC, unsigned int thread_num) {
  printf("Crashing in thread %d at PC value %d\n", thread_num, PC);
  throw crash_requested();
}
