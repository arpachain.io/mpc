
#ifndef ARPA_INPUT_OUTPUT_GATEWAY_H
#define ARPA_INPUT_OUTPUT_GATEWAY_H

/*
 * An IO class which takes input/output from DataGateway API.
 */

#include <fstream>
#include <memory>
#include <vector>

#include "LSSS/Share.h"
#include "arpa_mpc.grpc.pb.h"
#include "input_output_base.h"

using arpa_mpc::MpcData;
using std::istream;
using std::ostream;
using std::unique_ptr;

class InputOutputGateway : public InputOutputBase {
  const std::vector<long> &input;
  int data_index = 0;
  // unique_ptr<MpcData> output;
  ostream *outf;

  bool human;  // Only affects share output

 public:
  // The caller shouldn't modify input and output during the life cycle of this
  // class.
  InputOutputGateway(const std::vector<long> &input, ostream &output,
                     bool human_type)
      : InputOutputBase(), input(input), outf(&output), human(human_type) {}

  /*
  void init(const MpcData input, ostream &output, bool human_type) {
    input = input;
    // output = std::move(output);
    outf = &output;
    human = human_type;
  }
  */

  virtual void OpenChannel(unsigned int channel);
  virtual void CloseChannel(unsigned int channel);

  virtual gfp PrivateInputGfp(unsigned int channel);
  virtual void PrivateOutputGfp(const gfp &output, unsigned int channel);

  virtual void PublicOutputGfp(const gfp &output, unsigned int channel);
  virtual gfp PublicInputGfp(unsigned int channel);

  virtual void PublicOutputInt(const long output, unsigned int channel);
  virtual long PublicInputInt(unsigned int channel);

  virtual void OutputShare(const Share &S, unsigned int channel);
  virtual Share InputShare(unsigned int channel);

  virtual void Trigger(Schedule &schedule);

  virtual void DebugOutput(const stringstream &ss);

  virtual void Crash(unsigned int PC, unsigned int thread_num);

 private:
  int GetNextData();
};

#endif
