
#include "input_output_base.h"

InputOutputBase::InputOutputBase() { SHA256_Init(&sha256); }

void InputOutputBase::UpdateChecker(const stringstream &ss) {
  SHA256_Update(&sha256, ss.str().c_str(), ss.str().size());
}

void InputOutputBase::UpdateChecker(const gfp &input, unsigned int channel) {
  stringstream ss;
  ss << "GFP:" << channel << " " << input;
  UpdateChecker(ss);
}

void InputOutputBase::UpdateChecker(const long input, unsigned int channel) {
  stringstream ss;
  ss << "REGI :" << channel << " " << input;
  UpdateChecker(ss);
}

string InputOutputBase::GetCheck() {
  unsigned char hash[SHA256_DIGEST_LENGTH];
  SHA256_Final(hash, &sha256);
  string ss((char *)hash, SHA256_DIGEST_LENGTH);
  SHA256_Init(&sha256);
  return ss;
}
