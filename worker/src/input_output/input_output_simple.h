/*
Copyright (c) 2017, The University of Bristol, Senate House, Tyndall Avenue,
Bristol, BS8 1TH, United Kingdom.
Copyright (c) 2018, COSIC-KU Leuven, Kasteelpark Arenberg 10, bus 2452, B-3001
Leuven-Heverlee, Belgium.

All rights reserved
*/
#ifndef _InputOutputSimple
#define _InputOutputSimple

/* A simple IO class which just uses standard
 * input/output to communicate values
 *
 * Whereas share values are input/output using
 * a steam, with either human or non-human form
 */

#include "input_output_base.h"

#include <fstream>
using namespace std;

class InputOutputSimple : public InputOutputBase {
  istream *inpf;
  ostream *outf;

  bool human;  // Only affects share output

 public:
  InputOutputSimple(istream &ifs, ostream &ofs, bool human_type)
      : InputOutputBase() {
    inpf = &ifs;
    outf = &ofs;
    human = human_type;
  }

  virtual void OpenChannel(unsigned int channel);
  virtual void CloseChannel(unsigned int channel);

  virtual gfp PrivateInputGfp(unsigned int channel);
  virtual void PrivateOutputGfp(const gfp &output, unsigned int channel);

  virtual void PublicOutputGfp(const gfp &output, unsigned int channel);
  virtual gfp PublicInputGfp(unsigned int channel);

  virtual void PublicOutputInt(const long output, unsigned int channel);
  virtual long PublicInputInt(unsigned int channel);

  virtual void OutputShare(const Share &S, unsigned int channel);
  virtual Share InputShare(unsigned int channel);

  virtual void Trigger(Schedule &schedule);

  virtual void DebugOutput(const stringstream &ss);

  virtual void Crash(unsigned int PC, unsigned int thread_num);
};

#endif
