/*
 *  Copyright (c) 2018, ARPACORP LTD
 *  All rights reserved
 */

#include <arpa/inet.h>
#include <gflags/gflags.h>
#include <glog/logging.h>
#include <grpcpp/grpcpp.h>
#include <grpcpp/security/credentials.h>
#include <pthread.h>
#include <unistd.h>
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <list>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

#include "System/Networking.h"
#include "System/mpc_runtime.h"
#include "Tools/ezOptionParser.h"
#include "arpa_mpc.grpc.pb.h"
#include "config.h"
#include "input_output/input_output_rpc.h"
#include "server/coordinator_client.h"
#include "server/data_gateway_client.h"
#include "server/mpc_worker.h"
#include "server/openssl_generator.h"

using arpa_mpc::Coordinator;
using arpa_mpc::CoordinatorClient;
using arpa_mpc::DataGatewayClient;
using arpa_mpc::FinishMpcRequest;
using arpa_mpc::FinishMpcResponse;
using arpa_mpc::MpcContext;
using arpa_mpc::MpcWorker;
using arpa_mpc::MpcWorkerImpl;
using arpa_mpc::RegisterMpcNodeRequest;
using arpa_mpc::RegisterMpcNodeResponse;
using arpa_mpc::StartMpcRequest;
using arpa_mpc::StartMpcResponse;
using grpc::Channel;
using grpc::ClientContext;
using grpc::SecureChannelCredentials;
using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;
using std::string;

using namespace std;
using namespace ez;

static bool NonEmptyStr(const char *flagname, const string &value) {
  if (!value.empty()) {
    return true;
  }
  LOG(ERROR) << "Invalid value for --" << flagname << value;
  return false;
}

DEFINE_string(coordinator_addr, "", "Coordinator address (required).");
DEFINE_validator(coordinator_addr, NonEmptyStr);

DEFINE_string(
    datagateway_addr, "",
    "DataGateway server address. If specified, the worker will get input "
    "data from DataGateway, otherwise, it will get input from "
    "StartMpcRequest. Default is empty.");

DEFINE_string(rpc_addr, "localhost:8000",
              "Addres of grpc server (default: localhost:8000)");

DEFINE_string(blockchain_address, "",
              "Eth account address that is associated with this worker. "
              "(Optional, if not specified, this worker can't participate in "
              "mpc computation initiated from ETH network.) E.g. "
              "0xC2D7CF95645D33006175B78989035C7c9061d3F9");

DEFINE_int32(
    verbose, 0,
    "Set the verbose level. If positive the higher the value the more"
    "stuff printed for offline. If negative we print verbose for the online"
    "phase");

int main(int argc, char *argv[]) {
  // Initialize Google's flag and logging library.
  FLAGS_logtostderr = 1;  // Print log to stderr.
  google::InitGoogleLogging(argv[0]);
  gflags::ParseCommandLineFlags(&argc, &argv, true);

  // TODO(jiang): Fix the usage msg.
  gflags::SetUsageMessage(
      "Usage of the main worker.x program features."
      "./worker.x [OPTIONS]\n"
      "./worker.x --coordinator_addr=localhost:10000 \\ \n"
      "   -rpc_addr=localhost:8000 -datagateway_addr=localhost:11000\n");

  // Do not start online phase until we have m triples, s squares and b bits.
  // Must be the same on each machine.
  vector<int> minimums{0, 0, 0};

  // Stop the offline phase when m triples, s squares and b bits have been
  // produced. Must be the same on each machine."
  vector<int> maximums{0, 0, 0};

  // Setup connection with coordinator.
  // TODO(jiang): Use secure channel instead.
  std::shared_ptr<grpc::Channel> channel = grpc::CreateChannel(
      FLAGS_coordinator_addr, grpc::InsecureChannelCredentials());
  CHECK(channel != nullptr) << "Failed to connect to coordinator.";

  LOG(INFO) << "Successfully connected to coordinator: "
            << FLAGS_coordinator_addr;
  CoordinatorClient coordinator(channel);
  // Register this worker with coordinator.
  std::string name = ("mpc_worker_" + FLAGS_rpc_addr);
  LOG(INFO) << "Register worker: " << name;
  string my_node_id;

  std::string csr;
  arpa_mpc::OpensslGenerator generator;
  Status s_loadcsr = generator.GetCsr(&csr);
  CHECK(s_loadcsr.ok()) << "LoadCsr returns" << s_loadcsr.error_code() << " "
                        << s_loadcsr.error_message();
  Status s = coordinator.RegisterNode(name, FLAGS_rpc_addr, csr,
                                      FLAGS_blockchain_address, &my_node_id);

  CHECK(s.ok()) << "RegisterNode returns: " << s.error_code() << " "
                << s.error_message();
  LOG(INFO) << "Established connection with coordinator. "
            << "Get assigned node id:" << my_node_id;

  unique_ptr<vector<long>> input_data;
  if (!FLAGS_datagateway_addr.empty()) {
    // Setup connection with DataGateway.
    // TODO(jiang): Use secure channel instead.
    std::shared_ptr<grpc::Channel> gateway_channel = grpc::CreateChannel(
        FLAGS_datagateway_addr, grpc::InsecureChannelCredentials());
    CHECK(gateway_channel != nullptr) << "Failed to connect to DataGateway.";
    LOG(INFO) << "Successfully connected to DataGateway: "
              << FLAGS_datagateway_addr;
    DataGatewayClient gateway(gateway_channel);

    input_data = std::make_unique<std::vector<long>>();
    s = gateway.GetAllData(input_data.get());

    CHECK(s.ok()) << "GetAllData returns: " << s.error_code() << " "
                  << s.error_message();
    LOG(INFO) << "Established connection with DataGateway. "
              << "Get data size: " << input_data->size();
  }

  MpcContext mpc_context(FLAGS_verbose, my_node_id, coordinator,
                         input_data.get());

  arpa_mpc::RunServer(mpc_context, FLAGS_rpc_addr);  // For grpc.
}
