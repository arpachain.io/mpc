all: coordinator datagateway worker

.PHONY: coordinator
coordinator:
	cd coordinator ; make

.PHONY: datagateway
datagateway:
	cd datagateway; make

.PHONY: worker
worker:
	cd worker; make

check:
	cd coordinator; make check
	cd datagateway; make check
	cd eth; make check
	cd worker; make check

clean:
	-cd coordinator ; make clean
	-cd datagateway ; make clean
	-cd worker ; make clean
