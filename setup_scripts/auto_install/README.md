# Auto env config scripts intro
These two scripts can auto install depandances and run test with SCALE v1.1  
They have compatbility with Debian 10.1 and Ubuntu 18.04, divided by file name.  

Tested success OS:

- gcloue Debian GNU/Linux 9 (stretch)
- VirtualBox Debian 10.1
- VMware Debian 10.1
- VirtualBox Ubuntu 18.04 LTS
- VMware Ubuntu 18.04 LTS

There are two config parameters in `auto_install_OS-VERSION.sh` script:

- line 12, USEPROXY=0
    + this config stand for use proxy or not,
    + SET 0 WHEN USE IN GCLOUD 
    + SET 1 ONLY in VritualBox, VMware or windows Subsystem case, and need host machine has proxy software, enable `other connections` in shadowsocks 
- line 19, export HOSTPROXYIP=192.168.50.254:1080
    + this config stand for host instance ip address and port


# How to use?
## Install depandances
For fresh instance, you need 2 step.  
First step is config GOPATH, GOROOT and PATH variable  
Second step is auto install all dependances and run test for SCALE v1.1
 
- upload auto_install.sh to instance
- first run command `bash auto_install_OS-VERSION.sh`
- env variable config finish
- close current terminal and reopen a terminal
- in new terminal run `bash auto_install_OS-VERSION.sh` again for auto install depandances

## Init mpc repo
After install all env config, you need to git clone mpc repo to your machine.

Pre-request

- add local ssh public key(`.ssd/id_rsa.pub) to your github.com and gitlab.com

run command `bash init_mpc.sh` will clone mpc repo, init worker's `CONFIG.mine` config file, run `make` and `make check` in mpc folder.
