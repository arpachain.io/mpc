#!/bin/bash
# This script target with 
#	`windows subsystem - Debian` 
#   `gcloud - Debian GNU/Linux 9 (stretch)`
#   `Vbox - GNU/Linux 10.1(stretch)` RECOMMAND
#   `
# If use Virtualbox, you need config proxy, and enable ss connection in host
#
set -ex
#  Script config 
# use shadowsocks proxy or not (default 0 false, 1 true)
USEPROXY=1

# check GOPATH, GOROOT is config or not
if [[ $GOROOT == "" ]]; then
echo '' >> ~/.bashrc
if [[ $USEPROXY == "1" ]]; then
# host machine local ip address and port
echo 'export HOSTPROXYIP=192.168.50.254:1080' >> ~/.bashrc
echo 'export http_proxy=http://$HOSTPROXYIP' >> ~/.bashrc
echo 'export https_proxy=http://$HOSTPROXYIP' >> ~/.bashrc
echo 'alias go="https_proxy=$HOSTPROXYIP go"' >> ~/.bashrc
fi
echo 'export GO111MODULE=on' >> ~/.bashrc
echo 'export GOROOT=/usr/local/go' >> ~/.bashrc
echo 'export GOPATH=~/go' >> ~/.bashrc
echo 'export PATH=$PATH:$GOPATH/bin:$GOROOT/bin:~/.npm-global/bin' >> ~/.bashrc
echo 'export NPM_CONFIG_PREFIX=~/.npm-global' >> ~/.bashrc
echo "Finish set up GOPATH, GOROOT and add to PATH variable to ~/.bashrc, please reload this script in NEW TERMIANL to install dependances"
exit 1
fi

# install required tools
echo "Install env required tools..."
sudo apt-get update \
&& sudo apt-get --yes install apt-utils \
&& sudo apt-get --yes install apt-transport-https \
&& sudo apt-get --yes install software-properties-common \
&& sudo apt-get --yes install unzip \
&& sudo apt-get --yes install wget \
&& sudo apt-get --yes install curl \
&& sudo apt-get --yes install cmake \
&& sudo apt-get --yes install bzip2 \
&& sudo apt-get --yes install git \
&& sudo apt-get --yes install bash-completion \
&& sudo apt-get --yes install clang-format \
&& sudo apt-get --yes install build-essential autoconf libtool pkg-config

# install gcc
OSVERSION="$(awk -F= '/^NAME/{print $2}' /etc/os-release)" 
if [[ $OSVERSION == '"Debian GNU/Linux"' ]]; then
## debian9 install gcc 7
echo "Insstall gcc 7 for debian 10"
sudo add-apt-repository 'deb http://deb.debian.org/debian buster main' \
&& sudo apt-get update \
&& sudo apt-get --yes install gcc-7 \
&& sudo apt-get --yes install g++-7 \
&& sudo rm /usr/bin/cpp /usr/bin/gcc /usr/bin/g++ \
&& sudo ln -s /usr/bin/cpp-7 /usr/bin/cpp \
&& sudo ln -s /usr/bin/gcc-7 /usr/bin/gcc \
&& sudo ln -s /usr/bin/g++-7 /usr/bin/g++ \
&& gcc -v
elif [[ $OSVERSION == '"Ubuntu"' ]]; then
## ubuntu1804 install gcc
echo "Install gcc for ubuntu 18.04.3"
sudo apt-get --yes install gcc
elif [[ $OSVERSION == '"elementary OS"' ]]; then
sudo apt-get --yes install gcc
fi

# install gRPC and protobuf, prerequest openssl
cd \
&& sudo apt-get update \
&& sudo apt-get --yes install libgflags-dev libgtest-dev \
&& sudo apt-get --yes install clang libc++-dev \
&& cd
if [[ ! -d "$HOME/grpc" ]]; then
echo "Download grpc ..."
git clone -b v1.16.0 https://github.com/grpc/grpc
fi
cd grpc \
&& git submodule update --init \
&& sudo make \
&& sudo make install \
&& sudo make install grpc-cli \
&& cd third_party/protobuf \
&& sudo make install

# install openssl 1.1.1
cd \
&& sudo apt-get update
if [[ ! -f "$HOME/openssl-1.1.1.tar.gz" ]]; then
echo "Download openssl-1.1.1.tar.gz ..."
wget https://www.openssl.org/source/openssl-1.1.1.tar.gz
fi
tar -xvf openssl-1.1.1.tar.gz \
&& cd openssl-1.1.1 \
&& ./config \
&& make \
&& sudo make install \
&& sudo ldconfig

# install go, go tools, dep, and protoc-gen-go plugin
sudo apt-get update \
&& mkdir -p /usr/local/ \
&& cd
if [[ ! -f "$HOME/go1.13.1.linux-amd64.tar.gz" ]]; then
echo "Download go1.13.1.linux-amd64.tar.gz ..."
wget https://dl.google.com/go/go1.13.1.linux-amd64.tar.gz
fi
sudo tar -C /usr/local -xzf go1.13.1.linux-amd64.tar.gz \
&& go version \
&& go env \
&& go tool \
&& go get -u -v google.golang.org/grpc \
&& go get -u -v github.com/golang/mock/gomock \
&& go get -u -v github.com/golang/mock/mockgen \
&& go get -u -v github.com/processout/grpc-go-pool

# install utils including glog 
sudo apt-get update \
&& sudo apt-get --yes install cmake \
&& cd
if [[ ! -d "$HOME/glog" ]]; then
echo "Download glog ..."
git clone https://github.com/google/glog
fi
cd glog
if [[ ! -d "$HOME/glog/build" ]];then
mkdir build
fi
cd build \
&& cmake -D BUILD_gflags_LIBS=ON -D BUILD_SHARED_LIBS=ON -D BUILD_gflags_nothreads_LIBS=ON -D GFLAGS_NAMESPACE=ON .. \
&& make \
&& sudo make install \
&& sudo ldconfig

# install nodejs, npm, truffle, ganache-cli
cd \
&& sudo apt-get update \
&& curl -sL https://deb.nodesource.com/setup_11.x | sudo bash - \
&& sudo apt-get --yes install nodejs \
&& node --version \
&& sudo apt-get install npm -y \
&& npm --version \
&& mkdir ~/.npm-global \
&& npm config set prefix '~/.npm-global' \
&& npm install --global --unsafe-perm truffle@v5.0.1 \
&& truffle version \
&& npm install --global --unsafe-perm ganache-cli \
&& ganache-cli --version \
&& npm install --global --unsafe-perm openzeppelin-solidity@v2.1.0-rc.2

# install solc
cd \
&& sudo curl -o /usr/bin/solc -fL https://github.com/ethereum/solidity/releases/download/v0.5.4/solc-static-linux \
&& sudo chmod +x /usr/bin/solc \
&& solc --version

# install go-ethereum
sudo apt-get update \
&& cd \
&& git clone https://github.com/ethereum/go-ethereum.git \
&& cd go-ethereum \
&& git checkout v1.9.6 \
&& go mod init \
&& make devtools

# install prerequisite for SCALE-MAMBA (yasm, mpir, gmpy2, openssl)
# build and test-run SCALE-MAMBA
sudo apt-get update \
&& sudo apt-get install libboost-filesystem-dev -y \
&& sudo apt-get --yes install yasm \
&& cd
if [[ ! -f "$HOME/mpir-3.0.0.tar.bz2" ]]; then
echo "Download mpir-3.0.0.tar.bz2 ..."
wget http://mpir.org/mpir-3.0.0.tar.bz2
fi
tar -xvf mpir-3.0.0.tar.bz2 \
&& cd mpir-3.0.0 \
&& ./configure --enable-cxx \
&& make \
&& make check \
&& sudo make install \
&& sudo apt-get --yes install python-dev \
&& sudo apt-get --yes install python-gmpy2

# use official SCALE-MAMBA v1.1 test env configuration
cd
if [[ ! -d "$HOME/scale" ]]; then
echo "Not found scale folder, download scale ..."
git clone https://github.com/KULeuven-COSIC/SCALE-MAMBA.git scale
fi
cd scale
## checkout SCALE-MAMBA v1.1
git checkout e05c19efa943a5a79337fd4952edae208515e994
if [[ -f "$HOME/scale/CONFIG.mine" ]]; then
sudo rm "$HOME/scale/CONFIG.mine"
fi
echo "ROOT = $HOME/scale" >> CONFIG.mine \
&& echo "OSSL = $HOME/openssl-1.1.1" >> CONFIG.mine \
&& echo "FLAGS = -DMAX_MOD_SZ=7" >> CONFIG.mine \
&& echo "OPT = -O3" >> CONFIG.mine \
&& echo "LDFLAGS =" >> CONFIG.mine \
&& make \
&& cp Auto-Test-Data/Cert-Store/* Cert-Store/ \
&& cp Auto-Test-Data/1/* Data/ \
&& sudo sh -c "echo '/usr/local/lib' >> /etc/ld.so.conf.d/libs.conf" \
&& sudo ldconfig \
&& Scripts/test.sh test_sqrt