#!/bin/bash
# This script target with 
#   `Vbox - Ubuntu 18.04`
#   
# If use Virtualbox, you need config proxy, and enable ss connection in host machine
#
#  Script config 
# use shadowsocks proxy or not (default 0 false, 1 true)
USEPROXY=1

# check GOPATH is config or not
if [[ $GOPATH == "" ]]; then
echo '' >> ~/.bashrc
if [[ $USEPROXY == "1" ]]; then
# host machine local ip address and port
echo 'export HOSTPROXYIP=192.168.50.254:1080' >> ~/.bashrc
echo 'export http_proxy=http://$HOSTPROXYIP' >> ~/.bashrc
echo 'export https_proxy=http://$HOSTPROXYIP' >> ~/.bashrc
echo 'alias go="https_proxy=$HOSTPROXYIP go"' >> ~/.bashrc
fi
echo 'export GO111MODULE=on' >> ~/.bashrc
echo 'export GOPATH=~/go' >> ~/.bashrc
echo 'export PATH=$PATH:$GOPATH/bin' >> ~/.bashrc
echo "Finish set up GOPATH and add it to PATH variable to ~/.bashrc, reopen this script to install"
exit 1
fi

set -ex
sudo add-apt-repository -y ppa:ethereum/ethereum \
&& sudo apt update \
&& sudo apt install --yes build-essential autoconf automake libtool pkg-config cmake clang-format libgflags-dev libgtest-dev python-dev git curl libboost-filesystem-dev python-gmpy2 yasm ethereum \
&& sudo snap install go --classic \
&& sudo snap install solc \
&& cd \
&& curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.0/install.sh | bash \
&& nvm install 11.15.0 \
&& nvm alias default 11 \
&& npm install --global truffle@v5.0.1 \
&& npm install --global openzeppelin-solidity@v2.1.0-rc.2 \
&& go get -v -u google.golang.org/grpc \
&& go get -v -u github.com/golang/protobuf/protoc-gen-go \
&& go get -v -u github.com/golang/mock/mockgen \
&& cd \
&& git clone https://github.com/ethereum/go-ethereum \
&& cd go-ethereum \
&& git checkout v1.9.6 \
&& go mod init \
&& make devtools

cd \
&& wget http://mpir.org/mpir-3.0.0.tar.bz2 \
&& tar -xvf mpir-3.0.0.tar.bz2 \
&& cd mpir-3.0.0 \
&& ./configure --enable-cxx \
&& make \
&& sudo make install \
&& sudo ldconfig # refresh shared library cache

cd \
&& wget https://www.openssl.org/source/openssl-1.1.1.tar.gz \
&& tar -xvf openssl-1.1.1.tar.gz \
&& cd openssl-1.1.1 \
&& ./config \
&& make \
&& sudo make install \
&& sudo ldconfig # refresh shared library cache

cd \
&& git clone https://github.com/google/glog \
&& cd glog \
&& ./autogen.sh \
&& ./configure \
&& make \
&& sudo make install \
&& sudo ldconfig

cd \
&& git clone -b $(curl -L https://grpc.io/release) https://github.com/grpc/grpc \
&& cd grpc \
&& git submodule update --init \
&& make \
&& sudo make install \
&& make grpc_cli \
&& sudo make install-grpc-cli \
&& cd third_party/protobuf \
&& sudo make install \
&& sudo ldconfig # refresh shared library cache

