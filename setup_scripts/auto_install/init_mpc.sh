#!/bin/bash
# init worker repo
# pre-request:
# 	you need to add you ssh key to gitlab and gitlab account
#	mpc repo will place at /$HOME/go/src/gitlab.com/arpachain/
if [[ ! -d "$HOME/go/src" ]]; then
mkdir $HOME/go/src
echo "[INFO] create folder: $HOME/go/src"
fi

if [[ ! -d "$HOME/go/src/gitlab.com" ]]; then
mkdir $HOME/go/src/gitlab.com
echo "[INFO] create folder: $HOME/go/src/gitlab.com"
fi

if [[ ! -d "$HOME/go/src/gitlab.com/arpachain" ]]; then
cd $HOME/go/src/gitlab.com \
&& mkdir $HOME/go/src/gitlab.com/arpachain
echo "[INFO] create folder: $HOME/go/src/gitlab.com/arpachain"
fi

if [[ ! -d "$HOME/go/src/gitlab.com/arpachain/mpc" ]]; then
echo "[INFO] clone mpc to folder: $HOME/go/src/gitlab.com/arpachain"
cd $HOME/go/src/gitlab.com/arpachain \
&& git clone git@gitlab.com:arpachain/mpc.git
fi

# make worker
cd $HOME/go/src/gitlab.com/arpachain/mpc/worker \
&& git submodule update --init \
&& echo "ROOT = $HOME/go/src/gitlab.com/arpachain/mpc/worker" >> CONFIG.mine \
&& echo "OSSL = $HOME/openssl-1.1.1" >> CONFIG.mine \
&& echo "FLAGS = -DMAX_MOD_SZ=7" >> CONFIG.mine \
&& echo "OPT = -O3" >> CONFIG.mine \
&& echo "LDFLAGS =" >> CONFIG.mine \
&& cp Auto-Test-Data/Cert-Store/* Cert-Store/ \
&& cp Auto-Test-Data/1/* Data/

# npm install in smart_contract folder
cd $HOME/go/src/gitlab.com/arpachain/mpc/smart_contract \
&& npm install

# npm install in eth/test folder and do audit fix
cd $HOME/go/src/gitlab.com/arpachain/mpc/eth/test \
&& npm install \
&& npm audit fix

# make all and make check
cd $HOME/go/src/gitlab.com/arpachain/mpc \
&& make \
&& make check