package test_util

import (
	"gitlab.com/arpachain/mpc/coordinator/api"
	"gitlab.com/arpachain/mpc/coordinator/mock/certificate"
	"gitlab.com/arpachain/mpc/coordinator/pkg/group"
	"gitlab.com/arpachain/mpc/coordinator/pkg/mpc"
	"gitlab.com/arpachain/mpc/coordinator/pkg/node"
	"gitlab.com/arpachain/mpc/coordinator/pkg/program"
	"gitlab.com/arpachain/mpc/coordinator/pkg/util"
	"github.com/golang/mock/gomock"
)

const (
	numNodes  = 5
	numMPC    = 5
	numGroups = 5
)

func CreateNodes(n int, repository node.Repository) []string {
	ids := make([]string, n)
	for i := 0; i < n; i++ {
		ids[i], _ = repository.Create(GenerateTestNode())
	}
	return ids
}

func CreateGroups(n int, repository group.Repository) []string {
	ids := make([]string, n)
	for i := 0; i < n; i++ {
		ids[i], _ = repository.Create(GenerateTestGroup())
	}
	return ids
}

func GenerateTestNode() *node.Node {
	fakeBlockchainAddress, _ := util.GenerateID()
	return &node.Node{
		Name:        "testNode",
		Address:     "127.0.0.1:6000",
		IsGrouped:   false,
		Certificate: []byte("fake"),
		BlockchainAddress:  fakeBlockchainAddress,
	}
}

func GenerateTestGroup() *group.Group {
	fakeNodeIDs := GenerateNodeIDs(numNodes)
	return &group.Group{
		NodeIDs: fakeNodeIDs,
	}
}

func SetupMockCertificateGenerator(certificateGenerator *mock_certificate.MockGenerator) {
	certificateGenerator.
		EXPECT().
		GenerateCertificate(gomock.Any(), gomock.Any(), gomock.Any()).
		Return([]byte("fake_crt"), nil).
		AnyTimes()
}

func CreateMPC(n int, repository mpc.Repository) []string {
	ids := make([]string, n)
	for i := 0; i < n; i++ {
		// For testing, each mpc will have n node ids
		nodeIDs := GenerateNodeIDs(numNodes)
		mpc := GenerateMPC(nodeIDs)
		ids[i], _ = repository.Create(mpc)
	}
	return ids
}

func GenerateNodeIDs(n int) []string {
	ids := make([]string, n)
	for i := 0; i < n; i++ {
		ids[i], _ = util.GenerateID()
	}
	return ids
}

func GenerateMPC(nodeIDs []string) *mpc.MPC {
	resultMap := make(map[string]arpa_mpc.MpcResult)
	finishFlagMap := make(map[string]bool)
	for _, nodeID := range nodeIDs {
		finishFlagMap[nodeID] = false
	}
	return &mpc.MPC{
		NodeIDs:       nodeIDs,
		FinishFlagMap: finishFlagMap,
		ResultMap:     resultMap,
	}
}

func GenerateTestProgram() *program.Program {
	id, _ := util.GenerateID()
	return &program.Program{
		ID:         id,
		ProgramMap: make(map[string][]byte),
		Schedule:   "dummy_schedule",
	}
}
