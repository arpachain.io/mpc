package server

import (
	"context"
	"fmt"
	"log"
	"time"

	pb "gitlab.com/arpachain/mpc/coordinator/api"
	"gitlab.com/arpachain/mpc/coordinator/pkg/blockchain"
	"gitlab.com/arpachain/mpc/coordinator/pkg/node"
)

type HealthChecker interface {
	StartHeartbeat(heartbeatInterval, retryInterval time.Duration, numRetries int)
	StopHeartbeat()
}

type healthCheckServer struct {
	mapper              blockchain.Mapper
	repository          node.Repository
	workerClientFactory WorkerClientFactory
	quit                chan struct{}
}

func NewHealthChecker(
	mapper blockchain.Mapper,
	repository node.Repository,
	workerClientFactory WorkerClientFactory,
) HealthChecker {
	return &healthCheckServer{
		mapper:              mapper,
		repository:          repository,
		workerClientFactory: workerClientFactory,
	}
}

func (s *healthCheckServer) StartHeartbeat(heartbeatInterval, retryInterval time.Duration, numRetries int) {
	s.quit = make(chan struct{})
	go func() {
		for {
			select {
			case <-s.quit:
				return
			default:
				s.checkHealth(retryInterval, numRetries)
				time.Sleep(heartbeatInterval)
			}
		}
	}()
}

func (s *healthCheckServer) StopHeartbeat() {
	close(s.quit)
	log.Printf("heartbeat has been stopped")
}

func (s *healthCheckServer) checkHealth(retryInterval time.Duration, numRetries int) {
	allNodes, err := s.repository.GetAll()
	if err != nil {
		log.Printf("getting all nodes failed: %v", err)
	}
	for _, node := range allNodes {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()
		client, connection, err := s.workerClientFactory.GetWorkerClient(ctx, &node)
		defer connection.Close()
		if err != nil {
			log.Printf("could not initialize gRPC call to node: %v for reason: %v", node.ID, err)
		}
		request := &pb.HeartbeatRequest{
			NodeId: node.ID,
		}
		timesTried := 0
		for timesTried < numRetries {
			response, err := client.Heartbeat(ctx, request)
			if err == nil && response.Healthy {
				break
			}
			if err == nil {
				err = fmt.Errorf("health status of node is %t", response.Healthy)
			}
			log.Printf("heartbeat failed of node: %v for reason: %v, tried %v/%v times", node.ID, err, timesTried+1, numRetries)
			timesTried++
			time.Sleep(retryInterval)
		}
		if timesTried >= numRetries {
			log.Printf("node: %v is down, removing...", node.ID)
			s.repository.Remove(node.ID)
			s.mapper.Remove(node.BlockchainAddress)
			log.Printf("node: %v has been removed.", node.ID)
		}
	}
}
