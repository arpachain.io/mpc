package server

import (
	"context"
	"log"
	"sync"
	"time"

	pb "gitlab.com/arpachain/mpc/coordinator/api"
	"gitlab.com/arpachain/mpc/coordinator/pkg/node"
	grpcpool "github.com/processout/grpc-go-pool"
	"google.golang.org/grpc"
)

// WorkerClientFactory uses factory pattern to materialize (re-use) MpcWorkerClient and gRPC connetion
type WorkerClientFactory interface {
	GetWorkerClient(ctx context.Context, node *node.Node) (pb.MpcWorkerClient, *grpcpool.ClientConn, error)
	RemovePool(node *node.Node)
}

// Reference implementation:

type workerClientFactory struct {
	mu                *sync.Mutex // Protects the in-memory clientPoolMap when accessed concurrently
	clientPoolMap     map[string]*grpcpool.Pool
	initialNumClients int
	poolCapacity      int
	idleTimeout       time.Duration
}

// NewWorkerClientFactory returns an instance of server.WorkerClientFactory
func NewWorkerClientFactory() WorkerClientFactory {
	return &workerClientFactory{
		mu:                &sync.Mutex{},
		clientPoolMap:     make(map[string]*grpcpool.Pool),
		initialNumClients: 5,
		poolCapacity:      10,
		idleTimeout:       time.Minute,
	}
}

func (f *workerClientFactory) GetWorkerClient(ctx context.Context, node *node.Node) (pb.MpcWorkerClient, *grpcpool.ClientConn, error) {
	f.mu.Lock()
	defer f.mu.Unlock()
	var pool *grpcpool.Pool
	var err error
	if _, exists := f.clientPoolMap[node.ID]; exists {
		pool = f.clientPoolMap[node.ID]
	} else {
		// TODO(bomo): figure out the correct parameter
		pool, err = grpcpool.New(f.getConnection(node.Address), f.initialNumClients, f.poolCapacity, f.idleTimeout)
		if err != nil {
			return nil, nil, err
		}
		f.clientPoolMap[node.ID] = pool
	}
	connection, err := pool.Get(ctx)
	if err != nil {
		return nil, nil, err
	}
	return pb.NewMpcWorkerClient(connection.ClientConn), connection, nil
}

// this is a helper function for returning a func that the pool.New(...) uses
// when instantiating a pool
func (f *workerClientFactory) getConnection(nodeAddress string) func() (*grpc.ClientConn, error) {
	return func() (*grpc.ClientConn, error) {
		conn, err := grpc.Dial(nodeAddress, grpc.WithInsecure())
		if err != nil {
			log.Fatalf("failed to start gRPC connection: %v", err)
		}
		log.Printf("created gRPC connection to node at %s", nodeAddress)
		return conn, err
	}
}

func (f *workerClientFactory) RemovePool(node *node.Node) {
	f.mu.Lock()
	defer f.mu.Unlock()
	if pool, exists := f.clientPoolMap[node.ID]; exists {
		if !pool.IsClosed() {
			pool.Close()
		}
	}
	delete(f.clientPoolMap, node.ID)
}
