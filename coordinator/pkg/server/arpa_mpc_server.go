package server

import (
	"context"
	"errors"
	"fmt"
	"log"

	pb "gitlab.com/arpachain/mpc/coordinator/api"
	"gitlab.com/arpachain/mpc/coordinator/pkg/blockchain"
	"gitlab.com/arpachain/mpc/coordinator/pkg/certificate"
	"gitlab.com/arpachain/mpc/coordinator/pkg/group"
	"gitlab.com/arpachain/mpc/coordinator/pkg/mpc"
	"gitlab.com/arpachain/mpc/coordinator/pkg/node"
	"gitlab.com/arpachain/mpc/coordinator/pkg/player"
	"gitlab.com/arpachain/mpc/coordinator/pkg/program"
	"gitlab.com/arpachain/mpc/coordinator/pkg/sharing_data"
	"github.com/golang/protobuf/proto"
)

// arpaMPCServer is used to implement ArpaMpc
type arpaMPCServer struct {
	maxNumWorkers           uint32
	mapper                  blockchain.Mapper
	programRepository       program.Repository
	sharingDataRepository   sharing_data.Repository
	nodeManager             node.Manager
	groupManager            group.Manager
	mpcManager              mpc.Manager
	certificateGenerator    certificate.Generator
	mpcResultChannelMap     *map[string]chan MPCResult // mpcID -> result chan
	workerClientFactory     WorkerClientFactory
	blochchainAdapterClientFactory BlochchainAdapterClientFactory
}

// NewArpaMpcServer returns an instance of pb.ArpaMpcServer
func NewArpaMpcServer(
	maxNumWorkers uint32,
	programRepository program.Repository,
	sharingDataRepository sharing_data.Repository,
	mapper blockchain.Mapper,
	nodeManager node.Manager,
	groupManager group.Manager,
	mpcManager mpc.Manager,
	workerClientFactory WorkerClientFactory,
	certificateGenerator certificate.Generator,
	mpcResultChannelMap *map[string]chan MPCResult,
	blochchainAdapterClientFactory BlochchainAdapterClientFactory,
) pb.ArpaMpcServer {
	return &arpaMPCServer{
		maxNumWorkers:           maxNumWorkers,
		mapper:                  mapper,
		programRepository:       programRepository,
		sharingDataRepository:   sharingDataRepository,
		nodeManager:             nodeManager,
		groupManager:            groupManager,
		mpcManager:              mpcManager,
		workerClientFactory:     workerClientFactory,
		certificateGenerator:    certificateGenerator,
		mpcResultChannelMap:     mpcResultChannelMap,
		blochchainAdapterClientFactory: blochchainAdapterClientFactory,
	}
}

// RunMpc implements ArpaMpc.RunMpc
func (s *arpaMPCServer) RunMpc(
	ctx context.Context,
	in *pb.RunMpcRequest,
) (*pb.RunMpcResponse, error) {
	err := s.verifyNumWorkers(in.NumNodes)
	if err != nil {
		return nil, err
	}
	log.Printf("Length of MpcData: %v", len(in.MpcData))
	log.Printf("NumNodes: %v", int(in.NumNodes))
	// TODO(bomo): this check is for legacy compatibility,
	// in the future,
	// MpcData will only be fetched directly by worker from its datagateways
	if len(in.MpcData) != 0 && len(in.MpcData) != int(in.NumNodes) {
		return nil, fmt.Errorf("Invalid MpcData from request: %v", proto.MarshalTextString(in))
	}

	protoProgram, err := s.loadProgram(in.ProgramHash)
	if err != nil {
		return nil, err
	}

	group, err := s.groupManager.GetAvailableGroup(int(in.NumNodes))
	if err != nil {
		log.Printf("failed to get available group: %v", err)
		return nil, fmt.Errorf("failed to get available group: %v", err)
	}
	log.Printf("group id: %v", group.ID)

	jobErrors := make(chan error, in.NumNodes)
	mpcData := &pb.MpcData{}
	players, mpcID, err := s.initMPC(group, protoProgram, mpcData)
	if err != nil {
		log.Printf("Failed to generate players: %v", err)
		return nil, fmt.Errorf("Failed to generate players: %v", err)
	}
	resultChannel := make(chan MPCResult, in.NumNodes)
	(*s.mpcResultChannelMap)[mpcID] = resultChannel

	sharingData, err := s.getSharingData(int(in.NumNodes))
	if err != nil {
		log.Printf("Failed to get sharing data: %v", err)
		return nil, fmt.Errorf("Failed to get sharing data: %v", err)
	}

	for _, player := range players {
		var mpcData *pb.MpcData
		// TODO(bomo): this check is for legacy compatibility,
		// in the future,
		// MpcData will only be fetched directly by worker from its datagateways
		if len(in.MpcData) > 0 {
			mpcData = in.MpcData[player.Index]
		} else {
			mpcData = &pb.MpcData{}
		}
		player.MpcData = mpcData
		go s.startMPC(ctx, player, sharingData, jobErrors)
	}
	results, err := s.handleResult(ctx, mpcID, group.ID, int(in.NumNodes), jobErrors, resultChannel)
	if err != nil {
		return &pb.RunMpcResponse{}, err
	}

	// TODO(jiang): Use timeout to avoid waiting for a failed worker forever.
	// TODO(jiang): May need to modify the API to pass nodeIndex through and store result according to node index.
	// TODO(jiang): For a failed mpc instance, we also need to release its group.
	// This is tricky, as coordinator may need to explicitly kill the task in the involved nodes.
	// Alternatively, wait for the sent out requests to return and release all nodes in that group.
	return &pb.RunMpcResponse{
		MpcFinalResult: results,
	}, nil
}

// RunMpcForBlockchain implements ArpaMpc.RunMpcForBlockchain
func (s *arpaMPCServer) RunMpcForBlockchain(
	ctx context.Context,
	in *pb.RunMpcForBlockchainRequest,
) (*pb.RunMpcForBlockchainResponse, error) {
	err := s.verifyNumWorkers(in.GetNumTotalWorkers())
	if err != nil {
		return nil, err
	}
	log.Printf("NumNodes: %v", int(in.GetNumTotalWorkers()))

	protoProgram, err := s.loadProgram(in.ProgramHash)
	if err != nil {
		return nil, err
	}
	var delegatedNodeID string
	dataNodeIDs := make([]string, 0)

	numTotalWorkers := int(in.GetNumTotalWorkers())
	numAdditionalWorkers := numTotalWorkers - len(in.GetDataProviderWorkerBlockchainAddresses())
	if in.GetDelegatedWorkerBlockchainAddress() != "" {
		delegatedNodeID, err = s.mapper.LookUp(in.GetDelegatedWorkerBlockchainAddress())
		if err != nil {
			return nil, fmt.Errorf("failed to look up the id for eth address [%v]: %v",
				in.GetDelegatedWorkerBlockchainAddress(),
				err)
		}
		numAdditionalWorkers--
	}
	for _, address := range in.GetDataProviderWorkerBlockchainAddresses() {
		nodeID, err := s.mapper.LookUp(address)
		if err != nil {
			return nil, fmt.Errorf("failed to look up the id for eth address [%v]: %v",
				in.GetDelegatedWorkerBlockchainAddress(),
				err)
		}
		dataNodeIDs = append(dataNodeIDs, nodeID)
	}
	group, additionalWorkerIDs, err := s.groupManager.GetAvailableGroupForBlockchain(
		dataNodeIDs,
		delegatedNodeID,
		numAdditionalWorkers,
	)
	if err != nil {
		log.Printf("failed to get available group: %v", err)
		return nil, fmt.Errorf("failed to get available group: %v", err)
	}

	jobErrors := make(chan error, numTotalWorkers)

	// TODO(bomo): dummy mpcData for compatibility, should be removed in the future
	mpcData := &pb.MpcData{}
	players, mpcID, err := s.initMPC(group, protoProgram, mpcData)
	if err != nil {
		log.Printf("Failed to generate players: %v", err)
		return nil, fmt.Errorf("Failed to generate players: %v", err)
	}
	resultChannel := make(chan MPCResult, numTotalWorkers)
	(*s.mpcResultChannelMap)[mpcID] = resultChannel
	if err != nil {
		log.Printf("Failed to connect to eth adapter client: %v", err)
		return nil, fmt.Errorf("Failed to connect to eth adapter client: %v", err)
	}
	sharingData, err := s.getSharingData(numTotalWorkers)
	if err != nil {
		log.Printf("Failed to get sharing data: %v", err)
		return nil, fmt.Errorf("Failed to get sharing data: %v", err)
	}
	for _, player := range players {
		go s.startMPC(nil /*async needs its own context*/, player, sharingData, jobErrors)
	}
	go s.handleResult(nil /*async needs its own context*/, mpcID, group.ID, numTotalWorkers, jobErrors, resultChannel, s.processResponseForBlockchain)
	additionalWorkers, err := s.nodeManager.GetByIDs(additionalWorkerIDs)
	if err != nil {
		log.Printf("Failed to fetch workers: %v", err)
		return nil, fmt.Errorf("Failed to fetch workers: %v", err)
	}
	additionalWorkerBlockchainAddresses := make([]string, 0)
	for _, worker := range additionalWorkers {
		additionalWorkerBlockchainAddresses = append(additionalWorkerBlockchainAddresses, worker.BlockchainAddress)
	}
	return &pb.RunMpcForBlockchainResponse{
		AdditionalWorkerBlockchainAddresses: additionalWorkerBlockchainAddresses,
		MpcId:                        mpcID,
	}, nil
}

func (s *arpaMPCServer) generatePlayersInfo(nodes []*node.Node) []*pb.PlayerInfo {
	players := make([]*pb.PlayerInfo, len(nodes))
	for i, node := range nodes {
		players[i] = &pb.PlayerInfo{
			NodeId:  node.ID,
			Cert:    node.Certificate,
			Address: node.Address,
		}
	}
	return players
}

func (s *arpaMPCServer) startMPC(
	ctx context.Context,
	player *player.Player,
	sharingData string,
	jobErrors chan error,
) {
	if ctx == nil {
		ctx = context.Background()
	}
	client, connection, err := s.workerClientFactory.GetWorkerClient(ctx, player.Node)
	if err != nil {
		log.Printf("did not connect to node[%v]: %v", player.Node.Address, err)
		jobErrors <- err
		return
	}
	defer connection.Close()
	networkData := &pb.NetworkData{
		PlayerIndex: uint32(player.Index),
		Players:     player.PlayersInfo,
		RootCert:    player.RootCert,
	}

	request := &pb.StartMpcRequest{
		MpcId:       player.MpcID,
		Program:     player.Program,
		NetData:     networkData,
		SharingData: sharingData,
		MpcData:     player.MpcData,
	}
	log.Printf("Starting mpc for node[%v]", player.Node.Address)
	response, err := client.StartMpc(ctx, request)
	if err != nil {
		log.Printf("Error: could not start mpc: %v", err)
	} else {
		log.Printf("MPC successfully started: %t", response.Success)
	}
	jobErrors <- err
}

func (s *arpaMPCServer) loadProgram(programHash string) (*pb.Program, error) {
	program, err := s.programRepository.GetByID(programHash)
	if err != nil {
		log.Printf("failed to get program bytecode: %v for hash: %v", err, programHash)
		return nil, fmt.Errorf("failed to get program bytecode: %v for hash: %v", err, programHash)
	}
	protoProgram := &pb.Program{
		ProgMap:  program.ProgramMap,
		Schedule: program.Schedule,
		ProgramHash: program.ID,
	}
	return protoProgram, nil
}

func (s *arpaMPCServer) handleResult(
	ctx context.Context,
	mpcID string,
	groupID string,
	numTotalWorkers int,
	jobErrors chan error,
	resultChannel chan MPCResult,
	processResponse ...func(ctx context.Context, mpcID string, mpcResults []*pb.MpcResult) error,
) ([]*pb.MpcResult, error) {
	if ctx == nil {
		ctx = context.Background()
	}
	sentRPCRequests := 0
	var lastError error
	lastError = nil
	for i := 0; i < numTotalWorkers; i++ {
		jobError := <-jobErrors
		if jobError == nil {
			sentRPCRequests++
		} else {
			lastError = jobError
		}
	}
	log.Printf("Num received success rpc requests: %v", sentRPCRequests)
	if sentRPCRequests != numTotalWorkers {
		// Not all StartMpc requests succeed, so ignore all the sent out requests.
		return nil, lastError
	}
	results := make([]*pb.MpcResult, sentRPCRequests)
	for i := 0; i < sentRPCRequests; i++ {
		mpcResult := <-resultChannel
		if mpcResult.Err != nil {
			delete(*s.mpcResultChannelMap, mpcID)
			return nil, mpcResult.Err
		}
		log.Printf("Received MPC result from channel for %v", mpcResult.MpcID)
		results[i] = mpcResult.MpcResult
	}
	if len(processResponse) <= 1 {
		for _, fn := range processResponse {
			err := fn(ctx, mpcID, results)
			if err != nil {
				return nil, err
			}
		}
	} else {
		log.Printf("Mpc response can only be processed once")
	}
	err := s.groupManager.Ungroup([]string{groupID})
	if err != nil {
		log.Printf("ungrouping failed: %v for id: %v", err, groupID)
	}
	return results, err
}

func (s *arpaMPCServer) processResponseForBlockchain(ctx context.Context, mpcID string, mpcResults []*pb.MpcResult) error {
	blochchainAdapterClient, connection, err := s.blochchainAdapterClientFactory.GetBlochchainAdapterClient(ctx)
	if err != nil {
		return err
	}
	defer connection.Close()
	reportMpcResultRequest := &pb.ReportMpcResultRequest{
		MpcId:  mpcID,
		Result: mpcResults,
	}
	response, err := blochchainAdapterClient.ReportMpcResult(ctx, reportMpcResultRequest)
	if err != nil {
		return err
	}
	if !response.Success {
		return errors.New("finish mpc for eth has failed")
	}
	return nil
}

func (s *arpaMPCServer) initMPC(
	group *group.Group,
	program *pb.Program,
	mpcData *pb.MpcData,
) ([]*player.Player, string /*MpcID*/, error) {
	mpcID, err := s.mpcManager.Create(group.NodeIDs)
	if err != nil {
		log.Printf("error when creating mpc %v: %v", mpcID, err)
		return nil, "", fmt.Errorf("error when creating mpc %v: %v", mpcID, err)
	}
	log.Printf("Starting mpc: %v", mpcID)
	nodes, err := s.nodeManager.GetByIDs(group.NodeIDs)
	if err != nil {
		log.Printf("error when getting nodes: %v", err)
		return nil, "", fmt.Errorf("error when getting nodes: %v", err)
	}
	rootCert, err := s.certificateGenerator.GetCACertificate("")
	if err != nil {
		log.Printf("Failed to get CA root certificate: %v", err)
		return nil, "", fmt.Errorf("Failed to get CA root certificate: %v", err)
	}
	playersInfo := s.generatePlayersInfo(nodes)
	players := make([]*player.Player, 0)
	for playerIndex, node := range nodes {
		player := &player.Player{
			Index:       playerIndex,
			MpcID:       mpcID,
			Node:        node,
			PlayersInfo: playersInfo,
			RootCert:    rootCert,
			Program:     program,
			MpcData:     mpcData,
		}
		players = append(players, player)
	}
	return players, mpcID, nil
}

func (s *arpaMPCServer) verifyNumWorkers(requestedNum uint32) error {
	if requestedNum < 2 {
		return fmt.Errorf("number of workers requested has to be more than 2, got: %v", requestedNum)
	}
	if requestedNum > s.maxNumWorkers {
		return fmt.Errorf("number of workers requested exceeds the maximum allowed (%v), got: %v", s.maxNumWorkers, requestedNum)
	}
	return nil
}

func (s *arpaMPCServer) getSharingData(numParties int) (string, error) {
	sharingData, err := s.sharingDataRepository.GetByNumMPC(numParties)
	if err != nil {
		return "", err
	}
	return sharingData.Data, nil
}
