package mpc_test

import (
	"testing"

	"gitlab.com/arpachain/mpc/coordinator/pkg/mpc"
	"github.com/stretchr/testify/assert"
)

func TestIsFinished(t *testing.T) {
	assert := assert.New(t)
	t.Log("case 1: testing IsFinished...")
	// setting up 2 nodes, both unfinishied
	finishFlagMap := make(map[string]bool)
	finishFlagMap["node_id_1"] = false
	finishFlagMap["node_id_2"] = false
	mpc := &mpc.MPC{
		ID:            "fake_id",
		FinishFlagMap: finishFlagMap,
	}
	assert.False(mpc.IsFinished(), "should not be finished when all nodes are unfinished")
	mpc.FinishFlagMap["node_id_1"] = true
	assert.False(mpc.IsFinished(), "should not be finished when there is at least one unfinished node")
	mpc.FinishFlagMap["node_id_2"] = true
	assert.True(mpc.IsFinished(), "should be finished when all nodes are finished")
}
