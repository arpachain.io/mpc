package main

import (
	"flag"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"time"

	"gitlab.com/arpachain/mpc/coordinator/pkg/node"
	"gitlab.com/arpachain/mpc/coordinator/pkg/worker_status_updater"

	"golang.org/x/net/websocket"
)

var (
	address      = flag.String("address", "127.0.0.1:60000", "The ip address and port number of the worker status updater")
	wsPrivateKey = flag.String(
		"wskey",
		os.Getenv("HOME")+"/privkey.pem",
		"Full path to private key for websocket server for updating worker status.",
	)
	wsCertificate = flag.String(
		"wscert",
		os.Getenv("HOME")+"/fullchain.pem",
		"Full path to certificate for websocket server for updating worker status.",
	)
)

func main() {
	flag.Parse()
	rand.Seed(time.Now().UnixNano())
	repository := node.NewRepository()
	testNodes := generateTestNodes(10)
	for _, node := range testNodes {
		repository.Create(node)
	}
	updater := worker_status_updater.NewUpdater(repository)
	go func() {
		for {
			time.Sleep(time.Second * 2)
			randomUpdateStatus(repository)
			updater.SendUpdate()
		}
	}()

	http.HandleFunc("/test", HelloServer)
	http.Handle("/", websocket.Handler(updater.Register))
	log.Printf("Server listening on : %v", *address)
	if err := http.ListenAndServeTLS(*address, *wsCertificate, *wsPrivateKey, nil); err != nil {
		log.Fatal("ListenAndServe:", err)
	}
}

func HelloServer(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte("This is an example server.\n"))
}

func generateTestNodes(numNodes int) []*node.Node {
	testNodes := make([]*node.Node, 0)
	for i := 0; i < numNodes; i++ {
		testNode := &node.Node{
			ID:          fmt.Sprintf("%v", i),
			Name:        "Test_Node_" + fmt.Sprintf("%v", i),
			Address:     "127.0.0.1:600" + fmt.Sprintf("%v", i),
			IsGrouped:   rand.Float32() < 0.5,
			BlockchainAddress:  "fake_blockchain_address_" + fmt.Sprintf("%v", i),
			Certificate: []byte("fake_certificate"),
		}
		testNodes = append(testNodes, testNode)
	}
	return testNodes
}

func randomUpdateStatus(repository node.Repository) error {
	allNodes, err := repository.GetAll()
	if err != nil {
		return err
	}
	for _, node := range allNodes {
		if node.IsGrouped {
			if rand.Float32() < 0.5 {
				repository.UngroupNode(node.ID)
			}
		} else {
			if rand.Float32() < 0.5 {
				repository.GroupNode(node.ID)
			}
		}
	}
	return nil
}
