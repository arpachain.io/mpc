package worker_status_updater

import (
	"encoding/json"
	"log"

	"gitlab.com/arpachain/mpc/coordinator/pkg/node"
	"golang.org/x/net/websocket"
)

// Updater sends worker status updates (busy/free) to the front-end
type Updater interface {
	SendUpdate()
	Register(ws *websocket.Conn)
	Close(connectionString string)
}

// Reference implementation:

type updater struct {
	repository    node.Repository
	connectionMap map[string]*websocket.Conn
	quitSignalMap map[string]chan struct{}
}

// NewUpdater returns an instance of worker_status_updater
func NewUpdater(repository node.Repository) Updater {
	return &updater{
		repository:    repository,
		connectionMap: make(map[string]*websocket.Conn),
		quitSignalMap: make(map[string]chan struct{}),
	}
}

func (u *updater) SendUpdate() {
	log.Printf("sending worker status update to %v front-end clients", len(u.connectionMap))
	for _, connection := range u.connectionMap {
		statusMap := make(map[string]bool)
		allNodes, err := u.repository.GetAll()
		if err != nil {
			log.Print(err)
			return
		}
		for _, node := range allNodes {
			statusMap[node.ID] = node.IsGrouped
		}
		statusJSON, err := json.Marshal(statusMap)
		if err != nil {
			log.Printf("marshaling worker statuses failed: %v", err)
			return
		}
		err = websocket.Message.Send(connection, string(statusJSON))
		// TODO(bomo): add retry mechanism
		if err != nil {
			log.Printf("sending worker statuses update failed: %v", err)
			connectionString := connection.RemoteAddr().String()
			connection.Close()
			delete(u.connectionMap, connectionString)
			delete(u.quitSignalMap, connectionString)
			log.Printf("connection: %v has been closed and removed", connectionString)
		}
	}
}

func (u *updater) Close(connectionString string) {
	close(u.quitSignalMap[connectionString])
	delete(u.connectionMap, connectionString)
	delete(u.quitSignalMap, connectionString)
}

func (u *updater) Register(ws *websocket.Conn) {
	connectionString := ws.RemoteAddr().String()
	u.connectionMap[connectionString] = ws
	u.quitSignalMap[connectionString] = make(chan struct{})
	log.Printf("ws client: %v is registered", connectionString)
	// below is a hack to keep ws connection open
	// since native golang websocket library doesn't support keep alive
	for {
		select {
		case <-u.quitSignalMap[connectionString]:
			u.connectionMap[connectionString].Close()
			return
		}
	}
}
