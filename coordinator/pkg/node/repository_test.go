package node_test

import (
	"testing"

	"gitlab.com/arpachain/mpc/coordinator/pkg/node"
	tu "gitlab.com/arpachain/mpc/coordinator/test"
	"github.com/stretchr/testify/assert"
)

const (
	numNodes = 5
)

func TestCreateAndGetByID(t *testing.T) {
	assert := assert.New(t)
	repository := node.NewRepository()
	nodeToCreate := tu.GenerateTestNode()

	t.Log("case 1: testing Create...")
	id, err := repository.Create(nodeToCreate)
	assert.NoError(err)
	assert.NotEmpty(id)

	t.Log("case 2: testing GetByID...")
	createdNode, err := repository.GetByID(id)
	assert.NoError(err)
	assert.NotEmpty(createdNode)
	assert.Equal(createdNode.ID, id)
	assert.Equal(nodeToCreate.Name, createdNode.Name)
	assert.Equal(nodeToCreate.Address, createdNode.Address)
	assert.Equal(nodeToCreate.IsGrouped, createdNode.IsGrouped)
	assert.Equal(nodeToCreate.Certificate, createdNode.Certificate)

	t.Log("case 3: testing GetByID with an invalid id...")
	createdNode, err = repository.GetByID("invalid_id")
	assert.Error(err, "should fail for invalid id")
	assert.Empty(createdNode, "should return an empty node")
}

func TestGroupNodeAndUngroupNode(t *testing.T) {
	assert := assert.New(t)
	repository := node.NewRepository()

	t.Log("creating a testing node... assuming creation and retrieval always work")
	id, _ := repository.Create(tu.GenerateTestNode())
	node, _ := repository.GetByID(id)
	assert.False(node.IsGrouped, "node should be ungrouped initially")

	t.Log("case 1: testing GroupNode...")
	err := repository.GroupNode(id)
	assert.NoError(err, "there should be no error when grouping the node")
	node, _ = repository.GetByID(id)
	assert.True(node.IsGrouped, "node should be grouped now")

	t.Log("case 2: testing UngroupNode...")
	err = repository.UngroupNode(id)
	assert.NoError(err, "there should be no error when ungrouping the node")
	node, _ = repository.GetByID(id)
	assert.False(node.IsGrouped, "node should be ungrouped now")

	t.Log("case 3: testing Group on an already grouped node...")
	t.Log("first, group the node")
	err = repository.GroupNode(id)
	assert.NoError(err, "ungrouped node should be grouped with no error")
	t.Log("then, trying to group the already grouped node")
	err = repository.GroupNode(id)
	assert.Error(err, "should fail since you cannot group an already grouped node")

	t.Log("case 4: testing Ungroup on an already ungrouped node...")
	t.Log("first, ungroup the node")
	err = repository.UngroupNode(id)
	assert.NoError(err, "grouped node should be ungrouped with no error")
	t.Log("then, trying to ungroup the already ungrouped node")
	err = repository.UngroupNode(id)
	assert.Error(err, "should fail since you cannot ungroup an already ungrouped node")

	t.Log("case 5: testing Group with an invalid id...")
	err = repository.GroupNode("invalid_id")
	assert.Error(err, "should fail for invalid id")

	t.Log("case 6: testing Ungroup with an invalid id...")
	err = repository.UngroupNode("invalid_id")
	assert.Error(err, "should fail for invalid id")
}

func TestGetAll(t *testing.T) {
	assert := assert.New(t)
	repository := node.NewRepository()

	t.Log("case 1: testing GetAll when there is no node...")
	nodes, err := repository.GetAll()
	assert.Empty(nodes, "should return nil")

	t.Log("case 2: testing GetAll when there are n nodes...")
	t.Log("creating n nodes to the empty repository...")
	tu.CreateNodes(numNodes, repository)
	nodes, err = repository.GetAll()
	assert.NoError(err, "retrieval should be successful")
	assert.Equal(len(nodes), numNodes, "there should be the same number of nodes retrieved")
	t.Log("making sure all fields are the same compared to the sample node")
	node := nodes[0]
	sampleNode := tu.GenerateTestNode()
	assert.NotEmpty(node.ID)
	assert.Equal(node.Name, sampleNode.Name)
	assert.Equal(node.Address, sampleNode.Address)
	assert.Equal(node.Certificate, sampleNode.Certificate)
	assert.Equal(node.IsGrouped, sampleNode.IsGrouped)
}

func TestRemove(t *testing.T) {
	assert := assert.New(t)
	repository := node.NewRepository()
	id, _ := repository.Create(tu.GenerateTestNode())

	t.Log("case 1: testing Remove...")
	node, _ := repository.GetByID(id)
	assert.NotEmpty(node, "making sure the node is there")
	repository.Remove(id)
	node, _ = repository.GetByID(id)
	assert.Empty(node, "the node should be removed thus returned empty node object")
}
