package node

import (
	"gitlab.com/arpachain/mpc/coordinator/pkg/blockchain"
	"gitlab.com/arpachain/mpc/coordinator/pkg/certificate"
)

// Manager for node provides necessary functions for mpc server to interact with nodes
type Manager interface {
	GetByIDs(ids []string) ([]*Node, error)
	Register(address string, name string, csr []byte, BlockchainAddress string) (string, []byte, error)
}

// Reference implementation

type manager struct {
	repository Repository
	mapper     blockchain.Mapper
	generator  certificate.Generator
}

// NewManager returns an instance of node.Manager
func NewManager(
	repository Repository,
	mapper blockchain.Mapper,
	generator certificate.Generator,
) Manager {
	return &manager{
		repository: repository,
		mapper:     mapper,
		generator:  generator,
	}
}

func (m *manager) GetByIDs(ids []string) ([]*Node, error) {
	nodesToReturn := make([]*Node, len(ids))
	for i, nodeID := range ids {
		node, err := m.repository.GetByID(nodeID)
		if err != nil {
			return nil, err
		}
		nodesToReturn[i] = &node
	}
	return nodesToReturn, nil
}

func (m *manager) Register(
	address string,
	name string,
	csr []byte,
	blockchainAddress string,
) (string, []byte, error) {
	certificate, err := m.generator.GenerateCertificate(csr, "", "")
	if err != nil {
		return "", nil, err
	}
	node := &Node{
		Name:              name,
		Address:           address,
		IsGrouped:         false,
		Certificate:       certificate,
		BlockchainAddress: blockchainAddress,
	}
	id, err := m.repository.Create(node)
	if err != nil {
		return "", nil, err
	}
	if len(blockchainAddress) > 0 {
		err = m.mapper.Add(blockchainAddress, id)
		if err != nil {
			m.repository.Remove(id)
			return "", nil, err
		}
	}
	return id, certificate, nil
}
