package group

import (
	"errors"
	"sync"

	"gitlab.com/arpachain/mpc/coordinator/pkg/util"
)

type sequentialRepository struct {
	mu                 *sync.Mutex
	groupMap           map[string]Group
	sequentialGroupIDs []string /*to keep track of the sequence of group creation*/
}

// NewSequentialRepository returns an instance of group.Repository
func NewSequentialRepository() Repository {
	return &sequentialRepository{
		mu:                 &sync.Mutex{},
		groupMap:           map[string]Group{},
		sequentialGroupIDs: make([]string, 0),
	}
}

func (r *sequentialRepository) GetAll() ([]Group, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	groups := make([]Group, 0)
	for _, groupID := range r.sequentialGroupIDs {
		group := r.groupMap[groupID]
		groups = append(groups, group)
	}
	return groups, nil
}

// GetByID returns empty group and throws error when id does not exist
func (r *sequentialRepository) GetByID(id string) (Group, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	if group, exists := r.groupMap[id]; exists {
		return group, nil
	}
	return Group{}, errors.New("group does not exist")
}

func (r *sequentialRepository) Create(group *Group) (string, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	id, err := util.GenerateID()
	if err != nil {
		return "", err
	}
	if _, exists := r.groupMap[id]; exists {
		return "", errors.New("group with same id already exists")
	}
	r.groupMap[id] = Group{
		ID:      id,
		NodeIDs: group.NodeIDs,
		IsBusy:  false,
	}
	r.sequentialGroupIDs = append(r.sequentialGroupIDs, id)
	return id, nil
}

func (r *sequentialRepository) UpdateIsBusy(id string, isBusy bool) error {
	r.mu.Lock()
	defer r.mu.Unlock()
	group, exists := r.groupMap[id]
	if !exists {
		return errors.New("group does not exist")
	}
	group.IsBusy = isBusy
	r.groupMap[id] = group
	return nil
}

func (r *sequentialRepository) UpdateNodeIDs(id string, nodeIDs []string) error {
	r.mu.Lock()
	defer r.mu.Unlock()
	group, exists := r.groupMap[id]
	if !exists {
		return errors.New("group does not exist")
	}
	if group.IsBusy {
		return errors.New("cannot update a busy group")
	}
	group.NodeIDs = nodeIDs
	r.groupMap[id] = group
	return nil
}

func (r *sequentialRepository) Remove(id string) {
	r.mu.Lock()
	defer r.mu.Unlock()
	delete(r.groupMap, id)
}

// TODO(bomo): free groups should be released
func (r *sequentialRepository) GetAvailableGroup(size int) (Group, error) {
	allGroups, err := r.GetAll()
	if err != nil {
		return Group{}, err
	}
	for _, group := range allGroups {
		if group.GetSize() == size && !group.IsBusy {
			return group, nil
		}
	}
	return Group{}, errors.New("no available group")
}
