package group_test

import (
	"testing"

	"gitlab.com/arpachain/mpc/coordinator/pkg/group"
	tu "gitlab.com/arpachain/mpc/coordinator/test"
	"github.com/stretchr/testify/assert"
)

const (
	numGroups = 5
	numNodes  = 5
)

// TODO(bomo): tests should be broken out into different methods
func TestCreateAndGetByID(t *testing.T) {
	assert := assert.New(t)
	repository := group.NewRepository()
	groupToCreate := tu.GenerateTestGroup()

	t.Log("case 1: testing Create...")
	id, err := repository.Create(groupToCreate)
	assert.NoError(err)
	assert.NotEmpty(id)

	t.Log("case 2: testing GetByID...")
	createdGroup, err := repository.GetByID(id)
	assert.NoError(err)
	assert.NotEmpty(createdGroup)
	assert.Equal(createdGroup.ID, id)
	assert.Equal(groupToCreate.IsBusy, createdGroup.IsBusy)
	assert.Equal(groupToCreate.NodeIDs, createdGroup.NodeIDs)

	t.Log("case 3: testing GetByID with an invalid id...")
	createdGroup, err = repository.GetByID("invalid_id")
	assert.Error(err, "should fail for invalid id")
	assert.Empty(createdGroup, "should return an empty group")
}

func TestRemove(t *testing.T) {
	assert := assert.New(t)
	repository := group.NewRepository()
	id, _ := repository.Create(tu.GenerateTestGroup())

	t.Log("case 1: testing Remove...")
	group, _ := repository.GetByID(id)
	assert.NotEmpty(group, "making sure the group is there")
	repository.Remove(id)
	group, err := repository.GetByID(id)
	assert.Error(err)
	assert.Empty(group, "the group has been removed, thus empty group should be returned")
}

func TestUpdateIsBusy(t *testing.T) {
	assert := assert.New(t)
	repository := group.NewRepository()
	id, _ := repository.Create(tu.GenerateTestGroup())

	t.Log("case 1: testing UpdateIsBusy...")
	group, _ := repository.GetByID(id)
	assert.False(group.IsBusy, "the group should be free initially")
	repository.UpdateIsBusy(id, true)
	group, _ = repository.GetByID(id)
	assert.True(group.IsBusy, "the group should be busy now")
	repository.UpdateIsBusy(id, false)
	group, _ = repository.GetByID(id)
	assert.False(group.IsBusy, "the group should be free new")

	t.Log("case 2: testing UpdateIsBusy with an invalid id...")
	err := repository.UpdateIsBusy("fake_id", true)
	assert.Error(err)
}

func TestUpdateNodeIDs(t *testing.T) {
	assert := assert.New(t)
	repository := group.NewRepository()
	id, _ := repository.Create(tu.GenerateTestGroup())

	t.Log("case 1: testing UpdateNodeIDs...")
	group, _ := repository.GetByID(id)
	oldNodeIDs := group.NodeIDs
	newNodeIDs := tu.GenerateNodeIDs(len(oldNodeIDs))
	assert.NotEqual(oldNodeIDs, newNodeIDs)
	err := repository.UpdateNodeIDs(id, newNodeIDs)
	assert.NoError(err)
	group, _ = repository.GetByID(id)
	assert.Equal(group.NodeIDs, newNodeIDs, "node ids should be updated now")

	t.Log("case 2: testing UpdateNodeIDs on a busy group...")
	// manually mark the group as busy and try to update the node ids
	repository.UpdateIsBusy(id, true)
	err = repository.UpdateNodeIDs(id, newNodeIDs)
	assert.Error(err)

	t.Log("case 3: testing UpdateNodeIDs with an invalid id...")
	err = repository.UpdateNodeIDs("fake_id", newNodeIDs)
	assert.Error(err)
}

func TestGetAll(t *testing.T) {
	assert := assert.New(t)
	repository := group.NewRepository()

	t.Log("case 1: testing GetAll when there is no group...")
	groups, err := repository.GetAll()
	assert.Empty(groups, "should return empty group")

	t.Log("case 2: testing GetAll when there are n groups...")
	t.Log("creating n groups to the empty repository...")
	tu.CreateGroups(numGroups, repository)
	groups, err = repository.GetAll()
	assert.NoError(err, "retrieval should be successful")
	assert.Equal(len(groups), numGroups, "there should be the same number of groups retrieved")
	t.Log("making sure all fields are the same compared to the sample node")
	group := groups[0]
	sampleGroup := tu.GenerateTestGroup()
	assert.NotEmpty(group.ID)
	assert.Equal(group.IsBusy, sampleGroup.IsBusy)
	assert.Equal(len(group.NodeIDs), len(sampleGroup.NodeIDs))
}

func TestGetAvailableGroupInRepository(t *testing.T) {
	assert := assert.New(t)
	repository := group.NewRepository()

	group := tu.GenerateTestGroup()
	id, _ := repository.Create(group)

	t.Log("case 1: testing GetAvailableGroup when there is an available group with the size required...")
	availableGroup, err := repository.GetAvailableGroup(len(group.NodeIDs))
	assert.NoError(err)
	assert.NotEmpty(availableGroup, "the available group should not be empty")
	assert.Equal(availableGroup.ID, id, "group with the same id should be returned")
	assert.False(availableGroup.IsBusy, "the available group should be free")

	t.Log("case 2: testing GetAvailableGroup when there is available group with a different size...")
	noGroup, err := repository.GetAvailableGroup(len(group.NodeIDs) + 1)
	assert.Error(err)
	assert.Empty(noGroup, "group returned should be empty")

	t.Log("case 3: testing GetAvailableGroup when there is a busy group with the same size...")
	repository.UpdateIsBusy(id, true)
	noGroup, err = repository.GetAvailableGroup(len(group.NodeIDs))
	assert.Error(err)
	assert.Empty(noGroup, "group returned should be empty")

	t.Log("case 4: testing GetAvailableGroup when there is a busy group with a different size...")
	noGroup, err = repository.GetAvailableGroup(len(group.NodeIDs) + 1)
	assert.Error(err)
	assert.Empty(noGroup, "group returned should be empty")
}
