package certificate_test

import (
	"io/ioutil"
	"os"
	"testing"

	"gitlab.com/arpachain/mpc/coordinator/pkg/certificate"
	"github.com/stretchr/testify/assert"
)

func TestGenerateCertificate(t *testing.T) {
	assert := assert.New(t)
	goPath := os.Getenv("GOPATH")
	csr, err := ioutil.ReadFile(goPath + "/src/gitlab.com/arpachain/mpc/coordinator/pkg/certificate/test_certificates/test_node_1.csr")
	assert.NoError(err)
	generator := certificate.NewGenerator()
	clientCertificate, err := generator.GenerateCertificate(csr, "", "")
	assert.NoError(err)
	t.Logf("client certificate generated: %v", clientCertificate)
}

func TestGetCACertificate(t *testing.T) {
	assert := assert.New(t)
	goPath := os.Getenv("GOPATH")
	generator := certificate.NewGenerator()

	t.Log("case 1: testing GetCACertificate when path is provided...")
	path := goPath + "/src/gitlab.com/arpachain/mpc/coordinator/pkg/certificate/test_certificates/ca.crt"
	crt, err := generator.GetCACertificate(path)
	assert.NoError(err)
	assert.NotEmpty(crt)

	t.Log("case 2: testing GetCACertificate when path is not provided...")
	crt, err = generator.GetCACertificate("")
	// default path should be used and certificates should be successfully retrieved
	assert.NoError(err)
	assert.NotEmpty(crt)
}
