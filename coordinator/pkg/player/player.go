package player

import (
	pb "gitlab.com/arpachain/mpc/coordinator/api"
	"gitlab.com/arpachain/mpc/coordinator/pkg/node"
)

type Player struct {
	Index       int
	MpcID       string
	Node        *node.Node
	PlayersInfo []*pb.PlayerInfo
	RootCert    []byte
	Program     *pb.Program
	MpcData     *pb.MpcData
}
