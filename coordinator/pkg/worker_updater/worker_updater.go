package worker_updater

import (
	"context"
	"errors"
	"log"
	"time"

	pb "gitlab.com/arpachain/mpc/coordinator/api"
	"gitlab.com/arpachain/mpc/coordinator/pkg/node"
	"gitlab.com/arpachain/mpc/coordinator/pkg/server"
)

// Updater sends the list of nodes to eth adapter
type Updater interface {
	Start(ctx context.Context, interval time.Duration)
	Stop()
}

// Reference implementation:

type updater struct {
	repository node.Repository
	factory    server.BlochchainAdapterClientFactory
	quit       chan struct{}
}

// NewUpdater returns an instance of node.Updater
func NewUpdater(repository node.Repository, factory server.BlochchainAdapterClientFactory) Updater {
	return &updater{
		repository: repository,
		factory:    factory,
		quit:       make(chan struct{}),
	}
}

func (u *updater) updateWorkerList(ctx context.Context) error {
	nodes, err := u.repository.GetAll()
	if err != nil {
		return err
	}
	workerETHAddresses := make([]string, 0)
	for _, node := range nodes {
		workerETHAddresses = append(workerETHAddresses, node.BlockchainAddress)
	}
	request := &pb.UpdateWorkerListRequest{
		WorkerAddresses: workerETHAddresses,
	}
	client, connection, err := u.factory.GetBlochchainAdapterClient(ctx)
	if err != nil {
		return err
	}
	defer connection.Close()
	response, err := client.UpdateWorkerList(ctx, request)
	if err != nil {
		return err
	}
	if !response.Success {
		log.Printf("update worker list failed")
		return errors.New("update worker list failed")
	}
	log.Printf("update worker list succeeded")
	return nil
}

func (u *updater) Start(ctx context.Context, interval time.Duration) {
	ticker := time.NewTicker(interval)
	go func() {
		for {
			select {
			case <-ticker.C:
				err := u.updateWorkerList(ctx)
				if err != nil {
					log.Printf("update worker list to eth adapter failed: %v\n", err)
				}
			case <-ctx.Done():
				ticker.Stop()
				return
			case <-u.quit:
				ticker.Stop()
				return
			}
		}
	}()
}

func (u *updater) Stop() {
	close(u.quit)
}
