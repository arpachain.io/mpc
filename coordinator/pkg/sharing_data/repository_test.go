package sharing_data_test

import (
	"os"
	"testing"

	"gitlab.com/arpachain/mpc/coordinator/pkg/sharing_data"
	"github.com/stretchr/testify/assert"
)

const (
	numParties = 10
)

var (
	goPath   = os.Getenv("GOPATH")
	dataPath = goPath + "/src/gitlab.com/arpachain/mpc/coordinator/data/sharing_data"
)

func TestGetAll(t *testing.T) {
	assert := assert.New(t)
	repository := sharing_data.NewRepository(dataPath)
	allSharingData, err := repository.GetAll()
	assert.NoError(err)
	assert.NotEmpty(allSharingData)
	// The sharing_data directory stores the sharing data for num of parties from 2 to numParties.
	assert.Equal(numParties-1, len(allSharingData))
}
