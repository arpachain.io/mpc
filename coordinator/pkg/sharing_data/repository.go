package sharing_data

import (
	"errors"
	"io/ioutil"
	"log"
	"strconv"
	"sync"
)

// Repository for node provides CRUD operations to persist sharing data by number of workers
type Repository interface {
	GetAll() ([]SharingData, error)
	GetByNumMPC(numMPC int) (SharingData, error)
	Create(sharingData *SharingData) error
	Remove(numMPC int)
}

// Reference implementation:

type repository struct {
	mu       *sync.Mutex
	storeMap map[int]SharingData /*key: Number of MPC Parties*/
}

// NewRepository returns an instance of sharing_data.Repository
func NewRepository(sharingDataPath string) Repository {
	repository := &repository{
		mu:       &sync.Mutex{},
		storeMap: map[int]SharingData{},
	}
	log.Printf("loading mpc sharing data from: %v", sharingDataPath)
	err := repository.loadDefaultSharingData(sharingDataPath)
	if err != nil {
		log.Panicf("loading mpc sharing data failed: %v", err)
	}
	return repository
}

func (r *repository) GetAll() ([]SharingData, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	allSharingData := make([]SharingData, 0)
	for _, sharingData := range r.storeMap {
		allSharingData = append(allSharingData, sharingData)
	}
	return allSharingData, nil
}

// GetByNumMpc returns the sharing data based on provided number of parties in this MPC
func (r *repository) GetByNumMPC(numMPC int) (SharingData, error) {

	r.mu.Lock()
	defer r.mu.Unlock()
	if sharingData, exists := r.storeMap[numMPC]; exists {
		return sharingData, nil
	}
	return SharingData{}, errors.New("sharing data does not exist")
}
func (r *repository) Create(sharingData *SharingData) error {
	r.mu.Lock()
	defer r.mu.Unlock()
	if _, exists := r.storeMap[sharingData.NumMPC]; exists {
		return errors.New("sharingData with same number already exists")
	}
	r.storeMap[sharingData.NumMPC] = SharingData{
		Data:   sharingData.Data,
		NumMPC: sharingData.NumMPC,
	}
	return nil
}

func (r *repository) Remove(numMPC int) {
	r.mu.Lock()
	defer r.mu.Unlock()
	delete(r.storeMap, numMPC)
}

// The structure of the sharing data directory:
// Current default path is coordinator/data/sharing_data
// The name of each sub-directory in sharing_data is
// an integer that represents the number of parties in the MPC
// in which contains the sharing data (SharingData.txt)
// for that specific number of parties

func (r *repository) loadDefaultSharingData(defaultSharingDataPath string) error {
	dirs, err := ioutil.ReadDir(defaultSharingDataPath)
	if err != nil {
		return err
	}
	for _, dir := range dirs {
		sharingData := &SharingData{}
		if dir.IsDir() {
			currentPath := defaultSharingDataPath + "/" + dir.Name()
			numMPC := dir.Name()
			sharingData.NumMPC, err = strconv.Atoi(numMPC)
			if err != nil {
				return err
			}
			log.Printf("looking for sharing data in: %v", currentPath)
			files, err := ioutil.ReadDir(currentPath)
			if err != nil {
				return err
			}
			for _, file := range files {
				fn := file.Name()
				if file.Mode().IsRegular() {
					if fn == "SharingData.txt" {
						log.Printf("loading data from file: %v", fn)
						content, err := ioutil.ReadFile(currentPath + "/" + fn)
						if err != nil {
							return err
						}
						sharingData.Data = string(content)
						err = r.Create(sharingData)
						if err != nil {
							return err
						}
					}
				}
			}
		}
	}
	return nil
}
