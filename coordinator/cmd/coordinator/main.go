package main

import (
	"context"
	"flag"
	"log"
	"net"
	"net/http"
	"os"
	"time"

	pb "gitlab.com/arpachain/mpc/coordinator/api"
	"gitlab.com/arpachain/mpc/coordinator/pkg/blockchain"
	"gitlab.com/arpachain/mpc/coordinator/pkg/certificate"
	"gitlab.com/arpachain/mpc/coordinator/pkg/group"
	"gitlab.com/arpachain/mpc/coordinator/pkg/mpc"
	"gitlab.com/arpachain/mpc/coordinator/pkg/node"
	"gitlab.com/arpachain/mpc/coordinator/pkg/program"
	"gitlab.com/arpachain/mpc/coordinator/pkg/server"
	"gitlab.com/arpachain/mpc/coordinator/pkg/sharing_data"
	"gitlab.com/arpachain/mpc/coordinator/pkg/worker_status_updater"
	"gitlab.com/arpachain/mpc/coordinator/pkg/worker_updater"
	"golang.org/x/net/websocket"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var (
	address      = flag.String("address", "127.0.0.1:10000", "The ip address and port number of the coordinator server")
	useRandom    = flag.Bool("random", true, "If true, coordinator server randomly assigns mpc groups instead of sequentially.")
	programsPath = flag.String(
		"programs_path",
		os.Getenv("GOPATH")+"/src/gitlab.com/arpachain/mpc/coordinator/data/program",
		"The path to the compiled program bytecode and schedule files.",
	)
	sharingDataPath = flag.String(
		"sharing_data_path",
		os.Getenv("GOPATH")+"/src/gitlab.com/arpachain/mpc/coordinator/data/sharing_data",
		"The path to the sharing data files.",
	)
	blochchainAdapterAddress = flag.String("blockchain_adapter_address", "127.0.0.1:40000", "The ip address and port number of the blockchain adapter server")
	workersUpdateInterval    = flag.Int("workers_update_interval", 5, "The time interval for updating workers in minutes.")
	maxNumWorkers            = flag.Int("max_num_workers", 10, "The maximum number of workers allowed in each mpc request.")
	wsAddress                = flag.String("wsaddr", "127.0.0.1:60000", "The ip address and port number of websocket server for updating worker status.")
	wsPrivateKey             = flag.String(
		"wskey",
		os.Getenv("HOME")+"/privkey.pem",
		"Full path to private key for websocket server for updating worker status.",
	)
	wsCertificate = flag.String(
		"wscert",
		os.Getenv("HOME")+"/fullchain.pem",
		"Full path to certificate for websocket server for updating worker status.",
	)
	numHeartbeatRetries    = flag.Int("num_heartbeat_retries", 10, "The number of retries when heartbeat fails.")
	heartbeatRetryInterval = flag.Duration("heartbeat_retry_interval", 1*time.Second, "The interval between retries when heartbeat fails in seconds.")
	heartbeatInterval      = flag.Duration("heartbeat_interval", 1*time.Second, "The heartbeat interval in seconds.")
)

func main() {
	flag.Parse()
	lis, err := net.Listen("tcp", *address)

	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	log.Printf("Server listening on : %v", *address)
	s := grpc.NewServer()

	// Dependency composition:
	certificateGenerator := certificate.NewGenerator()

	var nodeRepository node.Repository
	var groupRepository group.Repository
	var mpcRepository mpc.Repository

	var nodeManager node.Manager
	var groupManager group.Manager
	var mpcManager mpc.Manager

	var workerStatusUpdater worker_status_updater.Updater

	mpcRepository = mpc.NewRepository()
	programRepository := program.NewRepository(*programsPath)
	sharingDataRepository := sharing_data.NewRepository(*sharingDataPath)
	mapper := blockchain.NewMapper()

	if *useRandom {
		nodeRepository = node.NewRepository()
		groupRepository = group.NewRepository()
		workerStatusUpdater = worker_status_updater.NewUpdater(nodeRepository)
		groupManager = group.NewManager(nodeRepository, groupRepository, workerStatusUpdater)
	} else {
		nodeRepository = node.NewSequentialRepository()
		groupRepository = group.NewSequentialRepository()
		groupManager = group.NewSequentialManager(nodeRepository, groupRepository)
	}
	nodeManager = node.NewManager(nodeRepository, mapper, certificateGenerator)
	mpcManager = mpc.NewManager(mpcRepository)

	// mpcID -> result chan
	mpcResultChannelMap := make(map[string]chan server.MPCResult)

	workerClientFactory := server.NewWorkerClientFactory()
	blochchainAdapterClientFactory := server.NewBlochchainAdapterFactory(*blochchainAdapterAddress)

	coordinatorServer := server.NewCoordinatorServer(nodeManager, mpcManager, &mpcResultChannelMap)
	arpaMPCServer := server.NewArpaMpcServer(
		uint32(*maxNumWorkers),
		programRepository,
		sharingDataRepository,
		mapper,
		nodeManager,
		groupManager,
		mpcManager,
		workerClientFactory,
		certificateGenerator,
		&mpcResultChannelMap,
		blochchainAdapterClientFactory,
	)
	updater := worker_updater.NewUpdater(nodeRepository, blochchainAdapterClientFactory)
	workersUpdateTimeInterval := time.Minute * time.Duration(*workersUpdateInterval)
	healthChecker := server.NewHealthChecker(mapper, nodeRepository, workerClientFactory)
	healthChecker.StartHeartbeat(
		*heartbeatInterval,
		*heartbeatRetryInterval,
		*numHeartbeatRetries,
	)
	updater.Start(context.Background(), workersUpdateTimeInterval)
	pb.RegisterCoordinatorServer(s, coordinatorServer)
	pb.RegisterArpaMpcServer(s, arpaMPCServer)

	go func() {
		http.HandleFunc("/test", RegisterCertServer)
		http.Handle("/", websocket.Handler(workerStatusUpdater.Register))
		log.Printf("Websocket Server for updating worker statuses is listening on : %v", *wsAddress)
		if err := http.ListenAndServeTLS(*wsAddress, *wsCertificate, *wsPrivateKey, nil); err != nil {
			log.Printf("Starting websocket worker status updater server failed: %v", err)
		}
	}()

	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}

func RegisterCertServer(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte("This is for allowing certificate.\n"))
}
