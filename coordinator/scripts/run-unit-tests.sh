#!/bin/bash

set -ex

cd $GOPATH/src/gitlab.com/arpachain/mpc/coordinator/

go test ./... -cover
